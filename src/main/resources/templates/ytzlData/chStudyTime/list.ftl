<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>任务表检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <select name="className" id="className">
                        <option value="0">请选择班级</option>
                    </select>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">开始时间</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" autocomplete="off" id="startTime" lay-verify="required"
                               placeholder="请选择任务开始时间">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">结束时间</label>
                    <div class="layui-input-inline">
                        <input type="text" class="layui-input" autocomplete="off" id="endTime" lay-verify="required"
                               placeholder="请选择任务结束时间">
                    </div>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn" id="search" data-type="getInfo" lay-submit="" lay-filter="searchForm">查询
                    </button>
                </div>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>


</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;


        var t = {
            elem: '#test',
            url: '${base}/chData/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3,10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                // {type: 'checkbox'},
                {field: 'realName', title: '学生姓名', width: '50%'},
                {field: 'studyTimeLong', title: '学习时长', width: '50%'}
            ]]
        };
        // table.render(t);


        $('#addChTask').on('click', function () {
            var type = $(this).data('type');
            actives[type] ? actives[type].call(this) : '';
        });

        //动态加载当前用户所拥有的班级
        $.get("/chTask/selectClass", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择班级</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].id + '">' + data.data[i].className + '</option>';
                }
                $("#className").html(html);
                form.render();
            }
        }, "json");


        //查询
        var active = {
            getInfo: function () {
                var classId=$("#className").val();
                var startTime =$("#startTime").val();
                var  endTime=$("#endTime").val();
                var index = layer.msg('查询中，请稍候', {icon: 16, time: false, shade: 0});
                table.reload('test', {
                    where: {
                        classId: classId,
                        startTime: startTime,
                        endTime: endTime
                    }
                });
                layer.close(index);
            }
        };

        //根据任务类型查询
        $("#search").on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });


        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.render(t);
            // table.reload('test', t);
            return false;
        });

        // 时间选择器初始化 开始时间
        laydate.render({
            elem: '#startTime',
            type: 'datetime',
            // format: 'yyyy-MM-dd HH:mm:ss',
            done: function (value, date, endDate) {
                var startDate = new Date(value).getTime();
                var endTime = new Date($('#endTime').val()).getTime();
                if (endTime < startDate) {
                    layer.msg('结束时间不能小于开始时间');
                    $('#startTime').val($('#endTime').val());
                }
            }
        });
        laydate.render({ //结束时间
            elem: '#endTime',
            type: 'datetime',
            // format: 'yyyy-MM-dd HH:mm:ss',
            done: function (value, date, endDate) {
                var startDate = new Date($('#startTime').val()).getTime();
                var endTime = new Date(value).getTime();
                if (endTime < startDate) {
                    layer.msg('结束时间不能小于开始时间');
                    $('#endTime').val($('#startTime').val());
                }
            }
        });
    });
</script>
</body>
</html>