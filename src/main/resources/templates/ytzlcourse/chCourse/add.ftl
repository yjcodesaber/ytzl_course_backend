<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <input value="${chCourse.id}" name="id" type="hidden">
    <div class="layui-form-item">
        <label class="layui-form-label">课程名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="courseName" id="courseName">
            <input type="hidden" name="id" id="id">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">课程描述</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="courseDescribe" id="courseDescribe">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">激活状态</label>
        <div class="layui-input-block">
            <select name="isActivated" id="isActivated" lay-verify="required">
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChCourse">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer'], function () {
        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer;

        /**
         * 加载状态
         */
        $.get("${base}/admin/system/dict/getDictByType/is_activated", {}, function (data) {
            var html = "";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#isActivated").html(html);
                form.val('form', {
                    "isActivated": 1
                });
                //表单重新渲染
                form.render();

            }

        }, "json");
        form.on("submit(addChCourse)", function (data) {
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var editIndex=parent.layer.getFrameIndex(window.name);
            //给角色赋值
            $.post("${base}/chCourse/add", data.field, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg("添加成功！", {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>