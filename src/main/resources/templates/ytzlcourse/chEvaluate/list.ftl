<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all" />
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
  <legend>评价检索</legend>
  <div class="layui-field-box">
      <div class="layui-input-inline" style="float: left; position: relative;">
          <input style="width: auto;" type="text" autocomplete="off" id="select_saleId" name="select_saleId" lay-verify="required" placeholder="用户 or 知识点" autocomplete="off" class="layui-input">
      </div>
      <div class="layui-form layui-inline" style="padding-left: 5px">
      <div class="layui-input-inline">
          <select name="isActivated" id="isActivated" lay-verify="" >
          </select>
      </div>
      </div>
      <div class="layui-inline">
          <button class="layui-btn"  lay-submit="" id="searchBtn" data-type="getInfo" style="float: left;">搜索</button>
      </div>
  </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="isActivated_Name">
        {{#  if(d.isActivated == 1){ }}
        <span class="layui-badge layui-bg-green">审核通过</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-gray">已禁用</span>
        {{#  } }}
    </script>

    <script type="text/html" id="evaluateStar_Name">
        {{#  if(d.evaluateStar == 1){ }}
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        {{#  } else if(d.evaluateStar ==2){ }}
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        {{#  } else if(d.evaluateStar ==3){ }}
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        {{#  } else if(d.evaluateStar ==4){ }}
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        {{#  } else { }}
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        <i class="layui-icon layui-icon-rate-solid" style="font-size: 20px; color:#F5A623;"></i>
        {{#  } }}
    </script>

    <script type="text/html" id="barDemo">
        {{#  if(d.isActivated == 2){ }}
        <a class="layui-btn layui-btn-xs" lay-event="edit">审核</a>
        {{#  } }}
        {{#  if(d.isActivated == 1){ }}
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="dis">禁用</a>
        {{#  } }}
            <a id="orderInfoClick" class="layui-btn layui-bg-orange layui-btn-xs" lay-event="del">删除</a>

    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer','form','table','laydate'], function() {
        var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form,
                laydate = layui.laydate,
                table = layui.table;


        //监听工具条
        table.on('tool(demo)', function(obj){
            top.evaluateData = obj.data;
            var editIndex;
            if (obj.event === 'edit') {
                editIndex = layer.open({
                    title : "审核",
                    type : 2,
                    content : "${base}/chEvaluate/edit",
                    success : function(layero, index){
                            layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(editIndex);
                });
                layer.full(editIndex);
            }
            if(obj.event === "dis"){
                layer.confirm("你确定要禁用么？",{btn:['是的,我确定','我再想想']},
                        function(){
                    //alert("{}}}}}}}}}}}}}}}}}}}}}}}}}}")
                            $.post("${base}/chEvaluate/disabled",{"id":top.evaluateData.id},function (res){
                                if(res.success){
                                    layer.msg("禁用成功",{time: 1000},function(){
                                        table.render(t);
                                    });
                                }else{
                                    parent.layer.msg(res.message, {time: 1000}, function () {
                                       // parent.layer.close(editIndex);
                                        //刷新父页面
                                        parent.layui.table.reload('test');
                                    });
                                    layer.msg(res.message);
                                }

                            });
                        }
                )
            }
            if(obj.event === "del"){
                layer.confirm("你确定要删除么？",{btn:['是的,我确定','我再想想']},
                    function(){
                        //alert("{}}}}}}}}}}}}}}}}}}}}}}}}}}")
                        $.post("${base}/chEvaluate/delete",{"id":top.evaluateData.id},function (res){
                            if(res.success){
                                layer.msg("删除成功",{time: 1000},function(){
                                    table.render(t);
                                });
                            }else{
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            }
        });

        var t = {
            elem: '#test',
            url:'${base}/chEvaluate/list',
            method:'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits:[3,10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type:'checkbox'},
                {field:'id',  title: '评价编号',width:'12%'},
                {field:'realName',  title: '用户名称',width:'12%'},
                {field:'lorePointName',  title: '知识点名称',width:'12%'},
                {field:'evaluateContent',  title: '评价内容',width:'12%'},
                {field:'evaluateStar',  title: '星级',width:'12%', templet: '#evaluateStar_Name'},
                {field:'evaluateLike',  title: '点赞数',width:'12%'},
                {field:'isActivated',  title: '审核状态',width:'12%', templet: '#isActivated_Name'},
                {field:'creationDate',  title: '创建时间',width:'15%',templet:'<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',unresize: true}, //单元格内容水平居中
                {field:'checkDate',  title: '审核时间',width:'15%',templet:'<div>{{ layui.laytpl.toDateString(d.checkDate) }}</div>',unresize: true}, //单元格内容水平居中
                {fixed: 'right', title:'操作',  width: '15%', align: 'center',toolbar: '#barDemo'}

            ]]
        };
        table.render(t);

        var addIndex;
        var active={
            getInfo: function () {
                var condition=$('#select_saleId').val().trim();
                var isActivated = $("#isActivated").val();
                // alert(condition +"--"+cId+"--"+pStatus);
                var index = layer.msg('查询中，请稍候...',{icon: 16,time:false,shade:0});
                table.reload('test', { //表格的id
                    where: {
                        condition:condition,
                        isActivated:isActivated
                    }
                });
                layer.close(index);
            },
            addChEvaluate : function(){
                 addIndex = layer.open({
                    title : "添加",
                    type : 2,
                    content : "${base}/chEvaluate/add",
                    success : function(layero, addIndex){
                            layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(addIndex);
                });
                layer.full(addIndex);
            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        //根据用户名知识点名搜索

        $('#searchBtn').on('click',function(){
            var type = $(this).data('type');
            // alert(type);//getinfo
            active[type] ? active[type].call(this) : '';
        });
        // 点击获取数据
        //监听回车事件,扫描枪一扫描或者按下回车键就直接执行查询
        $("#select_saleId").bind("keyup", function (e) {
            if (e.keyCode == 13) {
                var type = "getInfo";
                active[type] ? active[type].call(this) : '';
            }
        });

        /**
         * 加载状态
         */
        $.get("/admin/system/dict/getDictByType/isActivated", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择审核状态</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#isActivated").html(html);
                //表单重新渲染
                form.render();
            }

        }, "json");

        form.on("submit(searchForm)",function(data){
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>