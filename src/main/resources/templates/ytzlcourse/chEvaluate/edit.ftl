<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

        .rate-solid-root{
            padding-top: 6px;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <input value="${chEvaluate.id}" name="id" type="hidden">

    <div class="layui-form-item">
        <label class="layui-form-label">用户名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="realName" id="realName" value="${chEvaluate.realName}" disabled>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">知识点名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="lorePointName" id="lorePointName" value = "${chEvaluate.lorePointName}" disabled>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">评价内容</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="evaluateContent" id="evaluateContent" value="${chEvaluate.evaluateContent}" disabled>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">星级</label>
        <div class="layui-input-block">
                <#--<input type="text" class="layui-input" autocomplete="off" name="evaluateStar" id="evaluateStar" value = "${chEvaluate.evaluateStar}" disabled >-->
            <div id="rate-solid-root" class="rate-solid-root">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">审核状态</label>
        <div class="layui-input-block">
            <select name="isActivated" id="isActivated" lay-verify="required">
            </select>
        </div>
    </div>
    <#--<div class="layui-form-item">-->
        <#--<label class="layui-form-label">创建时间</label>-->
        <#--<div class="layui-input-block">-->
                <#--<input type="text" class="layui-input" name="creationDate" id="creationDate" value = "${chEvaluate.creationDate}" disabled>-->
        <#--</div>-->
    <#--</div>-->
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChEvaluate">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form','jquery','layer'],function(){
        var form = layui.form,
            $ = layui.jquery,
                layer = layui.layer;

        var evaluateData=top.evaluateData;
      //  var evaluateData.evaluateStar;
        //表单初始赋值
        form.val('form', {
            "id": evaluateData.id,
            "realName": evaluateData.realName,
            "lorePointName": evaluateData.lorePointName,
            "evaluateStar":evaluateData.evaluateStar,
            "evaluateContent": evaluateData.evaluateContent,
           // "creationDate": evaluateData.creationDate
        });
       for(var i=0;i<evaluateData.evaluateStar;i++){
        $("#rate-solid-root").append('<i class="layui-icon layui-icon-rate-solid"  id="start" style="font-size: 20px; color:#F5A623;" ></i>');
       }
        //var star=null;
        // $("#start").each(function(){
        //
        // });
      //  for (
        $("#evaluateStar").val(function () {
            if(evaluateStar==1){

            }
        })

        function highlightStar(num){
            var starBg = document.getElementById("starContainer");
            var stars = starBg.getElementsByTagName("i");
            for(var i = 0 ; i < num ; i++){
                stars[i].className = 'highlight';
            }
        }

        form.on("submit(addChEvaluate)",function(postData){
            console.log("----" + JSON.stringify(postData.field));
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var editIndex=parent.layer.getFrameIndex(window.name);
            //给角色赋值
            $.post("${base}/chEvaluate/edit",postData.field,function(res){
                layer.close(loadIndex);
                if(res.success){
                    parent.layer.msg("编辑成功！",{time:1000},function(){
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                }else{
                    parent.layer.msg(res.message, {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                }
            });
            return false;
        });

        /**
         * 加载状态
         */
        $.get("${base}/admin/system/dict/getDictByType/isActivated", {}, function (data) {
            var html = "";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#isActivated").html(html);
                //表单重新渲染
                form.render();
                form.val('form', {
                    "isActivated": evaluateData.isActivated
                });
            }

        }, "json");

    });
</script>
</body>
</html>