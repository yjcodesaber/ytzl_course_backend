<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
    <div class="layui-form-item">
        <label class="layui-form-label">评价问题</label>
        <div class="layui-input-block">

            <input type="hidden" class="layui-input" name="evaluateContent" id="evaluateContent" >
            <div class="layui-upload-drag" id="evaluateContent">
              <i class="layui-icon"></i>
              <p>点击上传，或将文件拖拽到此处</p>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户id</label>
        <div class="layui-input-block">

            <input type="hidden" class="layui-input" name="userId" id="userId" >
            <div class="layui-upload-drag" id="userId">
              <i class="layui-icon"></i>
              <p>点击上传，或将文件拖拽到此处</p>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">知识点id</label>
        <div class="layui-input-block">

            <input type="hidden" class="layui-input" name="pointId" id="pointId" >
            <div class="layui-upload-drag" id="pointId">
              <i class="layui-icon"></i>
              <p>点击上传，或将文件拖拽到此处</p>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">星级(1</label>
        <div class="layui-input-block">

            <input type="hidden" class="layui-input" name="evaluateStar" id="evaluateStar" >
            <div class="layui-upload-drag" id="evaluateStar">
              <i class="layui-icon"></i>
              <p>点击上传，或将文件拖拽到此处</p>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">是否激活（显示评价）</label>
        <div class="layui-input-block">

            <input type="hidden" class="layui-input" name="isActivated" id="isActivated" >
            <div class="layui-upload-drag" id="isActivated">
              <i class="layui-icon"></i>
              <p>点击上传，或将文件拖拽到此处</p>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">评价时间</label>
        <div class="layui-input-block">

            <input type="hidden" class="layui-input" name="creationDate" id="creationDate" >
            <div class="layui-upload-drag" id="creationDate">
              <i class="layui-icon"></i>
              <p>点击上传，或将文件拖拽到此处</p>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChEvaluate">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form','jquery','layer'],function(){
        var form      = layui.form,
                $     = layui.jquery,
                layer = layui.layer;


        form.on("submit(addChEvaluate)",function(data){

            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var addIndex=parent.layer.getFrameIndex(window.name);
            $.post("${base}/admin/chEvaluate/add",data.field,function(res){
                layer.close(loadIndex);
                if(res.success){
                    parent.layer.msg("添加成功！",{time:1000},function(){
                        parent.layer.close(addIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                }else{
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>