<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-inline" style="margin-left: 15px">
                <label>查询条件:</label>
                <div class="layui-input-inline">
                    <input type="text" value="" autocomplete="off" name="searchInfo" placeholder="专业编号-or-专业名称"
                           class="layui-input search_input">
                </div>
            </div>
            <div class="layui-inline">
                <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="addChMajor">添加</a>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-xs" lay-event="distribute">分配课程</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;


        //监听工具条
        table.on('tool(demo)', function (obj) {
            var data = obj.data;
            top.majorData = data;
            if (obj.event === 'edit') {
                var editIndex = layer.open({
                    title: "编辑",
                    type: 2,
                    content: "${base}/chMajor/edit?id=" + data.id,
                    area: ['400px', '200px']
                });
            }
            var distributeIndex;
            if (obj.event === 'distribute') {
                distributeIndex = layer.open({
                    title: "给" + data.majorName + "专业添加课程",
                    type: 2,
                    content: "${base}/chSubjectLinkMajor/list?"
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(distributeIndex);
                });
                layer.full(distributeIndex);
            }
            if (obj.event === "del") {
                layer.confirm("你确定要删除该么？", {btn: ['是的,我确定', '我再想想']},
                    function () {
                        $.post("${base}/chMajor/delete", {"id": data.id}, function (res) {
                            if (res.success) {
                                layer.msg("删除成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            }
        });

        var t = {
            elem: '#test',
            url: '${base}/chMajor/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type: 'checkbox'},
                {field: 'id', title: '专业编号', width: '15%'},
                {field: 'majorName', title: '专业名称', width: '16%'},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {
                    field: 'modifyedDate',
                    title: '修改时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.modifyedDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);

        var active = {
            addChMajor: function () {
                var addIndex = layer.open({
                    title: "添加",
                    type: 2,
                    content: "${base}/chMajor/add",
                    area: ['400px', '200px']
                });

            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>