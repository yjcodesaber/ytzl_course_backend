<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

        textarea{
           height: 80px;
            line-height: 80px;
        }
        #problemDescription{
            height:80px;
            line-height: 80px;
        }
        #answerContent{
            height:80px;width:956px;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <input value="${chProblem.id}" name="id" type="hidden">
    <div class="layui-form-item" style="padding-top: 10px">
        <label class="layui-form-label">问题标题</label>
        <div class="layui-input-block">
                <input type="text" class="layui-input" autocomplete="off" name="problemTitle" id="problemTitle" value = "" disabled>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">问题描述</label>
        <div class="layui-input-block">
            <textarea type="text" class="layui-input" name="problemDescription" id="problemDescription" value = "" disabled ></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">课程名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="courseName" id="courseName" value = "${chProblem.courseName}" disabled>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">单元名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="unitName" id="unitName" value = "${chProblem.unitName}" disabled>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">章节名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="sectionName" id="sectionName" value = "${chProblem.sectionName}" disabled>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">知识点名称</label>
        <div class="layui-input-block">
                <input type="text" class="layui-input" autocomplete="off" name="lorePointName" id="lorePointName" value = "${chProblem.lorePointName}" disabled>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">回复内容</label>
        <div class="layui-input-block">
            <textarea type="text" name="answerContent" id="answerContent" value = "${chProblem.answerContent}" ></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChProblem">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form','jquery','layer'],function(){
        var form      = layui.form,
                $     = layui.jquery,
                layer = layui.layer;

        var problemData=top.problemData;
        //alert("my name is=====-"+problemData.courseName);
        //表单初始赋值
        form.val('form', {
            "id": problemData.id,
            "problemTitle": problemData.problemTitle,
            "problemDescription": problemData.problemDescription,
            "courseName": problemData.courseName,
            "unitName": problemData.unitName,
            "sectionName": problemData.sectionName,
            "lorePointName": problemData.lorePointName,
            "answerContent": problemData.answerContent
            // "creationDate": evaluateData.creationDate
        });

        form.on("submit(addChProblem)",function(postData){
            console.log("----" + JSON.stringify(postData.field));
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });

            var editIndex=parent.layer.getFrameIndex(window.name);
            //给角色赋值
            $.post("${base}/chProblem/edit",postData.field,function(res){
                layer.close(loadIndex);
                if(res.success){
                    parent.layer.msg("回复成功！",{time:1000},function(){
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                }else{
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>