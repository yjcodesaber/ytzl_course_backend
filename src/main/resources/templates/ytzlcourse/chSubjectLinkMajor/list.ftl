<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<div class="layui-container" style="margin-top: 15px;">
    <div class="layui-card">
        <div class="layui-card-header" style="background-color: #f5f4f0">左边列出还未分配的课程，右边为已分配的课程</div>
    </div>
    <div id="page" style="min-height: 450px">
        <div id="root" ></div>
    </div>
    <div>
        <button class="layui-btn " id="distribution" style="display:block;margin:0 auto">确定分配</button>
    </div>
</body>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>

<script>
    layui.config({
        base: '${base}/static/layui_exts/transfer/'
    }).use(['transfer'], function () {
        var transfer = layui.transfer,
                $ = layui.$;
        var majorData=top.majorData;


        var str1 = [];
        var str2 = [];

        //表格列
        var cols = [{type: 'checkbox', fixed: 'left'},
            {field: 'id', title: 'ID', sort: true},
            {field: 'subjectName', title: '课程名称'}];
        //表格配置文件
        var tabConfig = {'page': true, 'limits': [3,10, 20, 30, 50, 80], 'height': 400};


        //左边数据源
        $.ajax({
            url: "${base}/chSubject/findAll?majorId="+majorData.id,
            type: 'get',
            async: false,
            data: {},
            dataType: 'json',
            success: function (data) {
                str1=data;
            }
        });
        //右边数据源
        $.ajax({
            url: "${base}/chSubject/findSubjectName",
            type: 'post',
            async: false,
            data: {"majorId":majorData.id},
            dataType: 'json',
            success: function (data) {
                str2=data;
                }
        });
        var tb1 = transfer.render({
            elem: "#root", //指定元素
            cols: cols, //表格列  支持layui数据表格所有配置
            data: [str1,str2], //[左表数据,右表数据[非必填]]
            tabConfig: tabConfig //表格配置项 支持layui数据表格所有配置
        });




        //transfer.get(参数1:初始化返回值,参数2:获取数据[all,left,right,l,r],参数:指定数据字段)

        $('#distribution').on('click', function () {
            var loadIndex = layer.load(2, {
                shade: [0.3,'#333']
            });
            var data = transfer.get(tb1, 'r');
            var subjectIds="";
            if(data!=null){
                for(var i=0;i<data.length;i++){
                    subjectIds+=data[i].id;
                    if(i<data.length-1){
                        subjectIds+=",";
                    }
                }
            }
          $.post("${base}/chSubjectLinkMajor/add",{subjectIds:subjectIds,majorId:majorData.id},function(res){

              if(res.success==true){
                    layer.msg("该专业分配相关课程成功");
                    parent.layer.close(parent.distributeIndex);
                    //刷新父页面
                    parent.location.reload();
                }else{
                  layer.msg("该专业分配相关课程失败");
              }

            });

        });
    })
</script>
</html>