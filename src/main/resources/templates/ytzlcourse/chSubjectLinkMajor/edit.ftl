<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <style type="text/css">
        .layui-form-item .layui-inline{ width:33.333%; float:left; margin-right:0; }
        @media(max-width:1240px){
            .layui-form-item .layui-inline{ width:100%; float:none; }
        }
        .layui-form-item .role-box {
            position: relative;
        }
        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
    <input value="${chSubjectLinkMajor.id}" name="id" type="hidden">
    <div class="layui-form-item">
        <label class="layui-form-label">课程Id</label>
        <div class="layui-input-block">
                <input type="hidden" class="layui-input" name="subjectId" id="subjectId" value = "${chSubjectLinkMajor.subjectId}" >
                <div class="layui-upload-drag" id="subjectId">
                  <i class="layui-icon"></i>
                  <p>点击上传，或将文件拖拽到此处</p>
                </div>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">专业Id</label>
        <div class="layui-input-block">
                <input type="hidden" class="layui-input" name="majorId" id="majorId" value = "${chSubjectLinkMajor.majorId}" >
                <div class="layui-upload-drag" id="majorId">
                  <i class="layui-icon"></i>
                  <p>点击上传，或将文件拖拽到此处</p>
                </div>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <div class="layui-input-block">
                <input type="hidden" class="layui-input" name="creationDate" id="creationDate" value = "${chSubjectLinkMajor.creationDate}" >
                <div class="layui-upload-drag" id="creationDate">
                  <i class="layui-icon"></i>
                  <p>点击上传，或将文件拖拽到此处</p>
                </div>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <div class="layui-input-block">
                <input type="hidden" class="layui-input" name="creationBy" id="creationBy" value = "${chSubjectLinkMajor.creationBy}" >
                <div class="layui-upload-drag" id="creationBy">
                  <i class="layui-icon"></i>
                  <p>点击上传，或将文件拖拽到此处</p>
                </div>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <div class="layui-input-block">
                <input type="hidden" class="layui-input" name="modifyedDate" id="modifyedDate" value = "${chSubjectLinkMajor.modifyedDate}" >
                <div class="layui-upload-drag" id="modifyedDate">
                  <i class="layui-icon"></i>
                  <p>点击上传，或将文件拖拽到此处</p>
                </div>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <div class="layui-input-block">
                <input type="hidden" class="layui-input" name="modifyedBy" id="modifyedBy" value = "${chSubjectLinkMajor.modifyedBy}" >
                <div class="layui-upload-drag" id="modifyedBy">
                  <i class="layui-icon"></i>
                  <p>点击上传，或将文件拖拽到此处</p>
                </div>

        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChSubjectLinkMajor">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form','jquery','layer'],function(){
        var form      = layui.form,
                $     = layui.jquery,
                layer = layui.layer;


        form.on("submit(addChSubjectLinkMajor)",function(data){
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            //给角色赋值
            var editIndex=parent.layer.getFrameIndex(window.name);
            $.post("${base}/chSubjectLinkMajor/edit",data.field,function(res){
                layer.close(loadIndex);
                if(res.success){
                    parent.layer.msg("编辑成功！",{time:1000},function(){
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                }else{
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>