<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all" />
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-inline" style="margin-left: 15px">
                <label>章节名称:</label>
                <div class="layui-input-inline">
                    <input type="text" value="" autocomplete="off" name="sectionName" placeholder="请输入单元名称" class="layui-input search_input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">所属课程：</label>
                <div class="layui-input-block">
                    <select name="courseId" id="courseId" lay-filter="course">

                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">所属单元：</label>
                <div class="layui-input-block">
                    <select name="unitId" id="unitId" >
                        <option value="">请先选择课程</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="addChSection">添加</a>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
    <script type="text/html" id="sectionStatus">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.isActivated == 1){ }}
        <span class="layui-badge layui-bg-green">已激活</span>
        {{#  } else if(d.isActivated ==2){ }}
        <span class="layui-badge layui-bg-blue">未激活</span>
        {{#  } }}

    </script>

    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer','form','table','laydate'], function() {
        var layer = layui.layer,
                $ = layui.jquery,
                form = layui.form,
                laydate = layui.laydate,
                table = layui.table;


        /**
         * 加载课程列表
         */
        $.post("${base}/chCourse/getCourseClass", {}, function (data) {
            var html = "<option value=\"\">请选择所属课程</option>";
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].courseName + '</option>';
            }
            $("#courseId").html(html);
            //表单重新渲染
            form.render();
        }, "json");


        /**
         * 监听form-select事件
         */
        form.on("select(course)",function(){
            var courseId = $("#courseId").val();
            /**
             * 加载单元列表
             */
            $.post("${base}/chUnit/getUnitClass", {courseId:courseId}, function (data) {
                var html = "<option value=\"\">请选择对应单元</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].unitName + '</option>';
                }
                $("#unitId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });



        //监听工具条
        table.on('tool(demo)', function(obj){
            var data = obj.data;
            top.sectionData = obj.data;
            var editIndex;
            if (obj.event === 'edit') {
                editIndex = layer.open({
                    title : "编辑",
                    type : 2,
                    content : "${base}/chSection/edit?id="+data.id,

                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(editIndex);
                });
                layer.full(editIndex);
            }
            if(obj.event === "del"){
                layer.confirm("你确定要删除该么？",{btn:['是的,我确定','我再想想']},
                        function(){
                            $.post("${base}/chSection/delete",{"id":data.id},function (res){
                                if(res.success){
                                    layer.msg("删除成功",{time: 1000},function(){
                                        table.render(t);
                                    });
                                }else{
                                    layer.msg(res.message);
                                }

                            });
                        }
                )
            }
        });

        var t = {
            elem: '#test',
            url:'${base}/chSection/list',
            method:'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits:[3,10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols:  [[
                {type: 'checkbox'},
                {field: 'id', title: '章节ID', width: '12%'},
                {field: 'sectionName', title: '章节名称', width: '12%'},
                {field: 'sectionDescribe', title: '章节描述', width: '12%'},
                {field: 'isActivated', title: '激活状态', width: '10%', templet: '#sectionStatus'},
                {field: 'unitId', title: '单元ID', width: '15%'},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    width: '15%',
                    unresize: true
                },
                {
                    field: 'modifyedDate',
                    title: '修改时间',
                     width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.modifyedDate) }}</div>',
                    unresize: true
                },
                //显示居中
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);

        var addIndex;
        var active={
            addChSection : function(){
                 addIndex = layer.open({
                    title : "添加",
                    type : 2,
                    content : "${base}/chSection/add",
                    success : function(layero, addIndex){
                        setTimeout(function(){
                            layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                        },500);
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function(){
                    layer.full(addIndex);
                });
                layer.full(addIndex);
            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)",function(data){
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>