<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>日志列表--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/formatJSON/jsonFormater.css" media="all"/>
    <style>
        #Canvas {
            margin-top: 13px;
            padding: 20px 20px;
        }

        div.Canvas {
            font-size: 13px;
            background-color: #ECECEC;
            color: #000000;
            border: solid 1px #CECECE;
        }
    </style>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>日志检索</legend>
    <div class="layui-field-box">
        <form class="layui-form">
            <div class="layui-input-inline">
                <select name="moduleName" id="moduleName">
                    <option value="0">请选择状态</option>
                </select>
            </div>
            <div class="layui-inline">
                <button class="layui-btn" id="search" data-type="getInfo" lay-submit="" lay-filter="searchForm">查询
                </button>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="params">
        {{# if(d.requestParam != '' && d.requestParam != null){ }}
        <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="showParams">点我显示</a>
        {{# }else{ }}
        <span></span>
        {{# } }}
    </script>
    <#--    <script type="text/html" id="barDemo">-->
    <#--        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>-->
    <#--    </script>-->
</div>
<div id="page"></div>
<div id='jsonContainer' class="Canvas" style="display: none"></div>
<script>
    var baseDir = '${base}';
</script>
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script type="text/javascript" src="${base}/static/formatJSON/jsonFormater.js?v=3.0"></script>
<script>
    layui.use(['layer', 'form', 'table'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            t;                  //表格数据变量

        t = {
            elem: '#test',
            url: '${base}/chLog/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 4, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30, 50, 80]
            },
            width: $(parent.window).width() - 223,
            cols: [[
                // {type: 'checkbox'},
                {field: 'moduleName', title: '模块名称', width: '15%'},
                {field: 'requestIp', title: '请求Ip', width: '15%'},
                {field: 'requestUrl', title: '请求地址', width: '15%'},
                {field: 'requestParam', title: '请求参数', width: '20%', templet: '#params'},
                {field: 'errorMsg', title: '错误信息', width: '20%'},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true,
                    width: '10%'
                } //单元格内容水平居中
                // {fixed: 'right', title: '操作', width: '10%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);

        //监听工具条
        table.on('tool(demo)', function (obj) {
            top.logData = obj.data;
            if (obj.event === "showParams") {
                $("#jsonContainer").empty();
                var options = {dom: document.getElementById('jsonContainer')};
                window.jf = new JsonFormatter(options);
                jf.doFormat(top.logData.requestParam);
                layer.open({
                    type: 1,
                    title: false,
                    closeBtn: 0,
                    area: '516px',
                    shadeClose: true,
                    content: $('#jsonContainer')
                });
            }
        });

        //功能按钮
        var active = {
            getInfo: function () {
                var moduleName = $("#moduleName").val();
                var index = layer.msg('查询中，请稍候', {icon: 16, time: false, shade: 0});
                table.reload('test', {
                    where: {
                        moduleName: moduleName
                    }
                });
                layer.close(index);
            }
        };
        /**
         * 加载模块名称
         */
        $.get("/admin/system/dict/getDictByType/moduleName", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择模块</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#moduleName").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");

        $('#search').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        //搜索
        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>