<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style type="text/css">
        .layui-form-item .layui-inline {
            width: 33.333%;
            float: left;
            margin-right: 0;
        }

        @media (max-width: 1240px) {
            .layui-form-item .layui-inline {
                width: 100%;
                float: none;
            }
        }

        .layui-form-item .role-box {
            position: relative;
        }

        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

        .lab {
            width: 100px;
            text-align: left;
        }
    </style>
</head>
<body class="childrenBody">
<form class="layui-form">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>请上传你的excel表格</legend>
    </fieldset>

    <div class="layui-upload-drag" style="margin-left:20px " id="test10">
        <i class="layui-icon"></i>
        <p>点击上传，或将文件拖拽到此处</p>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer', 'upload'], function () {
        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer;
        var upload = layui.upload;
        //拖拽上传
        upload.render({
            elem: '#test10',
            accept: "file"
            , url: '${base}/excel/importExcel'
            ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致
                layer.load(2, {
                    shade: [0.3, '#333']
                }); //上传loading
            }
            , done: function (res) {
                layer.closeAll('loading');
                layer.msg(res.message);
                parent.layer.close(parent.excelinIndex);
                parent.location.reload();
            }, error: function (res) {
                layer.msg(res.message+"，请重新选择文件", {anim: 6});
            }
        });

    });
</script>
</body>
</html>