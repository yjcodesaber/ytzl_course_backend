<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <script type="text/javascript" charset="utf-8" src="${base}/static/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${base}/static/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="${base}/static/ueditor/lang/zh-cn/zh-cn.js"></script>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style>
        #editor {
            height: 300px;
        }

        #editorBlock {
            margin-top: 10px;
            height: 400px;
            z-index: 0;
        }
    </style>
</head>
<body class="childrenBody" style=" background-color: #f8f8f8;">
<div style="padding: 20px">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header">搜索题库:</div>
                <div class="layui-card-body">
                    <form class="layui-form" lay-filter="form1" id="searchForm">
                        <div class="layui-inline" style="margin-left: 15px">
                            <label>题目标题:</label>
                            <div class="layui-input-inline">
                                <input type="text" value="" autocomplete="off" name="searchInfo" placeholder="请输入内容"
                                       class="layui-input search_input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
                        </div>
                        <div class="layui-inline">
                            <a class="layui-btn layui-btn-normal" data-type="exportQuestion">导出试题</a>
                        </div>
                    </form>
                    <div class="layui-form">
                        <table class="layui-table" id="test" lay-filter="demo"></table>
                        <script type="text/html" id="qType">
                            {{#  if(d.questionType == 1){ }}
                            <span class="layui-badge layui-bg-green">单选题</span>
                            {{#  } else { }}
                            <span class="layui-badge layui-bg-blue">多选题</span>
                            {{#  } }}
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-md6" style="float: right;">
            <div class="layui-card ">
                <div class="layui-card-header">试卷模板:可以选择题库题目，或者添加新的试题</div>
                <div class="layui-card-body" id="examlist">
                    <div class="layui-card" style=" background-color: #f8f8f8;">
                        <button class="layui-btn layui-btn-radius layui-btn-normal" id="downexam">
                            <i class="layui-icon" style="display: inline-block">&#xe601;</i>
                            <span style="display: inline-block;vertical-align: middle">下载</span>
                        </button>
                        <span>点击下载将会获得试卷和答案的图片</span>
                    </div>
                    <div id="exampage">

                    </div>
                </div>
            </div>
            <div class="layui-card ">
                <div class="layui-card-header">答案:</div>
                <div class="layui-card-body" id="anwserlist">
                    <div id="anwserpage">

                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-md6" style="float: left;clear: left;">
            <div class="layui-card">
                <div class="layui-card-header">创建试题:</div>
                <div class="layui-card-body">
                    <form class="layui-form" lay-filter="form2" style="margin-top: -50px; width: 95% ;">
                        <div class="layui-form-item" style="margin-top: 60px">
                            <label class="layui-form-label lab">选择目录</label>
                            <div class="layui-input-block">
                                <div class="layui-input-inline">
                                    <select name="courseId" class="submitlist" id="courseId" lay-filter="course"
                                            lay-verify="required">
                                        <option value="">选择课程</option>
                                    </select>

                                </div>
                                <div class="layui-input-inline">
                                    <select name="unitId" id="unitId" class="submitlist" lay-filter="unit"
                                            lay-verify="required">
                                        <option value="">请选择单元</option>
                                    </select>

                                </div>
                                <div class="layui-input-inline">
                                    <select name="sectionId" id="sectionId" class="submitlist" lay-filter="section"
                                            lay-verify="required">
                                        <option value="">请选择章节</option>
                                    </select>
                                </div>
                                <div class="layui-input-inline">
                                    <select name="lorePointId" id="lorePointId" class="submitlist"
                                            lay-filter="lorepoint"
                                            lay-verify="required">
                                        <option value="">请选择技能点</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label lab">关键字</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input submitlist" autocomplete="off" name="keyWords"
                                       id="keyWords"
                                       value=""
                                       lay-verify="required" placeholder="多个标题用空格分开">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label lab">题目描述</label>
                            <div class="layui-input-block" id="editorBlock">
                                <script id="editor" type="text/plain">

                                </script>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label lab">题目类型</label>
                            <div class="layui-input-inline">
                                <select name="questionType" class="submitlist" id="questionType"
                                        lay-verify="required">
                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label lab">答案解析</label>
                            <div class="layui-input-block">
                                <textarea name="questionAnalysis" id="questionAnalysis" placeholder="请输入答案解析"
                                          class="layui-textarea submitlist"></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label ">第一选项</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input submitlist" autocomplete="off" name="anwser1"
                                       id="anwser1"
                                       value=""
                                       lay-verify="required" placeholder="选项1">
                                <input type="checkbox" class="anwserok" name="a1ok" lay-skin="switch" lay-text="正确|错误" value="1">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label ">第二选项</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input submitlist" autocomplete="off" name="anwser2"
                                       id="anwser2"
                                       value=""
                                       lay-verify="required" placeholder="选项2">
                                <input type="checkbox" class="anwserok" name="a2ok" lay-skin="switch" lay-text="正确|错误" value="1">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label ">第三选项</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input submitlist" autocomplete="off" name="anwser3"
                                       id="anwser3"
                                       value=""
                                       lay-verify="required" placeholder="选项3">
                                <input type="checkbox" class="anwserok" name="a3ok" lay-skin="switch" lay-text="正确|错误" value="1">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label ">第四选项</label>
                            <div class="layui-input-block">
                                <input type="text" class="layui-input  submitlist" autocomplete="off" name="anwser4"
                                       id="anwser4"
                                       value=""
                                       lay-verify="required" placeholder="选项4">
                                <input type="checkbox" class="anwserok" name="a4ok" lay-skin="switch" lay-text="正确|错误" value="1">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button type="button" class="layui-btn submitlist" lay-submit=""
                                        lay-filter="addChQuestion">添入题库
                                </button>
                                <button type="reset " id="reset" class="layui-btn submitlist layui-btn-primary">重置
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script type="text/javascript" src="${base}/static/js/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/html2canvas/0.5.0-beta4/html2canvas.js"></script>
<script type="text/javascript">
    //直接选择要截图的dom，就能截图，但是因为canvas的原因，生成的图片模糊
    //html2canvas(document.querySelector('div')).then(function(canvas) {
    //    document.body.appendChild(canvas);
    //});
    //创建一个新的canvas
    $("#downexam").on('click', function () {
        var canvas2 = document.createElement("canvas");
        let _canvas = document.querySelector('#exampage');
        var w = parseInt(window.getComputedStyle(_canvas).width);
        var h = parseInt(window.getComputedStyle(_canvas).height);
        //将canvas画布放大若干倍，然后盛放在较小的容器内，就显得不模糊了
        canvas2.width = w * 2;
        canvas2.height = h * 2;
        canvas2.style.width = w + "px";
        canvas2.style.height = h + "px";
        //可以按照自己的需求，对context的参数修改,translate指的是偏移量
        //  var context = canvas.getContext("2d");
        //  context.translate(0,0);
        var context = canvas2.getContext("2d");
        context.scale(2, 2);
        html2canvas(document.querySelector('#exampage'),canvas2).then(function (canvas) {
            var imgUri = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"); // 获取生成的图片的url 　
            var saveLink = document.createElement('a');
            saveLink.href = imgUri;
            saveLink.download = 'exam.png';
            saveLink.click();
        });
        html2canvas(document.querySelector('#anwserpage'),canvas2).then(function (canvas) {
            var imgUri = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"); // 获取生成的图片的url 　
            var saveLink = document.createElement('a');
            saveLink.href = imgUri;
            saveLink.download = 'anwser.png';
            saveLink.click();
        });
    })
</script>


<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        //实例化编辑器
        //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
        var ue = UE.getEditor('editor', {
            toolbars: [
                [
                    'undo', //撤销
                    'bold', //加粗
                    'underline', //下划线
                    'preview', //预览
                    'horizontal', //分隔线
                    'inserttitle', //插入标题
                    'cleardoc', //清空文档
                    'fontfamily', //字体
                    'fontsize', //字号
                    'paragraph', //段落格式
                    'inserttable', //插入表格
                    'justifyleft', //居左对齐
                    'justifyright', //居右对齐
                    'justifycenter', //居中对
                    'justifyjustify', //两端对齐
                    'forecolor', //字体颜色
                    'insertcode' //插入代码
                ]
            ]
        });

        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;


        //监听工具条
        /*table.on('tool(demo)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {

            }
        });*/

        /**
         * 加载题目类型
         */
        $.get("${base}/admin/system/dict/getDictByType/question_type", {}, function (data) {
            var html = "<option value=\"\">请选择题目类型</option>";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#questionType").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");

        /**
         * 加载课程列表
         */
        $.post("${base}/chCourse/getCourseClass", {}, function (data) {
            var html = "<option value=\"\">请选择课程</option>";
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].courseName + '</option>';
            }
            $("#courseId").html(html);
            //表单重新渲染
            form.render();
        }, "json");

        /**
         * 监听form-select事件
         */
        form.on("select(course)", function () {
            var courseId = $("#courseId").val();
            if (courseId == "") return false;
            /**
             * 加载单元列表
             */
            $.post("${base}/chUnit/getUnitClass", {courseId: courseId}, function (data) {
                var html = "<option value=\"\">请选择单元</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].unitName + '</option>';
                }
                $("#unitId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });
        /**
         * 监听form-select事件
         */
        form.on("select(unit)", function () {
            var unitId = $("#unitId").val();
            if (unitId == "") {
                return false;
            }
            /**
             * 加载章节列表
             */
            $.post("${base}/chSection/getSectionClass", {unitId: unitId}, function (data) {
                var html = "<option value=\"\">请选择章节</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].sectionName + '</option>';
                }
                $("#sectionId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });
        /**
         * 监听form-select事件
         */
        form.on("select(section)", function () {
            var sectionId = $("#sectionId").val();
            if (sectionId == "") {
                return false;
            }
            /**
             * 加载知识点列表
             */
            $.post("${base}/chLorePoint/getLorePoint", {sectionId: sectionId}, function (data) {
                var html = "<option value=\"\">请选择技能点</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].lorePointName + '</option>';
                }
                $("#lorePointId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });


        var t = {
            elem: '#test',
            url: '${base}/chQuestion/questionVOlist',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type: 'checkbox'},
                {field: 'questionDescribe', width: '68%', title: '试题描述'},
                {field: 'questionType', width: '18%', title: '试题类型', templet: '#qType', fixed: 'right'}
            ]]
        };
        table.render(t);

        var active = {
            exportQuestion: function () {
                var checkStatus = table.checkStatus('test');
                for (var i = 0; i < checkStatus.data.length; i++) {

                    var examnum = $("#exampage").children().length;
                    $("#exampage").append("<div class=\"layui-card\" style=\" background-color: #f8f8f8;\">\n" +
                        "                        <div class=\"layui-card-body\">\n" +
                        '                            <p><span>' + (examnum + 1) + '\.</span>' + checkStatus.data[i].questionDescribe + '</p>\n' +
                        "                            <p>A:" + checkStatus.data[i].anwser1 + "</p>\n" +
                        "                            <p>B:" + checkStatus.data[i].anwser2 + "</p>\n" +
                        "                            <p>C:" + checkStatus.data[i].anwser3 + "</p>\n" +
                        "                            <p>D:" + checkStatus.data[i].anwser4 + "</p>\n" +
                        "                        </div>\n" +
                        "                    </div>");

                    var anwser_html = " <div class=\"layui-card\"  style=\" background-color: #f8f8f8;\"><div class=\"layui-card-header\">第" + (examnum + 1) + "题: 正确答案是: ";
                    if (checkStatus.data[i].a1ok == "1") {
                        anwser_html = anwser_html + "A ";
                    }
                    if (checkStatus.data[i].a2ok == "1") {
                        anwser_html = anwser_html + "B ";
                    }
                    if (checkStatus.data[i].a3ok == "1") {
                        anwser_html = anwser_html + "C ";
                    }
                    if (checkStatus.data[i].a4ok == "1") {
                        anwser_html = anwser_html + "D ";
                    }
                    anwser_html = anwser_html + "</div>";
                    anwser_html = anwser_html + " <div class=\"layui-card-body\"> <span>解析: </span>" + checkStatus.data[i].questionAnalysis + "</div></div > ";
                    $("#anwserpage").append(anwser_html);
                }
            }
        };
        form.on("submit(addChQuestion)", function (data) {
            var qType = data.field.questionType;
            var num = 0;
            $(".anwserok:checked").each(function(){
                num++;
            });
            if(num == 0){
                layer.msg("请给出正确选项",{anim:6});
                return false;
            }
            if (qType == "1" && num > 1) {
                layer.msg("该题为单选题，请确认选项",{anim:6});
                return false;
            }

            if (qType == "2" && num < 2) {
                layer.msg("该题为多选题，请确认选项",{anim:6});
                return false;
            }

            data.field.questionDescribe = UE.getEditor('editor').getContent();
            $("#selectvedio").addClass('layui-btn-disabled').attr('disabled', "true");
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            $.post("${base}/chQuestion/creatQandA", data.field, function (res) {
                if (res.success) {
                    layer.close(loadIndex);
                    layer.msg("导入成功");
                    $("#selectvedio").removeClass('layui-btn-disabled').removeAttr("disabled");
                    var examnum = $("#exampage").children().length;
                    $("#exampage").append("<div class=\"layui-card\" style=\" background-color: #f8f8f8;\">\n" +
                        "                        <div class=\"layui-card-body\">\n" +
                        '                            <p><span>' + (examnum + 1) + '\.</span>' + data.field.questionDescribe + '</p>\n' +
                        "                            <p>A:" + data.field.anwser1 + "</p>\n" +
                        "                            <p>B:" + data.field.anwser2 + "</p>\n" +
                        "                            <p>C:" + data.field.anwser3 + "</p>\n" +
                        "                            <p>D:" + data.field.anwser4 + "</p>\n" +
                        "                        </div>\n" +
                        "                    </div>");

                    var anwser_html = " <div class=\"layui-card\"  style=\" background-color: #f8f8f8;\">" +
                        "                            <div class=\"layui-card-header\">第" + (examnum + 1) + "题:  正确答案是: ";
                    if (data.field.a1ok == "1") {
                        anwser_html = anwser_html + "A ";
                    }
                    if (data.field.a2ok == "1") {
                        anwser_html = anwser_html + "B ";
                    }
                    if (data.field.a3ok == "1") {
                        anwser_html = anwser_html + "C ";
                    }
                    if (data.field.a4ok == "1") {
                        anwser_html = anwser_html + "D ";
                    }
                    anwser_html = anwser_html + "</div>";
                    anwser_html = anwser_html + " <div class=\"layui-card-body\"> <span>解析: </span>" + data.field.questionAnalysis + "</div></div > ";
                    form.val('form2',{
                        'keyWords':"",
                        'questionType':"",
                        'questionAnalysis':"",
                        'anwser1':"",
                        'anwser2':"",
                        'anwser3':"",
                        'anwser4':"",
                        'a1ok':"",
                        'a2ok':"",
                        'a3ok':"",
                        'a4ok':""
                    })
                    ue.addListener("ready", function () {
                        //赋值
                        ue.setContent("");
                    });
                    $("#anwserpage").append(anwser_html);

                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });

        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>