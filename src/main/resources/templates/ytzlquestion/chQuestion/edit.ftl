<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <script type="text/javascript" charset="utf-8" src="${base}/static/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${base}/static/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="${base}/static/ueditor/lang/zh-cn/zh-cn.js"></script>
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style type="text/css">
        .layui-form-item .layui-inline {
            width: 33.333%;
            float: left;
            margin-right: 0;
        }

        @media (max-width: 1240px) {
            .layui-form-item .layui-inline {
                width: 100%;
                float: none;
            }
        }

        .layui-form-item .role-box {
            position: relative;
        }

        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

        .lab {
            width: 100px;
            text-align: left;
        }
        #editor{
            height: 300px;
        }
        #editorBlock{
            margin-top: 10px;
            height: 370px;
            z-index: 0;
        }
    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <input value="" name="id" type="hidden">

    <div class="layui-form-item" style="margin-top: 60px">
        <label class="layui-form-label lab">选择所属目录</label>
        <div class="layui-input-inline">
            <select class="submitlist" name="courseId" id="courseId" lay-filter="course">
                <option value="">选择课程</option>
            </select>
        </div>
        <div class="layui-input-inline">
            <select class="submitlist" name="unitId" id="unitId" lay-filter="unit">
                <option value="">请选择单元</option>
            </select>

        </div>
        <div class="layui-input-inline">
            <select class="submitlist" name="sectionId" id="sectionId" lay-filter="section">
                <option value="">请选择章节</option>
            </select>

        </div>
        <div class="layui-input-inline">
            <select class="submitlist" name="lorePointId" id="lorePointId" lay-filter="lorepoint" lay-verify="required">
                <option value="">请选择技能点</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label lab">题目标题</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input submitlist" autocomplete="off" name="keyWords" id="keyWords"
                   value=""
                   lay-verify="required" placeholder="多个标题用空格分开">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label lab">题目描述</label>
        <div  class="layui-input-block" id="editorBlock">
            <script id="editor" type="text/plain" >

            </script>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label lab">题目类型</label>
        <div class="layui-input-inline">
            <select class="submitlist" name="questionType" id="questionType" lay-verify="required">

            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label lab">答案解析</label>
        <div class="layui-input-block">
            <textarea name="questionAnalysis" id="questionAnalysis" placeholder="请输入答案解析"
                      class="layui-textarea submitlist "></textarea>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn submitlist" id="submit-btn" lay-submit="" lay-filter="editQuestion">立即提交</button>
            <button type="reset" class=" submitlist layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/qiniu.min.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer'], function () {

        //实例化编辑器
        //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
        var ue = UE.getEditor('editor', {
            toolbars: [
                [
                    'undo', //撤销
                    'bold', //加粗
                    'underline', //下划线
                    'preview', //预览
                    'horizontal', //分隔线
                    'inserttitle', //插入标题
                    'cleardoc', //清空文档
                    'fontfamily', //字体
                    'fontsize', //字号
                    'paragraph', //段落格式
                    'inserttable', //插入表格
                    'justifyleft', //居左对齐
                    'justifyright', //居右对齐
                    'justifycenter', //居中对
                    'justifyjustify', //两端对齐
                    'forecolor', //字体颜色
                    'insertcode' //插入代码
                ]
            ]
        });

        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer;

        var questionData = top.questionData;
        form.val('form', {
            'id': questionData.id,
            'keyWords':questionData.keyWords,
            'questionAnalysis': questionData.questionAnalysis
        })

        ue.addListener("ready", function () {
            //赋值
            ue.setContent(questionData.questionDescribe);
            //取值
            var content = ue.getContent();
        });

        /**
         * 加载状态
         */
        $.get("${base}/admin/system/dict/getDictByType/question_type", {}, function (data) {
            var html = "<option value=\"\">请选择题目类型</option>";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#questionType").html(html);
                //表单重新渲染
                form.render();
                form.val('form', {
                    'questionType': questionData.questionType
                });
            }
        }, "json");
        /**
         * 加载课程列表
         */
        $.post("${base}/chCourse/getCourseClass", function (data) {
            var html = "<option value=\"\">请选择课程</option>";
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].courseName + '</option>';
            }
            $("#courseId").html(html);
            //表单重新渲染
            form.render();
        }, "json");

        /**
         * 监听form-select事件
         */
        form.on("select(course)", function () {
            var courseId = $("#courseId").val();
            if (courseId == "") return false;
            /**
             * 加载单元列表
             */
            $.post("${base}/chUnit/getUnitClass", {courseId: courseId}, function (data) {
                var html = "<option value=\"\">请选择单元</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].unitName + '</option>';
                }
                $("#unitId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });
        /**
         * 监听form-select事件
         */
        form.on("select(unit)", function () {
            var unitId = $("#unitId").val();
            if (unitId == "") {
                return false;
            }
            /**
             * 加载章节列表
             */
            $.post("${base}/chSection/getSectionClass", {unitId: unitId}, function (data) {
                var html = "<option value=\"\">请选择章节</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].sectionName + '</option>';
                }
                $("#sectionId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });
        /**
         * 监听form-select事件
         */
        form.on("select(section)", function () {
            var sectionId = $("#sectionId").val();
            if (sectionId == "") {
                return false;
            }
            /**
             * 加载知识点列表
             */
            $.post("${base}/chLorePoint/getLorePoint", {"sectionId": sectionId}, function (data) {
                var html = "<option value=\"\">请选择技能点</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].lorePointName + '</option>';
                }
                $("#lorePointId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });


        form.on("submit(editQuestion)", function (data) {
            data.field.questionDescribe = UE.getEditor('editor').getContent();
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var editIndex = parent.layer.getFrameIndex(window.name);
            $("#selectvedio").addClass('layui-btn-disabled').attr('disabled', "true");
            $.post("${base}/chQuestion/edit", data.field, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg("编辑成功！", {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>