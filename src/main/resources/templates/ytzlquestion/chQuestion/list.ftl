<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-inline">
                <div class="layui-input-inline">
                    <select name="courseId" id="courseId" lay-filter="course">
                        <option value="">选择课程</option>
                    </select>
                </div>
                <span>-</span>
                <div class="layui-input-inline">
                    <select name="unitId" id="unitId" lay-filter="unit">
                        <option value="">请先选择课程</option>
                    </select>
                </div>
                <span>-</span>
                <div class="layui-input-inline">
                    <select name="sectionId" id="sectionId" lay-filter="section">
                        <option value="">请选择章节</option>
                    </select>
                </div>
                <span>-</span>
                <div class="layui-input-inline">
                    <select name="lorePointId" id="lorePointId" lay-filter="lorepoint">
                        <option value="">请选择技能点</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <select name="questionType" id="type" lay-filter="type">
                    <option value="">选择题目类型</option>
                </select>
            </div>
            <hr class="layui-bg-gray">
            <div class="layui-inline">
                <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="addChQuestion">新增试题</a>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="delAll">删除</a>
            </div>
           <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="excelin">excel导入</a>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="creationQuestion">创建试卷</a>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
    <script type="text/html" id="qType">
        {{#  if(d.questionType == 1){ }}
        <span class="layui-badge layui-bg-green">单选题</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-blue">多选题</span>
        {{#  } }}
    </script>

    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="answer">答案</a>
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>

<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;
        /**
         * 加载状态
         */
        $.get("${base}/admin/system/dict/getDictByType/question_type", {}, function (data) {
            var html = "<option value=\"\">请选择题目类型</option>";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#type").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");
        /**
         * 加载课程列表
         */
        $.post("${base}/chCourse/getCourseClass", {}, function (data) {
            var html = "<option value=\"\">请选择课程</option>";
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].courseName + '</option>';
            }
            $("#courseId").html(html);
            //表单重新渲染
            form.render();
        }, "json");

        /**
         * 监听form-select事件
         */
        form.on("select(course)", function () {
            var courseId = $("#courseId").val();
            if (courseId == "") return false;
            /**
             * 加载单元列表
             */
            $.post("${base}/chUnit/getUnitClass", {courseId: courseId}, function (data) {
                var html = "<option value=\"\">请选择单元</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].unitName + '</option>';
                }
                $("#unitId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });
        /**
         * 监听form-select事件
         */
        form.on("select(unit)", function () {
            var unitId = $("#unitId").val();
            if (unitId == "") {
                return false;
            }
            /**
             * 加载章节列表
             */
            $.post("${base}/chSection/getSectionClass", {unitId: unitId}, function (data) {
                var html = "<option value=\"\">请选择章节</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].sectionName + '</option>';
                }
                $("#sectionId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });
        /**
         * 监听form-select事件
         */
        form.on("select(section)", function () {
            var sectionId = $("#sectionId").val();
            if (sectionId == "") {
                return false;
            }
            /**
             * 加载知识点列表
             */
            $.post("${base}/chLorePoint/getLorePoint", {sectionId: sectionId}, function (data) {
                var html = "<option value=\"\">请选择技能点</option>";
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].lorePointName + '</option>';
                }
                $("#lorePointId").html(html);
                //表单重新渲染
                form.render();
            }, "json");
        });


        //监听工具条
        table.on('tool(demo)', function (obj) {
            var data = obj.data;
            if (obj.event === 'answer') {
                top.questionId = data.id;
                var editIndex = layer.open({
                    title: "试题答案",
                    type: 2,
                    content: "${base}/chChoice/list",
                    anim: 4,
                    area: ['700px', '360px']
            });
            }
            if (obj.event === 'edit') {
                top.questionData = data;
                var editIndex = layer.open({
                    title: "编辑试题",
                    type: 2,
                    content: "${base}/chQuestion/edit?id=" + data.id,
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(editIndex);
                });
                layer.full(editIndex);
            }
            if (obj.event === "del") {
                layer.confirm("你确定要删除该么？", {btn: ['是的,我确定', '我再想想']},
                    function () {
                        var loadIndex = layer.load(2, {
                            shade: [0.3, '#333']
                        });
                        $.post("${base}/chQuestion/delete", {"id": data.id}, function (res) {
                            layer.close(loadIndex);
                            if (res.success) {

                                layer.msg("删除成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            }

        });

        var t = {
            elem: '#test',
            url: '${base}/chQuestion/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3,10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type: 'checkbox'},
                {field: 'id', title: '试题ID', width: '12%'},
                {field: 'keyWords', title: '题目标题', width: '12%'},
                {field: 'lorePointId', title: '所属知识点', width: '12%'},
                {field: 'questionDescribe', title: '试题描述', width: '12%'},
                {field: 'questionType', title: '试题类型', width: '12%', templet: '#qType'},
                {field: 'questionAnalysis', title: '试题分析', width: '12%'},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true
                },
                {
                    field: 'modifyedDate',
                    title: '修改时间',
                    width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.modifyedDate) }}</div>',
                    unresize: true
                },
                //单元格内容水平居中
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);

        var active = {
            addChQuestion: function () {
                var addIndex = layer.open({
                    title: "试题添加",
                    type: 2,
                    content: "${base}/chQuestion/add",
                    success: function (layero, addIndex) {
                            layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                                tips: 3
                            });
                    }
                });

                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(addIndex);
                });
                layer.full(addIndex);
            },
            delAll: function () {
                var checkStatus = table.checkStatus('test'),
                    data = checkStatus.data, ids = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        ids += data[i].id;
                        if (i < data.length - 1) {
                            ids += ",";
                        }
                    }

                }
                layer.confirm("确定批量删除？", {btn: ['是的,我确定', '我再想想']},
                    function () {
                        var loadIndex = layer.load(2, {
                            shade: [0.3, '#333']
                        });
                        $.post("${base}/chQuestion/deleteAll", {"ids": ids}, function (res) {
                            layer.close(loadIndex);
                            if (res.success) {
                                layer.msg("删除成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            },
            excelin: function () {
                var excelinIndex = layer.open({
                    title: "批量导入试题--请遵循格式",
                    type: 2,
                    content: "${base}/excel/toImportExcel",
                    area:['300px','350px']
                });
            },
            creationQuestion:function () {
                var creatExam = layer.open({
                    title: "出题",
                    type: 2,
                    content: "${base}/chQuestion/creatExam",
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(creatExam);
                });
                layer.full(creatExam);
            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>