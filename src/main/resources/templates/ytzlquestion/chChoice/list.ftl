<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <div class="layui-input-inline">
        <input class="layui-input" id="describe" autocomplete="off" type="text" style="width: 570px"
               placeholder="请输入选项描述">
    </div>
    <div class="layui-inline">
        <button class="layui-btn layui-btn-normal" id="addChoice" data-type="addChoice">添加选项</button>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
    <script type="text/html" id="isAnswer">
        {{#  if(d.isAnswer == 1){ }}
        <span class="layui-badge layui-bg-green">正确</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-gray">错误</span>
        {{#  } }}
    </script>

    <script type="text/html" id="barDemo">
        {{#  if(d.isAnswer == 2){ }}
        <a class="layui-btn  layui-btn-radius layui-btn-xs" lay-event="correct">设为正确</a>
        {{#  } else { }}
        <a class="layui-btn  layui-btn-radius layui-btn-warm layui-btn-xs" lay-event="wrong">设为错误</a>
        {{#  } }}
        <a class="layui-btn layui-btn-radius layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>
<div class="layui-input-inline" id="erroInfo"></div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;
        var questionId = top.questionId;
        //监听工具条
        table.on('tool(demo)', function (obj) {
            var data = obj.data;
            if (obj.event === "correct") {
                layer.confirm("将该选项设为正确答案？", {btn: ['是的,我确定', '我再想想']},
                    function () {
                        $.post("${base}/chChoice/correct", {"id": data.id}, function (res) {
                            if (res.success) {
                                layer.msg("设置成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            }
            if (obj.event === "wrong") {
                $.post("${base}/chChoice/wrong", {"id": data.id}, function (res) {
                    if (res.success) {
                        layer.msg("设置成功", {time: 1000}, function () {
                            table.render(t);
                        });
                    } else {
                        layer.msg(res.message);
                    }

                });
            }
            if (obj.event === "del") {
                $.post("${base}/chChoice/delete", {"id": data.id}, function (res) {
                    if (res.success) {
                        layer.msg("删除成功", {time: 1000}, function () {
                            table.render(t);
                        });
                    } else {
                        layer.msg(res.message);
                    }
                });
            }
        });

        var t = {
            elem: '#test',
            url: '${base}/chChoice/list',
            where: {questionId: questionId},
            method: 'post',
            // cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                // {field: 'id', title: '选项ID'},
                {field: 'choiceDescribe', title: '选项描述'},
                {field: 'isAnswer', title: '状态', width: "10%", templet: '#isAnswer'},
                {fixed: 'right', title: '操作', width: "20%", align: 'center', toolbar: '#barDemo'}
            ]],
            done: function (res, curr, count) {
                //如果是异步请求数据方式，res即为你接口返回的信息。
                //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
                //console.log(res);
                //得到当前页码
                //console.log(curr);
                //得到数据总量
                //console.log(count);
                switch (count) {
                    case 1:
                    case 2:
                    case 3:
                        $("#erroInfo").html("试题应有四个选项，请补充！！！");
                        $("#addChoice").removeClass('layui-btn-disabled').removeAttr('disabled');
                        break;
                    case 4:
                        $("#addChoice").addClass('layui-btn-disabled').attr('disabled', "true");
                        $("#erroInfo").html("");
                        break;
                    default:
                        $("#addChoice").addClass('layui-btn-disabled').attr('disabled', "true");
                        $("#erroInfo").html("");
                }
                var num = 0;
                var type;
                for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].isAnswer == 1) {
                        num++;
                    }
                }
                if (num < 1) {
                    $("#erroInfo").html("至少给一个正确选项吧！");
                    return false;
                }
                if (num >= 2) type = 2;
                else type = 1;

                $.get("${base}/chQuestion/getType?questionId=" + questionId, function (data) {
                    if (data.data != type) {
                        $("#erroInfo").html("该题是" + (data.data == 1 ? "单选题" : "多选题") + "，请核对选项！！！");
                    } else {
                        $("#erroInfo").html("");
                    }
                })
            }
        };
        table.render(t);

        var active = {
            addChoice: function () {
                var info = $("#describe").val();
                if (info == "" || info == null) {
                    layer.msg("选项内容不能为空！", {anim: 6, time: 500});
                    return false;
                }
                $("#addChoice").addClass('layui-btn-disabled').attr('disabled', "true");
                var loadIndex = layer.load(2, {
                    shade: [0.3, '#333']
                });
                $.post("${base}/chChoice/add", {
                    "questionId": questionId,
                    "choiceDescribe": $("#describe").val()
                }, function (res) {
                    layer.close(loadIndex);
                    if (res.success) {
                        layer.msg("添加成功", {time: 1000}, function () {
                            location.reload();
                        });
                    } else {
                        layer.msg(res.message);
                    }

                });
            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });


    });
</script>
</body>
</html>