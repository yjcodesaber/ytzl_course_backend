<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style type="text/css">
        .layui-form-item .layui-inline {
            width: 33.333%;
            float: left;
            margin-right: 0;
        }

        @media (max-width: 1240px) {
            .layui-form-item .layui-inline {
                width: 100%;
                float: none;
            }
        }

        .layui-form-item .role-box {
            position: relative;
        }

        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
    <div class="layui-form-item">
        <label class="layui-form-label">学校</label>
        <div class="layui-input-block">

            <select id="schoolId" name="schoolId" lay-verify="required">
                <option value="">请选择学校</option>
            </select>

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">院系</label>
        <div class="layui-input-block">

            <input type="text" class="layui-input" placeholder="请填写院系" autocomplete="off" name="departmentName" id="departmentName"
                   lay-verify="required">

        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">系主任</label>
        <div class="layui-input-block">

            <select id="deanId" name="deanId" lay-verify="required">
                <option value=''>请选择系主任</option>
            </select>
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChDepartment">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer'], function () {
        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer;


        //加载系主任
        $.post("${base}/admin/system/user/getOwnName", {name: "系主任"}, function (data) {
            var html = "<option value=''>请选择系主任</option>";
            if (data != null && data != "") {
                for (var i = 0; i < data.length; i++) {
                    html += "<option value='" + data[i].id + "'>" + data[i].nickName + "</option>";
                }
                $("#deanId").html(html);
                form.render();
            }
        }, "json");

        //查询学校列表
        $.post("${base}/school/getSchoolName", {}, function (data) {
            var html = '<option value="">请选择学校</option>';
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].schoolName + '</option>'
            }
            $("#schoolId").html(html);
            form.render();
        });


        form.on("submit(addChDepartment)", function (data) {

            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var addIndex = parent.layer.getFrameIndex(window.name);
            $.post("${base}/department/add", data.field, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg("添加成功！", {time: 1000}, function () {
                        parent.layer.close(addIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>