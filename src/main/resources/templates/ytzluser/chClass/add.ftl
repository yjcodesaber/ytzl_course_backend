<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style type="text/css">
        .layui-form-item .layui-inline {
            width: 33.333%;
            float: left;
            margin-right: 0;
        }

        @media (max-width: 1240px) {
            .layui-form-item .layui-inline {
                width: 100%;
                float: none;
            }
        }

        .layui-form-item .role-box {
            position: relative;
        }

        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <div class="layui-form-item">
        <label class="layui-form-label">学校</label>
        <div class="layui-input-block">
            <select name="schoolId" id="schoolId" lay-verify="required" lay-filter="school">
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">院系名称</label>
        <div class="layui-input-block">
            <select name="departmentId" id="departmentId" lay-verify="required" >
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">班级名称</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="className" id="className" value="" placeholder="填写规范：2019届java就业班"
                   lay-verify="required">
            <input type="hidden" name="id" id="id">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">教师</label>
        <div class="layui-input-block">
            <select name="teacherId" id="teacherId" lay-verify="required">
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">班主任</label>
        <div class="layui-input-block">
            <select name="headTeacherId" id="headTeacherId" lay-verify="required">
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChClass">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer'], function () {
        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer;

        //加载学校
        $.post("${base}/school/getSchoolName", {}, function (data) {
            var html = "<option value=''>请选择所属学校</option>";
            if (data!=null && data!="") {
                for (var i = 0; i < data.length; i++) {
                    html += "<option value='" + data[i].id + "'>" + data[i].schoolName + "</option>";
                }
                $("#schoolId").html(html);
                form.render();

            }
        }, "json");

        //加载院系
        form.on("select(school)",function () {
            var schoolId=$("#schoolId").val();
            if(schoolId!=null && schoolId!=""){
                $.post("${base}/department/getDepartmentName", {schoolId:schoolId}, function (data) {
                    var html = "<option value=''>请选择所属学校</option>";
                    if (data!=null && data!="") {
                        for (var i = 0; i < data.length; i++) {
                            html += "<option value='" + data[i].id + "'>" + data[i].departmentName + "</option>";
                        }
                        $("#departmentId").html(html);
                        form.render();

                    }
                }, "json");
            }
        });


        //加载教员
        $.post("${base}/admin/system/user/getOwnName", {name:"教师"}, function (data) {
            var html = "<option value=''>请选择班级教员</option>";
            if (data!=null && data!="") {
                for (var i = 0; i < data.length; i++) {
                    html += "<option value='" + data[i].id + "'>" + data[i].nickName + "</option>";
                }
                $("#teacherId").html(html);
                form.render();
            }
        }, "json");
        //加载班主任
        $.post("${base}/admin/system/user/getOwnName", {name:"班主任"}, function (data) {
            var html = "<option value=''>请选择班主任</option>";
            if (data!=null && data!="") {
                for (var i = 0; i < data.length; i++) {
                    html += "<option value='" + data[i].id + "'>" + data[i].nickName + "</option>";
                }
                $("#headTeacherId").html(html);
                form.render();
            }
        }, "json");


        form.on("submit(addChClass)", function (data) {

            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var addIndex=parent.layer.getFrameIndex(window.name);
            $.post("${base}/class/add", data.field, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg("添加成功！", {time: 1000}, function () {
                        parent.layer.close(addIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>