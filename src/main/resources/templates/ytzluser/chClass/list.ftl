<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-inline" style="margin-left: 15px">
                <label>查询条件:</label>
                <div class="layui-input-inline">
                    <input type="text" value="" autocomplete="off" name="searchInfo" placeholder="班级名称"
                           class="layui-input search_input">
                </div>
            </div>
            <div class="layui-inline" style="margin-left: 15px">
                <div class="layui-input-inline">
                    <select id="schoolId" name="schoolId" lay-filter="school">

                    </select>
                </div>
            </div>
            <div class="layui-inline" style="margin-left: 15px">
                <div class="layui-input-inline">
                    <select id="departmentId" name="departmentId">

                    </select>
                </div>
            </div>

            <div class="layui-inline">
                <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="addChClass">添加</a>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;



        $.post("${base}/school/getSchoolName", {}, function (data) {
            var html = '<option value="">请选择学校名称</option>';
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].schoolName + '</option>';
            }
            $("#schoolId").html(html);
            form.render();

        }, "json");

        form.on("select(school)", function () {
            var schoolId = $("#schoolId").val();
            if (schoolId == ""){
                var html = '<option value="">请选择院系名称</option>';
                $("#departmentId").html(html);
                form.render();
            }
            $.post("${base}/department/getDepartmentName", {schoolId: schoolId}, function (data) {
                var html = '<option value="">请选择院系名称</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].departmentName + '</option>';
                }
                $("#departmentId").html(html);
                form.render();

            }, "json");
        });

        //监听工具条
        table.on('tool(demo)', function (obj) {
            var data = obj.data;
            if (obj.event === 'edit') {
                top.classInfo = data;
                var editIndex = layer.open({
                    title: "编辑",
                    type: 2,
                    content: "${base}/class/edit?id=" + data.id,
                    area: ['600px', '400px']
                });

            }
            if (obj.event === "del") {
                layer.confirm("你确定要删除该么？", {btn: ['是的,我确定', '我再想想']},
                    function () {
                        $.post("${base}/class/delete", {"id": data.id}, function (res) {
                            if (res.success) {
                                layer.msg("删除成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            }
        });



        var t = {
            elem: '#test',
            url: '${base}/class/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3,10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type: 'checkbox'},
                {field: 'id', title: '班级编号'},
                {field: 'className', title: '班级名称', width: '12%'},
                {field: 'departmentName', title: '院系名称', width: '12%'},
                {field: 'schoolName', title: '学校名称', width: '12%'},
                {field: 'teacherName', title: '教员', width: '12%'},
                {field: 'headTeacherName', title: '班主任', width: '12%'},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {field:'creationBy',    title: '创建人',width:'12%'},
                {
                    field: 'modifyedDate',
                    title: '修改时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.modifyedDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {field:'modifyedBy',    title: '修改人',width:'12%'},
                {fixed: 'right', title: '操作', width: '12%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);

        var active = {
            addChClass: function () {
                var addIndex = layer.open({
                    title: "添加",
                    type: 2,
                    content: "${base}/class/add",
                    area: ['600px', '400px']

                });
            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>