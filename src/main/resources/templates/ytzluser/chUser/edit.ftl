<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <#--    <style type="text/css">-->
    <#--        .layui-form-item .layui-inline {-->
    <#--            width: 33.333%;-->
    <#--            float: left;-->
    <#--            margin-right: 0;-->
    <#--        }-->

    <#--        @media (max-width: 1240px) {-->
    <#--            .layui-form-item .layui-inline {-->
    <#--                width: 100%;-->
    <#--                float: none;-->
    <#--            }-->
    <#--        }-->

    <#--        .layui-form-item .role-box {-->
    <#--            position: relative;-->
    <#--        }-->

    <#--        .layui-form-item .role-box .jq-role-inline {-->
    <#--            height: 100%;-->
    <#--            overflow: auto;-->
    <#--        }-->
    <#--    </style>-->
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <input name="id" type="hidden">
    <div class="layui-form-item">
        <label class="layui-form-label">学号</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" lay-verify="required|number" autocomplete="off" name="userNumber" id="userNumber">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">手机号</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" lay-verify="required|phone" name="userAccount" id="userAccount">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" lay-verify="required" autocomplete="off" name="realName" id="realName">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">身份证号</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" lay-verify="identity" autocomplete="off" name="idCard" id="idCard">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">入学时间</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="inSchoolDate" id="inSchoolDate" readonly>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">毕业时间</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="outSchoolDate" id="outSchoolDate" readonly>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" type="button" id="updateData" lay-submit="" lay-filter="updateData">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer','laydate'], function () {
        var form = layui.form,
            $ = layui.jquery,
            laydate = layui.laydate,
            layer = layui.layer;
        var stuData = top.studentData;

        form.val("form", {
            "userNumber": stuData.userNumber,
            "userAccount": stuData.userAccount,
            "realName": stuData.realName,
            "idCard": stuData.idCard,
            "inSchoolDate": setTime(stuData.inSchoolDate),
            "outSchoolDate": setTime(stuData.outSchoolDate)
        });


        // 时间选择器初始化 开始时间
        laydate.render({
            elem: '#inSchoolDate',
            trigger: 'click',
            format: 'yyyy-MM-dd',
            done: function (value, date, endDate) {
                var startDate = new Date(value).getTime();
                var endTime = new Date($('#outSchoolDate').val()).getTime();
                if (endTime < startDate) {
                    layer.msg('毕业时间不能小于入学时间');
                    $('#inSchoolDate').val($('#outSchoolDate').val());
                }
            }
        });
        laydate.render({ //结束时间
            elem: '#outSchoolDate',
            trigger: 'click',
            format: 'yyyy-MM-dd',
            done: function (value, date, endDate) {
                var startDate = new Date($('#inSchoolDate').val()).getTime();
                var endTime = new Date(value).getTime();
                if (endTime < startDate) {
                    layer.msg('毕业时间不能小于入学时间');
                    $('#outSchoolDate').val($('#inSchoolDate').val());
                }
            }
        });


        form.on("submit(updateData)", function (data) {
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            //给角色赋值
            var editIndex = parent.layer.getFrameIndex(window.name);
            var userNumber = $('#userNumber').val();
            var userAccount = $('#userAccount').val();
            var realName = $('#realName').val();
            var idCard = $('#idCard').val();
            var inSchoolDate = $('#inSchoolDate').val();
            var outSchoolDate = $('#outSchoolDate').val();
            $.post("${base}/chUser/editor", {
                id: stuData.id,
                userNumber: userNumber,
                userAccount: userAccount,
                realName:realName,
                idCard:idCard,
                inSchoolDate:inSchoolDate,
                outSchoolDate:outSchoolDate
            }, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg(res.message, {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });


    });
</script>
<script type="text/javascript">
    /**13位时间戳转换成 年月日 上午 时间  2018-12-27 15：35 */
    function setTime(v) {
        if (v == null) {
            return null;
        }
        return new Date(parseInt(v)).toLocaleString()
    }

    Date.prototype.toLocaleString = function () {
        var y = this.getFullYear();
        var m = this.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = this.getDate();
        d = d < 10 ? ("0" + d) : d;
        var h = this.getHours();
        h = h < 10 ? ("0" + h) : h;
        var M = this.getMinutes();
        M = M < 10 ? ("0" + M) : M;
        var S = this.getSeconds();
        S = S < 10 ? ("0" + S) : S;
        return y + "-" + m + "-" + d;
        //返回的时间格式 2018-12-27 15：35 + " " + h + ":" + M + ":" + S
        //自定义时间格式直接更改return格式即可
    };
</script>
</body>
</html>