<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
</head>
<body class="childrenBody">
<form class="layui-form" style="width:80%;">
    <div class="layui-form-item">
        <label class="layui-form-label">班级</label>
        <div class="layui-input-block">
            <select id="className" lay-filter="className" lay-verify="required">
                <option value="">请选择要分配的班级</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="updateClass">立即分配</button>
        </div>
    </div>
</form>

<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer', 'laydate'], function () {
        var form = layui.form,
            $ = layui.jquery,
            laydate = layui.laydate,
            layer = layui.layer;
        var stuData = top.studentData;


        //动态班级信息
        $.get("/chTask/selectClassName", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择要分配的班级</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].id + '">' + data.data[i].className + '</option>';
                }
                $("#className").html(html);
                form.render('select')
            }
        }, "json");


        form.on("submit(updateClass)", function (data) {
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var activeIndex=parent.layer.getFrameIndex(window.name);
            $.post("${base}/chUser/distributionClass", {
                ids:top.ids,
                classId:$("#className").val()
            }, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg(res.message, {time: 1000}, function () {
                        parent.layer.close(activeIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });


    });
</script>
</body>
</html>