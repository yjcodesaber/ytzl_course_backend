<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-inline" style="margin-left: 15px">
                <label>查询条件:</label>
                <div class="layui-input-inline">
                    <input type="text" value="" autocomplete="off" name="select_realInfo" placeholder="学号-姓名-账号"
                           class="layui-input search_input">
                </div>
            </div>
            <#--根据学校查找-->
            <div class="layui-inline">
                <select id="schoolId" name="schoolId" lay-filter="school">
                    <option value="">请选择学校</option>
                </select>
            </div>
            <#--根据院系查找-->
            <div class="layui-inline">
                <select id="departmentId" name="departmentId" lay-filter="department">
                    <option value="">请选择院系</option>
                </select>
            </div>
            <#--根据班级查找-->
            <div class="layui-inline">
                <select id="classId" name="classId" lay-filter="class">
                    <option value="">请选择班级</option>
                </select>
            </div>
            <div class="layui-inline-block" style="margin-left: 80px;padding-top: 15px">
                <div class="layui-inline">
                    <a class="layui-btn" lay-submit="" lay-filter="searchForm">搜索</a>
                </div>
                <div class="layui-inline">
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
                <div class="layui-inline">
                    <a class="layui-btn layui-btn-normal" data-type="acti">激活</a>
                </div>
                <div class="layui-inline">
                    <a class="layui-btn layui-btn-normal" data-type="distribute">分配课程</a>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn" type="button" id="getCheckData" data-type="getCheckData">导出Excel</button>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn layui-bg-orange" type="button" id="getClassStu" data-type="getClassStu">
                        分配班级
                    </button>
                </div>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
    <script type="text/html" id="active">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.isActivated == 1){ }}
        <span class="layui-badge layui-bg-green">已激活</span>
        {{#  } else if(d.isActivated ==2){ }}
        <span class="layui-badge layui-bg-blue">未激活</span>
        {{#  } }}

    </script>

    <script type="text/html" id="barDemo">
        {{#  if(d.isActivated == 1){ }}
        <a class="layui-btn layui-btn-danger layui-btn-xs" id="undo-btn" lay-event="undo">禁用</a>
        <a class="layui-btn layui-btn-warm layui-btn-xs" id="detail-btn" lay-event="detail">课程详情</a>
        {{#  } else {d.isActivated == 2 }}
        <a class="layui-btn layui-btn-xs" id="wake-btn" lay-event="wake">启用</a>
        {{#  } }}
        <a class="layui-btn layui-bg-blue layui-btn-xs" id="editor-btn" lay-event="editor">编辑</a>
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            table = layui.table;

        $.post("${base}/school/getSchoolName", {}, function (data) {
            var html = '<option value="">请选择学校名称</option>';
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].id + '">' + data[i].schoolName + '</option>';
            }
            $("#schoolId").html(html);
            form.render();
        }, "json");

        form.on("select(school)", function () {
            var schoolId = $("#schoolId").val();
            if (schoolId == "") {
                var html = '<option value="">请选择院系名称</option>';
                $("#departmentId").html(html);
                var html1 = '<option value="">请选择班级名称</option>';
                $("#classId").html(html1);
                form.render();

            }
            $.post("${base}/department/getDepartmentName", {schoolId: schoolId}, function (data) {
                var html = '<option value="">请选择院系名称</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].departmentName + '</option>';
                }
                $("#departmentId").html(html);
                form.render();
            }, "json");
        });

        form.on("select(department)", function () {
            var departmentId = $("#departmentId").val();
            if (departmentId == "") {
                var html = '<option value="">请选择班级名称</option>';
                $("#classId").html(html);
                form.render();
            }
            $.post("${base}/class/getClassName", {departmentId: departmentId}, function (data) {
                var html = '<option value="">请选择班级名称</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].className + '</option>';
                }
                $("#classId").html(html);
                form.render();
            }, "json");
        });


        //监听工具条
        table.on('tool(demo)', function (obj) {
            top.studentData = obj.data;
            var data = obj.data;
            if (obj.event === 'wake') {
                if (data.inSchoolDate != null && data.inSchoolDate != "") {
                    //按钮【按钮一】的回调
                    $.post("${base}/chUser/wake", {"id": data.id, "wake": true}, function (res) {
                        if (res.success) {
                            layer.msg("激活成功！", {time: 1000}, function () {
                                table.render(t);
                            });
                        } else {
                            var activeindex = layer.open({
                                title: "激活",
                                type: 2,
                                content: "${base}/chUser/wake",
                                area: ['600px', '350px']
                            });
                        }
                    });
                } else {
                    layer.msg("未激活学生不能直接启用，请先激活该学生！");
                }

            }
            if (obj.event === "undo") {
                $.post("${base}/chUser/wake", {"id": data.id, "wake": false}, function (res) {
                    if (res.success) {
                        layer.msg("禁用成功", {time: 1000}, function () {
                            table.render(t);
                        });
                    } else {
                        layer.msg("禁用失败:" + res.message);
                    }
                });
            }

            if (obj.event === 'detail') {
                top.userId = data.id;
                var detailIndex = layer.open({
                    title: data.realName + "的课程",
                    type: 2,
                    content: "${base}/userLinkCourse/detail",
                    area: ['600px', '350px']
                });
            }
            var editorIndex
            if (obj.event === 'editor') {
                 editorIndex = layer.open({
                    title: '编辑',
                    type: 2,
                    content: "${base}/chUser/editor",
                    area: ['600px', '350px'],
                    anim: 3,
                    success: function (layero, addTask) {
                        table.render();
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(editorIndex);
                });
                layer.full(editorIndex);
            }
        });

        var t = {
            elem: '#test',
            url: '${base}/chUser/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type: 'checkbox'},
                {field: 'id', title: '学生编号', width: '12%'},
                {field: 'userNumber', title: '学生学号', width: '12%'},
                {field: 'userAccount', title: '手机号', width: '12%'},
                {field: 'realName', title: '姓名', width: '12%'},
                {field: 'idCard', title: '身份证号', width: '12%'},
                {field: 'className', title: '班级名称', width: '12%'},
                {field: 'departmentName', title: '院系名称', width: '12%'},
                {field: 'schoolName', title: '学校名称', width: '12%'},
                {
                    field: 'inSchoolDate',
                    title: '入学时间',
                    width: '15%',
                    templet: '<div>{{ setTime(d.inSchoolDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {
                    field: 'outSchoolDate',
                    title: '毕业时间',
                    width: '15%',
                    templet: '<div>{{ setTime(d.outSchoolDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {field: 'isActivated', title: '激活状态', width: '12%', templet: "#active"},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);


        var active = {
            acti: function () {
                var checkStatus = table.checkStatus('test'),
                    data = checkStatus.data;
                var ids = "";
                if (data != null && data != "") {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].isActivated == 1) continue;
                            ids += data[i].id;
                            if (i < data.length - 1) {
                                ids += ",";
                            }
                        }
                        if (ids == "" || ids == null) {
                            layer.msg("所选学生全部为激活状态,请重新选择");
                            return false;
                        }
                        top.ids = ids;
                    }

                    var activeindex = layer.open({
                        title: "激活",
                        type: 2,
                        content: "${base}/chUser/active",
                        area: ['600px', '500px']
                    });
                } else {
                    layer.msg("至少选一个激活学生吧！");
                }

            },
            distribute: function () {
                var checkStatus = table.checkStatus('test'),
                    data = checkStatus.data;
                var ids = "";
                if (data != "") {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].isActivated == 2) {
                                return layer.msg("未激活学生不能分配课程,请重新选择");
                            }
                            ids += data[i].id;
                            if (i < data.length - 1) {
                                ids += ",";
                            }
                        }
                        top.ids = ids;
                    }
                    var distributeIndex = layer.open({
                        title: "分配课程",
                        type: 2,
                        content: "${base}/userLinkCourse/add",
                        area: ['600px', '350px']
                    });
                } else {
                    layer.msg("至少选一个学生吧！");
                }

            },
            getCheckData: function () { //获取选中数据
                var checkStatus = table.checkStatus('test')
                    , data = checkStatus.data;
                var tableData = [];
                if (data.length <= 0) {
                    layer.msg("请选择要导出的数据", {time: 1000});
                } else {
                    for (var i = 0; i < data.length; i++) {
                        var isActivated = '';
                        if (data[i].isActivated === 1) {
                            isActivated = '已激活';
                        } else {
                            isActivated = '未激活';
                        }
                        tableData.push([isNotNull(data[i].userNumber), isNotNull(data[i].userAccount), isNotNull(data[i].realName),
                            isNotNull(data[i].idCard), isNotNull(data[i].className), isNotNull(data[i].departmentName),
                            isNotNull(data[i].schoolName), setTime(data[i].inSchoolDate), setTime(data[i].outSchoolDate),
                            isActivated, setTime(data[i].creationDate)]);
                    }
                    table.exportFile(['学生学号', '登录账号', '姓名',
                            '身份证号', '班级名称', '院系名称',
                            '学校名称', '入学时间', '毕业时间',
                            '激活状态', '创建时间'],
                        tableData
                        , 'csv');
                }
            },
            getClassStu: function () {
                var checkStatus = table.checkStatus('test'),
                    data = checkStatus.data;
                if (data.length <= 0) {
                    layer.msg("至少选择一个学生吧！", {time: 1000});
                } else {
                    var ids = "";
                    for (var i = 0; i < data.length; i++) {
                        ids += data[i].id;
                        if (i < data.length - 1) {
                            ids += ",";
                        }
                    }
                    top.ids = ids;
                    var getClassIndex = layer.open({
                        title: '分配班级',
                        type: 2,
                        content: "${base}/chUser/distributionClass",
                        area: ['600px', '350px'],
                        anim: 3
                    });
                }
            }

        };


        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        //点击导出Excel
        $('#getCheckData').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        //点击分配班级
        $('#getClassStu').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });


    });
</script>

<script>

    function isNotNull(d) {
        if (d == null) {
            return null;
        }
        return d;
    }

    /**13位时间戳转换成 年月日 上午 时间  2018-12-27 15：35 */
    function setTime(v) {
        if (v == null) {
            return null;
        }
        return new Date(parseInt(v)).toLocaleString()
    }

    Date.prototype.toLocaleString = function () {
        var y = this.getFullYear();
        var m = this.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = this.getDate();
        d = d < 10 ? ("0" + d) : d;
        var h = this.getHours();
        h = h < 10 ? ("0" + h) : h;
        var M = this.getMinutes();
        M = M < 10 ? ("0" + M) : M;
        var S = this.getSeconds();
        S = S < 10 ? ("0" + S) : S;
        return y + "-" + m + "-" + d;
        //返回的时间格式 2018-12-27 15：35 + " " + h + ":" + M + ":" + S
        //自定义时间格式直接更改return格式即可
    };
</script>
</body>
</html>