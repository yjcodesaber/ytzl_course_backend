<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-inline" style="margin-left: 15px">
                <label>学生信息:</label>
                <div class="layui-input-inline">
                    <input type="text" value="" autocomplete="off" name="searchInfo" placeholder="请输入学生名称或学号"
                           class="layui-input search_input">
                </div>
            </div>
            <div class="layui-inline">
                <div class="layui-input-inline">
                    <select name="paymentMode" id="paymentMode" lay-filter="course">
                        <option value="">付款方式</option>
                    </select>
                </div>
                <span>-</span>
                <div class="layui-input-inline">
                    <select name="paymentType" id="paymentType" lay-filter="unit">
                        <option value="">付款类型</option>
                    </select>
                </div>
                <span>-</span>
                <div class="layui-input-inline">
                    <select name="paymentTerm" id="paymentTerm" lay-filter="section">
                        <option value="">学期</option>
                    </select>
                </div>

            </div>

            <hr class="layui-bg-gray">
            <div class="layui-inline">
                <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="addChPayment">添加</a>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="outExcel">导出Excel</a>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
    <script type="text/html" id="payPstatus">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.paymentPrincipalStatus == 1){ }}
        <span class="layui-badge layui-bg-green">已审核</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-red">未审核</span>
        {{#  } }}
    </script>

    <script type="text/html" id="paySstatus">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if (d.paymentStudentStatus == 1){ }}
        <span class="layui-badge layui-bg-green">已审核</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-red">未审核</span>
        {{#  } }}
    </script>

    <!-- 支付类型-->
    <script type="text/html" id="payType">
        {{#  if (d.paymentType == 1){ }}
        <span class="layui-badge-rim layui-bg-cyan">一次付清</span>
        {{#  } else { }}
        <span class="layui-badge-rim">分期付款</span>
        {{#  } }}
    </script>
    <!--学期显示 -->
    <script type="text/html" id="payTerm">
        {{#  if (d.paymentTerm == 1){ }}
        <span class="layui-badge layui-bg-blue">大一上学期</span>
        {{#  } else if (d.paymentTerm == 2){ }}
        <span class="layui-badge layui-bg-blue">大一下学期</span>
        {{#  } else if (d.paymentTerm == 3){ }}
        <span class="layui-badge layui-bg-blue">大二上学期</span>
        {{#  } else if (d.paymentTerm == 4){ }}
        <span class="layui-badge layui-bg-blue">大二下学期</span>
        {{#  } else if (d.paymentTerm == 5){ }}
        <span class="layui-badge layui-bg-blue">大三上学期</span>
        {{#  } else if (d.paymentTerm == 6){ }}
        <span class="layui-badge layui-bg-blue">大三下学期</span>
        {{#  } else if (d.paymentTerm == 7){ }}
        <span class="layui-badge layui-bg-blue">大四上学期</span>
        {{#  } else if (d.paymentTerm == 8){ }}
        <span class="layui-badge layui-bg-blue">大四下学期</span>
        {{#  } else { }}
        <span class="layui-badge layui-bg-blue">其他</span>
        {{#  }  }}
    </script>
    <!-- 支付方式-->
    <script type="text/html" id="payMode">
        {{#  if (d.paymentMode == 1){ }}
        <span class="">支付宝</span>
        {{#  } else if (d.paymentMode == 2){ }}
        <span class="">微信</span>
        {{#  } else if (d.paymentMode == 3){ }}
        <span class="">银行卡</span>
        {{#  } else if (d.paymentMode == 4){ }}
        <span class="">现金</span>
        {{#  } else if (d.paymentMode == 5){ }}
        <span class="">学校</span>
        {{#  }}}
    </script>
    <script type="text/html" id="barDemo">
        <@shiro.hasAnyRoles name="系主任,系统管理员">
            <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="examine">审核</a>
        </@shiro.hasAnyRoles>
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>

    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;

        /**
         * 加载支付方式
         */
        $.get("${base}/admin/system/dict/getDictByType/pay_mode", {}, function (data) {
            var html = "<option value=''>请选择支付方式</option>";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#paymentMode").html(html);
                //表单重新渲染
                form.render();
            }

        }, "json");


        /**
         * 加载支付类型
         */
        $.get("${base}/admin/system/dict/getDictByType/pay_type", {}, function (data) {
            var html = "<option value=''>请选择支付类型</option>";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#paymentType").html(html);
                //表单重新渲染
                form.render();

            }

        }, "json");

        /**
         * 加载学期
         */
        $.get("${base}/admin/system/dict/getDictByType/school_term", {}, function (data) {
            var html = "<option value=''>请选择付款学期</option>";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#paymentTerm").html(html);
                //表单重新渲染
                form.render();
            }

        }, "json");
        //监听工具条
        table.on('tool(demo)', function (obj) {
            var data = obj.data;
            var editIndex;
            if (obj.event === 'edit') {
                top.paymentInfo = data;
                    editIndex = layer.open({
                    title: "编辑",
                    type: 2,
                    content: "${base}/chPayment/toedit"
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(editIndex);
                });
                layer.full(editIndex);
            }
            if (obj.event === "del") {
                layer.confirm("你确定要删除该么？", {btn: ['是的,我确定', '我再想想']},
                    function () {
                        $.post("${base}/chPayment/delete", {"id": data.id}, function (res) {
                            if (res.success) {
                                layer.msg("删除成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            }
            if (obj.event === "examine") {
                layer.confirm("审核通过？", {btn: ['是', '否']},
                    function () {
                        $.post("${base}/chPayment/examine", {"id": data.id}, function (res) {
                            if (res.success) {
                                layer.msg("审核成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }
                        });
                    }
                )
            }
        });
        var exportData = [];
        var html = "";
        var html2 = "";
        var realNameUser = "";
        var t = {
            elem: '#test',
            url: '${base}/chPayment/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {fixed: 'left', type: 'checkbox'},
                {field: 'userNumber', width: "10%", title: '学生学号'},
                {field: 'realName', width: "10%", title: '学生姓名'},
                {field: 'paymentMode', width: "10%", title: '支付方式', templet: "#payMode"},
                {
                    field: 'receivablesId', width: "15%", title: '账户详情', templet: function (data) {
                        if (data.receivablesId==null||data.receivablesId.trim()=="")
                            return "<div>暂无账户</div>"
                        else
                            return "<div>"+data.receivablesName+"("+data.receivablesAccount+")</div>"
                    }

                },
                {field: 'paymentType', width: "10%", title: '支付类型', templet: "#payType"},
                {field: 'paymentTerm', width: "10%", title: '支付学期', templet: "#payTerm"},
                {
                    field: 'paymentMoney', width: "10%", title: '本次缴费金额(元)', templet: function (d) {
                        return "<div>" + d.paymentMoney / 100 + "</div> "
                    }
                },
                {
                    field: 'paymentSpareMoney', width: "10%", title: '剩余缴费金额(元)', templet: function (d) {
                        return "<div>" + d.paymentSpareMoney / 100 + "</div> "
                    }
                },
                {
                    field: 'paymentNowTime',
                    width: "14%",
                    title: '支付时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.paymentNowTime) }}</div>'
                },
                {
                    field: 'paymentNextTime',
                    width: "14%",
                    title: '下次缴费时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.paymentNextTime) }}</div>'
                },


                {field: 'paymentPrincipalStatus', width: "10%", title: '校长审核', templet: "#payPstatus"},
                {field: 'paymentStudentStatus', width: "10%", title: '学生审核', templet: "#paySstatus"},
                {
                    field: 'creationDate',
                    width: "12%",
                    title: '创建时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);
        var addIndex;
        var active = {
            addChPayment: function () {
                 addIndex = layer.open({
                    title: "添加",
                    type: 2,
                    content: "${base}/chPayment/add"
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(addIndex);
                });
                layer.full(addIndex);
            },
            outExcel: function () {
                exportData = table.checkStatus('test'),
                    data = exportData.data;
                var tableData = [];
                if (data.length <= 0) {
                    return layer.msg("请选择需要导出的数据！");
                } else {
                    for (var i = 0; i < data.length; i++) {
                        //支付学期
                        var schoolTerm = '';
                        if (data[i].schoolTerm === 1) {
                            schoolTerm = '大一上学期';
                        } else if (data[i].schoolTerm === 2) {
                            schoolTerm = '大一下学期';
                        } else if (data[i].schoolTerm === 3) {
                            schoolTerm = '大二上学期';
                        } else if (data[i].schoolTerm === 4) {
                            schoolTerm = '大二下学期';
                        } else if (data[i].schoolTerm === 5) {
                            schoolTerm = '大三上学期';
                        } else {
                            schoolTerm = '大三下学期';
                        }
                        //支付方式
                        var paymentMode = '';
                        if (data[i].paymentMode === 1) {
                            paymentMode = '支付宝';
                        } else if (data[i].paymentMode === 2) {
                            paymentMode = '微信';
                        } else {
                            paymentMode = '银行卡';
                        }
                        //支付类型
                        var paymentType = '';
                        if (data[i].paymentType === 1) {
                            paymentType = '一次付清';
                        } else {
                            paymentType = '分期付款';
                        }
                        //校长审核
                        var paymentPrincipalStatus = '';
                        if (data[i].paymentPrincipalStatus === 1) {
                            paymentPrincipalStatus = '审核';
                        } else {
                            paymentPrincipalStatus = '未审核';
                        }
                        //学生审核
                        var paymentStudentStatus = '';
                        if (data[i].paymentStudentStatus === 1) {
                            paymentStudentStatus = '审核';
                        } else {
                            paymentStudentStatus = '未审核';
                        }
                        tableData.push([isNotNull(data[i].userNumber), isNotNull(realNameUser),
                            isNotNull(paymentMode), isNotNull(data[i].receivablesId), isNotNull(paymentType),
                            isNotNull(schoolTerm), isNotNull(data[i].paymentMoney), isNotNull(data[i].paymentSpareMoney),
                            setTime(data[i].paymentNowTime), setTime(data[i].paymentNextTime), isNotNull(paymentPrincipalStatus),
                            isNotNull(paymentStudentStatus), setTime(data[i].creationDate)]);
                    }
                    // console.log(data);
                    //table.exportFile('test',data,'xls');
                    table.exportFile(['学生学号', '学生姓名',
                            '支付方式', '账户详情',
                            '支付类型', '支付学期', '本次缴费金额',
                            '剩余缴费金额', '支付时间', '下次缴费时间', '校长审核', '学生审核', '创建时间'],
                        tableData
                        , 'csv');
                }
            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
<script>
    /**13位时间戳转换成 年月日 上午 时间  2018-12-27 15：35 */
    function setTime(v) {
        if (v == null) {
            return null;
        }
        return new Date(parseInt(v)).toLocaleString()
    }

    Date.prototype.toLocaleString = function () {
        var y = this.getFullYear();
        var m = this.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = this.getDate();
        d = d < 10 ? ("0" + d) : d;
        var h = this.getHours();
        h = h < 10 ? ("0" + h) : h;
        var M = this.getMinutes();
        M = M < 10 ? ("0" + M) : M;
        var S = this.getSeconds();
        S = S < 10 ? ("0" + S) : S;
        return y + "-" + m + "-" + d;
        //返回的时间格式 2018-12-27 15：35 + " " + h + ":" + M + ":" + S
        //自定义时间格式直接更改return格式即可
    };

    function isNotNull(d) {
        if (d == null) {
            return null;
        }
        return d;
    }
</script>
</body>
</html>