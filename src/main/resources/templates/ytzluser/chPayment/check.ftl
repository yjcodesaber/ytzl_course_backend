<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style type="text/css">
        .layui-form-item .layui-inline {
            width: 33.333%;
            float: left;
            margin-right: 0;
        }

        @media (max-width: 1240px) {
            .layui-form-item .layui-inline {
                width: 100%;
                float: none;
            }
        }

        .layui-form-item .role-box {
            position: relative;
        }

        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;margin-top: 10px">
    <#--<label class="layui-form-label">学生编号数组</label>-->
    <input type="hidden" class="layui-inline" name="ids" id="ids" value="">

    <div class="layui-form-item">
        <label class="layui-form-label">学生学号</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input layui-disabled" name="userNumber" id="userNumber">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">学生姓名</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" name="realName" id="realName" value="">
            <span>请将姓名补全</span>
        </div>

    </div>

    <div class="layui-form-item">
        <div class="layui-input-block" style="text-align:center">
            <button class="layui-btn" lay-submit="" lay-filter="checkUser">确定</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer', 'laydate'], function () {
        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer,
            laydate = layui.laydate;
        var userNumber = top.userNumber;
        form.val('form',{
            "userNumber":userNumber
        })
        var loadIndex = layer.load(2, {
            shade: [0.3, '#333']
        });
        $.post("${base}/chUser/getUserInfo",{"userNumber":userNumber+""},function (res) {
            layer.close(loadIndex);
           var userName = res.realName;
            // var checkIndex = parent.layer.getFrameIndex(window.name);
           if (userName!=null){
               var name = userName.substring(0,userName.length-1);
               $("#realName").val(name);
           }else{
               $("#realName").val("未找到该学生")
               parent.layer.msg("未找到该学生...", {time: 1000}, function () {
                   // parent.layer.close(checkIndex);
               });
           }

        })



        form.on("submit(checkUser)", function (data) {
            top.checkok=false;
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var checkIndex = parent.layer.getFrameIndex(window.name);
            $.post("${base}/chUser/check",{"userNumber":$("#userNumber").val(),"realName":$("#realName").val()}, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    top.checkok=true;
                    parent.layer.msg("验证成功！", {time: 1000}, function () {
                        parent.layer.close(checkIndex);
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>