<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style type="text/css">
        .layui-form-item .layui-inline {
            width: 33.333%;
            float: left;
            margin-right: 0;
        }

        @media (max-width: 1240px) {
            .layui-form-item .layui-inline {
                width: 100%;
                float: none;
            }
        }

        .layui-form-item .role-box {
            position: relative;
        }

        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <div class="layui-form-item">
        <label class="layui-form-label">收款人姓名</label>
        <div class="layui-input-block">
            <input type="text" name="receivablesName" autocomplete="off" id="receivablesName" autocomplete="off"
                   placeholder="请输入收款人姓名"
                   class="layui-input" lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">收款类型</label>
        <div class="layui-input-block">
            <select id="receivablesType" lay-filter="" lay-verify="required">
                <option value="">选择收款类型</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">收款人账户</label>
        <div class="layui-input-block">
            <input type="text" name="receivablesAccount" id="receivablesAccount" autocomplete="off"
                   placeholder="请输入收款人账户"
                   class="layui-input" lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChReceivables">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer'], function () {
        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer;


        /**
         * 动态获取收款类型
         */
        $.get("/admin/system/dict/getDictByType/receivablesType", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择收款类型</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#receivablesType").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");

        var data = top.receivablesData;
        form.val('form', {
            'receivablesName': data.receivablesName,
            'receivablesAccount': data.receivablesAccount
        });
        form.on("submit(addChReceivables)", function (d) {
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var editIndex = parent.layer.getFrameIndex(window.name);
            //获取表单中的值
            var receivablesName = $("#receivablesName").val();
            var receivablesType = $("#receivablesType").val();
            var receivablesAccount = $("#receivablesAccount").val();
            //给角色赋值
            $.post("${base}/chReceivables/edit", {
                receivablesName: receivablesName,
                receivablesType: receivablesType,
                receivablesAccount:receivablesAccount,
                id:data.id
            }, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg("编辑成功！", {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });
    });
</script>
</body>
</html>