<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-input-inline">
                <input type="tel" id="userName" name="userName" autocomplete="off" placeholder="请输入收款人姓名"
                       class="layui-input">
            </div>
            <div class="layui-inline">
                <button class="layui-btn" id="search" data-type="getInfo" lay-submit="" lay-filter="searchForm">查询
                </button>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="addChReceivables">添加</a>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
    <script type="text/html" id="receivablesType">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.receivablesType == 1){ }}
        <span class="layui-badge layui-bg-green">支付宝</span>
        {{#  } else if(d.receivablesType ==2){ }}
        <span class="layui-badge layui-bg-blue">微信</span>
        {{#  } else if(d.receivablesType ==3){ }}
        <span class="layui-badge layui-bg-gray">银行卡</span>
        {{#  } }}
    </script>

    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <button class="layui-btn layui-btn-danger layui-btn-xs" type="button" lay-event="del">禁用</button>
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;


        //监听工具条
        table.on('tool(demo)', function (obj) {
            top.receivablesData = obj.data;

            var editIndex;
            if (obj.event === 'edit') {
                editIndex = layer.open({
                    title: "编辑",
                    type: 2,
                    content: "${base}/chReceivables/edit",
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(editIndex);
                });
                layer.full(editIndex);
            }
            if (obj.event === "del") {
                layer.confirm("你确定要禁用吗？", {btn: ['是的,我确定', '取消']},
                    function () {
                        $.post("${base}/chReceivables/delete", {"id": top.receivablesData.id}, function (res) {
                            if (res.success) {
                                layer.msg("禁用成功成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }
                        });
                    }
                )
            }
        });

        var t = {
            elem: '#test',
            url: '${base}/chReceivables/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {field: 'id', title: '主键id'},
                {field: 'receivablesName', title: '收款人姓名', width: '15%'},
                {field: 'receivablesAccount', title: '收款账号', width: '15%'},
                {field: 'bankName', title: '银行', width: '15%'},
                {field: 'receivablesType', title: '收款类型', templet: '#receivablesType', width: '10%'},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {
                    field: 'modifyedDate',
                    title: '修改时间',
                    width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.modifyedDate) }}</div>',
                    unresize: true
                },
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'},
            ]], done: function () {
                $("[data-field='id']").css('display', 'none');
            }
        };
        table.render(t);
        var addIndex;
        var active = {
            addChReceivables: function () {
                addIndex = layer.open({
                    title: "添加",
                    type: 2,
                    content: "${base}/chReceivables/add",

                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(addIndex);
                });
                layer.full(addIndex);
            },
            //查询
            getInfo: function () {
                var userName = $("#userName").val();
                var index = layer.msg('查询中，请稍候', {icon: 16, time: false, shade: 0});
                table.reload('test', {
                    where: {
                        userName: userName
                    }
                });
                layer.close(index);
            }
        };

        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>