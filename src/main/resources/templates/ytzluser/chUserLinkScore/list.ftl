<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" lay-filter="form" id="searchForm">
            <div class="layui-inline" style="margin-left: 15px">
                <label>查询条件:</label>
                <div class="layui-input-inline">
                    <input type="text" value="" autocomplete="off" name="selectInfo" placeholder="学生学号-or-姓名"
                           class="layui-input search_input">
                </div>
            </div>
            <#--根据学期查询-->
                <div class="layui-inline">
                    <select id="schoolTerm" name="schoolTerm">
                        <option value="">请选择学期</option>
                    </select>
                </div>
            <#--根据院系查询-->
                <div class="layui-inline">
                    <select id="departmentId" name="departmentId" lay-filter="department">
                        <option value="">请选择院系</option>
                    </select>
                </div>
                <span>-</span>
            <#--根据班级查询-->
                <div class="layui-inline">
                    <select id="classId" name="classId">
                        <option value="">请选择班级</option>
                    </select>
                </div>
                <hr class="layui-bg-gray">
            <#--根据专业查询-->
                <div class="layui-inline">
                    <select id="majorId" name="majorId" lay-filter="major">
                        <option value="">请选择专业</option>
                    </select>
                </div>
                <span>-</span>
            <#--根据学科查询-->
            <div class="layui-inline">
                <select id="subjectId" name="subjectId">
                    <option value="">请选择学科</option>
                </select>
            </div>
            <div class="layui-inline">
                <a class="layui-btn" lay-submit="" lay-filter="searchForm">查询</a>
            </div>
            <div class="layui-inline">
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="addChUserLinkScore">添加</a>
            </div>
            <div class="layui-inline">
                <a class="layui-btn layui-btn-normal" data-type="outExcel">导出Excel</a>
            </div>
        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
    <script type="text/html" id="term">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.schoolTerm == 1){ }}
        <span class="layui-badge layui-bg-green">大一上学期</span>
        {{#  }else if(d.schoolTerm == 2){ }}
        <span class="layui-badge layui-bg-green">大一下学期</span>
        {{#  }else if(d.schoolTerm == 3){ }}
        <span class="layui-badge layui-bg-orange">大二上学期</span>
        {{#  }else if(d.schoolTerm == 4){ }}
        <span class="layui-badge layui-bg-orange">大二下学期</span>
        {{#  }else if(d.schoolTerm == 5){ }}
        <span class="layui-badge layui-bg-blue">大三上学期</span>
        {{#  }else if(d.schoolTerm == 6){ }}
        <span class="layui-badge layui-bg-blue">大三下学期</span>
        {{#  }else if(d.schoolTerm == 7){ }}
        <span class="layui-badge layui-bg-blue">大四上学期</span>
        {{#  }else if(d.schoolTerm == 8){ }}
        <span class="layui-badge layui-bg-blue">大四下学期</span>
        {{#  } else  { }}
        <span class="layui-badge layui-bg-blue">其他</span>
        {{#  } }}
    </script>

    <script type="text/html" id="barDemo">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;


        /**
         * 加载状态
         */
        $.get("${base}/admin/system/dict/getDictByType/school_term", {}, function (data) {
            var html = "<option value=\"\">请选择学期</option>";
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#schoolTerm").html(html);
                //表单重新渲染
                form.render();

            }
        }, "json");

        /**
         * 加载院系
         */
        $.post("${base}/department/getDepartmentName", {}, function (data) {
            var html = "<option value=\"\">请选择院系</option>";
            if (data != null) {
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].departmentName + '</option>';
                }
                $("#departmentId").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");
        /**
         * 加载专业
         */
        $.get("${base}/chMajor/getMajorList", {}, function (data) {
            var html = "<option value=\"\">请选择专业</option>";
            if (data != "") {
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].majorName + '</option>';
                }
                $("#majorId").html(html);
                //表单重新渲染
                form.render();

            }
        }, "json");


        //选择院系的监听事件
        form.on("select(department)", function () {

            var departmentId = $("#departmentId").val();
            if (departmentId == "") {
                var html = '<option value="">请选择班级名称</option>';
                $("#classId").html(html);
                form.render();
            }
            $.post("${base}/class/getClassName", {departmentId: departmentId}, function (data) {
                var html = '<option value="">请选择班级名称</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].className + '</option>';
                }
                $("#classId").html(html);
                form.render();

            }, "json");
        });
        //选择专业的监听事件
        form.on("select(major)", function () {
            var majorId = $("#majorId").val();
            if (majorId == "") {
                var html = '<option value="">请选择学科</option>';
                $("#subjectId").html(html);
                form.render();
            }
            $.post("${base}/chSubject/findSubjectName", {majorId: majorId}, function (data) {
                var html = '<option value="">请选择学科</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].subjectName + '</option>';
                }
                $("#subjectId").html(html);
                form.render();
            }, "json");
        });


        //监听工具条
        table.on('tool(demo)', function (obj) {
            var data = obj.data;
            var editorIndex;
            if (obj.event === 'edit') {
                top.scoreData = data;
                editorIndex = layer.open({
                    title: "编辑",
                    type: 2,
                    content: "${base}/chUserLinkScore/edit",
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(editorIndex);
                });
                layer.full(editorIndex);
            }
            if (obj.event === "del") {
                layer.confirm("你确定要删除该么？", {btn: ['是的,我确定', '我再想想']},
                    function () {
                        $.post("${base}/chUserLinkScore/delete", {"id": data.id}, function (res) {
                            if (res.success) {
                                layer.msg("删除成功", {time: 1000}, function () {
                                    table.render(t);
                                });
                            } else {
                                layer.msg(res.message);
                            }

                        });
                    }
                )
            }
        });
        var exportData = [];
        var t = {
            elem: '#test',
            url: '${base}/chUserLinkScore/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type: 'checkbox'},
                {field: 'userNumber', title: '学生学号', width: '12%'},
                {field: 'realName', title: '姓名', width: '12%'},
                {field: 'majorName', title: '专业名称', width: '12%'},
                {field: 'subjectName', title: '课程名称', width: '12%'},
                {field: 'schoolTerm', title: '学期', width: '12%', templet: '#term'},
                {field: 'machineTestResult', title: '上机成绩', width: '12%'},
                {field: 'writtenTestResult', title: '笔试成绩', width: '12%'},
                {
                    field: 'creationDate',
                    title: '创建时间',
                    width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.creationDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {
                    field: 'modifyedDate',
                    title: '修改时间',
                    width: '15%',
                    templet: '<div>{{ layui.laytpl.toDateString(d.modifyedDate) }}</div>',
                    unresize: true
                }, //单元格内容水平居中
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'}
            ]]
        };
        table.render(t);
        var data = [];
        var addIndex;
        var active = {
            addChUserLinkScore: function () {
                addIndex = layer.open({
                    title: "添加",
                    type: 2,
                    content: "${base}/chUserLinkScore/add"
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(addIndex);
                });
                layer.full(addIndex);
            },
            outExcel: function () {
                exportData = table.checkStatus('test'),
                    data = exportData.data;
                var tableData = [];
                if (data.length <= 0) {
                    return layer.msg("请选择需要导出的数据！");
                } else {
                    for (var i = 0; i < data.length; i++) {
                        var schoolTerm = '';
                        if (data[i].schoolTerm === 1) {
                            schoolTerm = '大一上学期';
                        } else if (data[i].schoolTerm === 2) {
                            schoolTerm = '大一下学期';
                        } else if (data[i].schoolTerm === 3) {
                            schoolTerm = '大二上学期';
                        } else if (data[i].schoolTerm === 4) {
                            schoolTerm = '大二下学期';
                        } else if (data[i].schoolTerm === 5) {
                            schoolTerm = '大三上学期';
                        } else if (data[i].schoolTerm === 6) {
                            schoolTerm = '大三下学期';
                        } else if (data[i].schoolTerm === 7) {
                            schoolTerm = '大四上学期';
                        } else if (data[i].schoolTerm === 8) {
                            schoolTerm = '大四下学期';
                        } else {
                            schoolTerm = '其他';
                        }
                        tableData.push([isNotNull(data[i].userNumber), isNotNull(data[i].realName),
                            isNotNull(data[i].subjectName), isNotNull(schoolTerm), isNotNull(data[i].machineTestResult),
                            isNotNull(data[i].writtenTestResult), setTime(data[i].creationDate), isNotNull(data[i].creationBy), setTime(data[i].modifyedDate), isNotNull(data[i].modifyedBy)]);
                    }
                    table.exportFile(['学生学号', '姓名',
                            '课程名称', '学期',
                            '上机成绩', '笔试成绩', '创建时间',
                            '创建人', '修改时间', '修改人'],
                        tableData
                        , 'csv');
                }

            }
        }
        $('.layui-inline .layui-btn-normal').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        form.on("submit(searchForm)", function (data) {
            if ($("#departmentId").val() != "" && $("#classId").val() == "") {
                return layer.msg("请选择相关班级进行查找！")
            }
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
<script>
    /**13位时间戳转换成 年月日 上午 时间  2018-12-27 15：35 */
    function setTime(v) {
        if (v == null) {
            return null;
        }
        return new Date(parseInt(v)).toLocaleString()
    }

    Date.prototype.toLocaleString = function () {
        var y = this.getFullYear();
        var m = this.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = this.getDate();
        d = d < 10 ? ("0" + d) : d;
        var h = this.getHours();
        h = h < 10 ? ("0" + h) : h;
        var M = this.getMinutes();
        M = M < 10 ? ("0" + M) : M;
        var S = this.getSeconds();
        S = S < 10 ? ("0" + S) : S;
        return y + "-" + m + "-" + d;
        //返回的时间格式 2018-12-27 15：35 + " " + h + ":" + M + ":" + S
        //自定义时间格式直接更改return格式即可
    };

    function isNotNull(d) {
        if (d == null) {
            return null;
        }
        return d;
    }
</script>
</body>
</html>