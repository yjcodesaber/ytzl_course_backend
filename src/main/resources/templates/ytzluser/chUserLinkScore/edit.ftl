<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>编辑--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <style type="text/css">

        @media (max-width: 1240px) {
            .layui-form-item .layui-inline {
                width: 100%;
                float: none;
            }
        }

        .layui-form-item .role-box {
            position: relative;
        }

        .layui-form-item .role-box .jq-role-inline {
            height: 100%;
            overflow: auto;
        }

    </style>
</head>
<body class="childrenBody">
<form class="layui-form" lay-filter="form" style="width:80%;">
    <input name="id" type="hidden">
    <div class="layui-form-item">
        <label class="layui-form-label">学生学号</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="userNumber" id="userNumber">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">专业名称</label>
        <div class="layui-input-block">
            <select id="majorId" name="majorId" lay-filter="major">
                <option value="">请选择专业</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">学科名称</label>
        <div class="layui-input-block">
            <select id="subjectId" name="subjectId">
                <option value="">请选择学科</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">学期</label>
        <div class="layui-input-block">
            <select id="schoolTerm" name="schoolTerm">
                <option value="">请选择学期</option>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">上机成绩</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="machineTestResult" id="machineTestResult">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">笔试成绩</label>
        <div class="layui-input-block">
            <input type="text" class="layui-input" autocomplete="off" name="writtenTestResult" id="writtenTestResult">
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="addChUserLinkScore">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script>
    layui.use(['form', 'jquery', 'layer'], function () {
        var form = layui.form,
            $ = layui.jquery,
            layer = layui.layer;
        var scoreData = top.scoreData;



        $.get("${base}/admin/system/dict/getDictByType/school_term", {}, function (data) {
            var html = '<option value="">请选择学期</option>';
            if (data.success === true) {
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#schoolTerm").html(html);
                //表单重新渲染
                form.render();
                form.val("form", {
                    "schoolTerm": scoreData.schoolTerm
                })
            }
        }, "json");


        //获取所有专业名称
        $.get("${base}/chMajor/getMajorList", {}, function (data) {
            var html = "<option value=''>请选择专业名称</option>";
            if (data!="") {
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].majorName + '</option>';
                }
                $("#majorId").html(html);
                form.render();
                form.val("form", {
                    "majorId": scoreData.majorId
                })
            }
        });
       //获取所有课程名称
        $.post("${base}/chSubject/findSubjectName", {majorId: scoreData.majorId}, function (data) {
            var html = '<option value="">请选择学科</option>';
            if (data!=null){
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].subjectName + '</option>';
                }
            }
            $("#subjectId").html(html);
            form.render();
            form.val("form", {
                "subjectId": scoreData.subjectId
            })
        }, "json");

        //选择专业的监听事件
        form.on("select(major)", function () {
            var majorId = $("#majorId").val();
            if (majorId == ""){
                var html = '<option value="">请选择学科</option>';
                $("#subjectId").html(html);
                form.render();
            }
            $.post("${base}/chSubject/findSubjectName", {majorId: majorId}, function (data) {
                var html = '<option value="">请选择学科</option>';
                if (data!=null){
                    for (var i = 0; i < data.length; i++) {
                        html += '<option value="' + data[i].id + '">' + data[i].subjectName + '</option>';
                    }
                }
                $("#subjectId").html(html);
                form.render();
            }, "json");
        });

        form.val('form', {
            'id': scoreData.id,
            'userNumber': scoreData.userNumber,
            'machineTestResult': scoreData.machineTestResult,
            'writtenTestResult': scoreData.writtenTestResult
        });
        form.on("submit(addChUserLinkScore)", function (data) {
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            //给角色赋值
            var editIndex=parent.layer.getFrameIndex(window.name);
            $.post("${base}/chUserLinkScore/edit", data.field, function (res) {
                layer.close(loadIndex);
                if (res.success) {
                    parent.layer.msg("编辑成功！", {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    layer.msg(res.message);
                }
            });
            return false;
        });
    });
</script>
</body>
</html>