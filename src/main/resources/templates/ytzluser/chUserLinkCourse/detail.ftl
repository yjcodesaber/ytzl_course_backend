<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>
</div>

<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            table = layui.table;
        var userId = top.userId;


        var t = {
            elem: '#test',
            url: '${base}/userLinkCourse/getList?userId=' + userId,
            method: 'get',
            cellMinWidth: 100, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {
                    field: 'courseName',
                    title: '课程名称'
                },
                {
                    field: 'expDate',
                    title: '到期时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.expDate) }}</div>',
                    unresize: true
                }
            ]]
        };
        table.render(t);
    });
</script>
</body>
</html>