<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<fieldset class="layui-elem-field">
    <legend>任务表检索</legend>
    <div class="layui-field-box">
        <form class="layui-form" id="searchForm">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="tel" id="userName" name="userName" autocomplete="off" placeholder="请输入用户名"
                           class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <select name="taskType" id="taskType">
                        <option value="0">请选择任务类型</option>

                    </select>
                </div>
                <div class="layui-input-inline">
                    <select name="taskStatus" id="taskStatus">
                        <option value="0">请选择状态</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <select name="className" id="className">
                        <option value="0">请选择班级</option>
                    </select>
                </div>
                <div class="layui-inline">
                    <button class="layui-btn" id="search" data-type="getInfo" lay-submit="" lay-filter="searchForm">查询
                    </button>
                </div>
                <div class="layui-inline">
                    <a class="layui-btn layui-btn-warm" id="addChTask" data-type="addChTask">分配任务</a>
                </div>
            </div>

        </form>
    </div>
</fieldset>
<div class="layui-form users_list">
    <table class="layui-table" id="test" lay-filter="demo"></table>

    <script type="text/html" id="taskType1">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.taskType == 1){ }}
        <span class="layui-badge layui-bg-green">视频学习</span>
        {{#  } else if(d.taskType ==2){ }}
        <span class="layui-badge layui-bg-blue">题库测验</span>
        {{#  } else if(d.taskType ==3){ }}
        <span class="layui-badge layui-bg-gray">其他</span>
        {{#  } }}

    </script>

    <script type="text/html" id="taskStatuss">
        <!-- 这里的 checked 的状态只是演示 -->
        {{#  if(d.taskStatus == 1){ }}
        <span class="layui-badge layui-bg-green">已创建</span>
        {{#  } else if(d.taskStatus ==2){ }}
        <span class="layui-badge layui-bg-blue">待审核</span>
        {{#  } else if(d.taskStatus ==3){ }}
        <span class="layui-badge layui-bg-cyan">已点评</span>
        {{#  } else if(d.taskStatus ==4){ }}
        <span class="layui-badge layui-bg-red">任务结束</span>
        {{#  } }}
    </script>

    <script type="text/html" id="barDemo">
        {{#  if(d.taskStatus==1){ }}
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="audit">审核</a>
        {{#  } else if(d.taskStatus ==2){ }}
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="audit">审核</a>
        {{#  } }}
    </script>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;


        //监听工具条
        table.on('tool(demo)', function (obj) {
            top.taskData = obj.data;
            var auditIndex;
            if (obj.event === "audit") {
                auditIndex = layer.open({
                    title: "审核",
                    type: 2,
                    content: "${base}/chTask/audit?taskType=" + top.taskData.taskType,
                    success: function (layero, index) {
                        table.render();
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(auditIndex);
                });
                layer.full(auditIndex);
            }
        });

        var t = {
            elem: '#test',
            url: '${base}/chTask/list',
            method: 'post',
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'], //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                groups: 2, //只显示 1 个连续页码
                first: "首页", //显示首页
                last: "尾页", //显示尾页
                limits: [3, 10, 20, 30, 50, 80]
            },
            cellMinWidth: 80, //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            cols: [[
                {type: 'checkbox'},
                {field: 'id', title: '任务主键id', width: '12%'},
                {field: 'userId', title: '学生id', width: '12%'},
                {field: 'userLinkTaskId', title: '用户关联任务表id', width: '12%'},
                {field: 'taskName', title: '任务名称', width: '12%'},
                {field: 'taskDescribe', title: '任务描述', width: '12%'},
                {field: 'taskType', title: '任务类型', templet: '#taskType1', width: '12%'},
                {field: 'realName', title: '用户姓名', width: '12%'},
                {field: 'taskStatus', title: '任务状态', templet: '#taskStatuss', width: '12%'},
                {
                    field: 'taskStartTime',
                    title: '任务开始时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.taskStartTime) }}</div>',
                    unresize: true,
                    width: '12%'
                },
                {
                    field: 'taskEndTime',
                    title: '任务结束时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.taskEndTime) }}</div>',
                    unresize: true,
                    width: '12%'
                },
                {
                    field: 'completeTime',
                    title: '完成时间',
                    templet: '<div>{{ layui.laytpl.toDateString(d.completeTime) }}</div>',
                    unresize: true,
                    width: '12%'
                },
                {field: 'taskComments', title: '老师评语', width: '12%'},
                {field: 'taskNote', title: '备注', width: '12%'},
                //单元格内容水平居中
                {fixed: 'right', title: '操作', width: '15%', align: 'center', toolbar: '#barDemo'}
            ]], done: function () {
                $("[data-field='id']").css('display', 'none');
                $("[data-field='userId']").css('display', 'none');
                $("[data-field='userLinkTaskId']").css('display', 'none');
            }
        };
        table.render(t);
        var addTaskIndex;
        var actives = {
            addChTask: function () {
                addTaskIndex = layer.open({
                    title: "分配任务",
                    type: 2,
                    content: "${base}/chTask/assiGnedTasks",
                    success: function (layero, addTask) {
                        table.render();
                    }
                });
                //改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
                $(window).resize(function () {
                    layer.full(addTaskIndex);
                });
                layer.full(addTaskIndex);
            }
        };

        $('#addChTask').on('click', function () {
            var type = $(this).data('type');
            actives[type] ? actives[type].call(this) : '';
        });


        //动态加载当前用户所拥有的班级
        $.get("/chTask/selectClass", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择班级</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].id + '">' + data.data[i].className + '</option>';
                }
                $("#className").html(html);
                form.render();
            }
        }, "json");


        /**
         * 加载任务类型
         */
        $.get("/admin/system/dict/getDictByType/taskType", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择任务类型</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#taskType").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");
        /**
         * 加载状态
         */
        $.get("/admin/system/dict/getDictByType/taskStatus", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择状态</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#taskStatus").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");
        //查询
        var active = {
            getInfo: function () {
                var condition = $("#userName").val().trim();
                var taskType = $("#taskType").val();
                var taskStatus = $("#taskStatus").val();
                var classId = $("#className").val();
                var index = layer.msg('查询中，请稍候', {icon: 16, time: false, shade: 0});
                table.reload('test', {
                    where: {
                        condition: condition,
                        taskType: taskType,
                        taskStatus: taskStatus,
                        classId: classId
                    }
                });
                layer.close(index);
            }
        };

        //根据任务类型查询
        $("#search").on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

        //监听输入框回车事件
        $("#select").bind('keyup', function (e) {
            if (e.keyCode == 13) {
                var type = "getInfo";
                active[type] ? active[type].call(this) : '';
            }
        });
        form.on("submit(searchForm)", function (data) {
            t.where = data.field;
            table.reload('test', t);
            return false;
        });

    });
</script>
</body>
</html>