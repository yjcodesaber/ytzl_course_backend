<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<body class="childrenBody">
<div class="layui-form users_list">
    <div class="layui-form-item">
        <label class="layui-form-label">班级</label>
        <div class="layui-input-block">
            <select id="className" lay-filter="className">
                <option value="">请选择班级</option>
            </select>
        </div>
    </div>


    <div class="layui-container" style="margin-top: 15px;">

        <div id="root"></div>
        <div id="root1"></div>
    </div>
    <table class="layui-table" id="test" lay-filter="demo">

    </table>

    <form class="layui-form layui-form-pane" lay-filter="form">
        <div class="layui-form-item">
            <label class="layui-form-label">任务名称</label>
            <div class="layui-input-block">
                <input type="text" name="taskName" autocomplete="off" id="taskName" autocomplete="off"
                       placeholder="请输入任务名称"
                       class="layui-input" lay-verify="required" maxlength="30">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">任务类型</label>
            <div class="layui-input-block">
                <select id="taskType" lay-filter="aihao" lay-verify="required">
                    <option value="">选择任务类型</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">开始时间</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" readonly autocomplete="off" id="startTime" lay-verify="required"
                           placeholder="请选择任务开始时间">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">结束时间</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" readonly autocomplete="off" id="endTime" lay-verify="required"
                           placeholder="请选择任务结束时间">
                </div>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">任务描述</label>
            <div class="layui-input-block">
                <textarea id="text" lay-verify="required" autocomplete="off" placeholder="请输入内容"
                          class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block" style="text-align: center">
                <div class="layui-btn-group">
                    <button class="layui-btn right" type="button" lay-submit="" lay-filter="submit">立即分配</button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="page"></div>
<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<#--<script type="text/javascript" src="${base}/static/layui_exts/transfer/transfer.js"></script>-->
<script>
    layui.config({
        base: '${base}/static/layui_exts/transfer/'
    }).use(['form', 'transfer', 'layer', 'laydate', 'jquery'], function () {
        var transfer = layui.transfer,
            layer = layui.layer,
            laydate = layui.laydate,
            form = layui.form, $ = layui.jquery;
        //表格列
        var cols = [{type: 'checkbox', fixed: 'left'},
            {field: 'userNumber', title: '学号'},
            {field: 'realName', title: '学生姓名'}];
        //表格配置文件
        var tabConfig = {'page': true, 'limit': 10, 'height': 400};

        var str1 = [];
        var str2 = [];
        var tb1 = transfer.render({
            elem: "#root", //指定元素
            cols: cols, //表格列  支持layui数据表格所有配置
            data: [str1, str2], //[左表数据,右表数据[非必填]]
            tabConfig: tabConfig //表格配置项 支持layui数据表格所有配置
        })
        //动态班级信息
        $.get("/chTask/selectClassName", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">请选择班级</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].id + '">' + data.data[i].className + '</option>';
                }
                $("#className").html(html);
                form.render('select')
            }
        }, "json");
        /**
         * 加载任务类型
         */
        $.get("/admin/system/dict/getDictByType/taskType", {}, function (data) {
            var html = "";
            if (data.success === true) {
                html += '<option value="">选择任务类型</option>';
                for (var i = 0; i < data.data.length; i++) {
                    html += '<option value="' + data.data[i].value + '">' + data.data[i].label + '</option>';
                }
                $("#taskType").html(html);
                //表单重新渲染
                form.render();
            }
        }, "json");

        //监听下拉框选中
        form.on('select(className)', function (data) {

            $.post("/chTask/selectStudent", {classId: data.value}, function (dataAjax) {
                //获取右边数据
                var dataRight = transfer.get(tb1, 'r');
                //
                var data = getSubtract(dataAjax, dataRight);
                tb1 = transfer.render({
                    elem: "#root", //指定元素
                    cols: cols, //表格列  支持layui数据表格所有配置
                    data: [data, dataRight], //[左表数据,右表数据[非必填]]
                    tabConfig: tabConfig //表格配置项 支持layui数据表格所有配置
                })
            })
        });

        //数组求差
        function getSubtract(unionArr, subsetArr) {
            var new_tmp = new Array();
            for (var i = 0; i < unionArr.length; i++) {
                var flag = true;
                for (var j = 0; j < subsetArr.length; j++) {
                    if (unionArr[i].id == subsetArr[j].id) {
                        flag = false;
                    }
                }
                if (flag) {
                    new_tmp.push(unionArr[i]);
                }
            }
            return new_tmp;
        }


        // 时间选择器初始化 开始时间
        laydate.render({
            elem: '#startTime',
            type: 'datetime',
            trigger: 'click',
            // format: 'yyyy-MM-dd HH:mm:ss',
            done: function (value, date, endDate) {
                var startDate = new Date(value).getTime();
                var endTime = new Date($('#endTime').val()).getTime();
                if (endTime < startDate) {
                    layer.msg('结束时间不能小于开始时间');
                    $('#startTime').val($('#endTime').val());
                }
            }
        });
        laydate.render({ //结束时间
            elem: '#endTime',
            type: 'datetime',
            trigger: 'click',
            // format: 'yyyy-MM-dd HH:mm:ss',
            done: function (value, date, endDate) {
                var startDate = new Date($('#startTime').val()).getTime();
                var endTime = new Date(value).getTime();
                if (endTime < startDate) {
                    layer.msg('结束时间不能小于开始时间');
                    $('#endTime').val($('#startTime').val());
                }
            }
        });


        //transfer.get(参数1:初始化返回值,参数2:获取数据[all,left,right,l,r],参数:指定数据字段)
        //获取数据
        form.on("submit(submit)", function (d) {
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var data = transfer.get(tb1, 'r');
            // layer.msg(JSON.stringify(data));
            var id = "";
            for (var i = 0; i < data.length; i++) {
                id += data[i].id;
                if (i !== data.length - 1) {
                    id += ",";
                }
            }
            var taskName = $("#taskName").val();
            var taskType = $("#taskType").val();
            var startTime = $("#startTime").val();
            var endTime = $("#endTime").val();
            var taskDe = $("#text").val();
            var editIndex=parent.layer.getFrameIndex(window.name);
            $.post("/chTask/submit",
                {
                    id: id,
                    taskName: taskName,
                    taskType: taskType,
                    taskStartTime: startTime,
                    taskEndTime: endTime,
                    taskDescribe: taskDe
                }, function (data) {
                    if (data.success) {
                        parent.layer.msg("分配成功！", {time: 1000}, function () {
                            parent.layer.close(editIndex);
                            //刷新父页面
                            parent.layui.table.reload('test');
                        });
                    }else {
                        layer.msg(data.message);
                    }
                })
        });
        return false;
    });
    // $('.right').on('click',);

</script>


<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;
    });
</script>
</body>
</html>