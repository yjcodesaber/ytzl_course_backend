<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>--${site.name}</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="${site.description}"/>
    <meta name="keywords" content="${site.keywords}"/>
    <meta name="author" content="${site.author}"/>
    <link rel="icon" href="${site.logo}">
    <link rel="stylesheet" href="${base}/static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all"/>
    <link rel="stylesheet" href="${base}/static/css/user.css" media="all"/>
</head>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/echarts.min.js"></script>
<script type="text/javascript" src="/static/js/customed.js"></script>
<body class="childrenBody">
<div class="layui-form users_list">
    <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
    <div id="main" style="width: 800px;height:400px;"></div>
    <form class="layui-form layui-form-pane" action="">
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">老师评语</label>
            <div class="layui-input-block">
                <textarea id="taskComments" lay-verify="required" autocomplete="off" placeholder="请输入内容"
                          class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <div class="layui-btn-group">
                    <button class="layui-btn right" id="approved" type="button" lay-submit="" lay-filter="approved">
                        审核通过
                    </button>
                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
            </div>
        </div>
    </form>
</div>
</body>


<script type="text/javascript" src="${base}/static/layui/layui.js"></script>
<script type="text/javascript" src="${base}/static/js/tools.js"></script>
<script>
    layui.use(['layer', 'form', 'table', 'laydate'], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            laydate = layui.laydate,
            table = layui.table;

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'), 'roma');

        var x = [];
        var y = [];
        $.post('/chTask/chart', {
            userId: top.taskData.userId,
            taskStartTime: top.taskData.taskStartTime,
            taskEndTime: top.taskData.taskEndTime
        }, function (data) {
            if (data.success) {
                if (data.data.length === 0) {
                    layer.msg(data.message,{time:4000});
                }
                for (var i = 0; i < data.data.length; i++) {
                    x.push(setTime(data.data[i].creationDate));
                    y.push(minutes(data.data[i].studyTimeLong));
                }
                myChart.hideLoading();
                myChart.setOption({        //加载数据图表
                    title: {
                        text: '学习时长统计'
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                            type: 'cross'
                        }
                    },
                    legend: {
                        data: ['分钟'],
                        type: 'scroll'
                    },
                    xAxis: {
                        name: '日期',
                        type: 'category',
                        data: x
                    },
                    yAxis: {
                        name: '分钟',
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}min'
                        }
                    },
                    series: [{
                        // 根据名字对应到相应的系列
                        name: '分钟',
                        type: 'bar',
                        data: y
                    }]
                });
            } else {
                layer.msg(data.message);
            }
        });
        //审核按钮
        form.on("submit(approved)", function (d) {
            var loadIndex = layer.load(2, {
                shade: [0.3, '#333']
            });
            var taskComments = $("#taskComments").val();

            var editIndex = parent.layer.getFrameIndex(window.name);
            $.post("/chTask/approved", {
                userLinkTaskId: top.taskData.userLinkTaskId,
                taskComments: taskComments
            }, function (data) {
                if (data.success) {
                    parent.layer.msg("审核成功", {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                } else {
                    parent.layer.msg(data.message, {time: 1000}, function () {
                        parent.layer.close(editIndex);
                        //刷新父页面
                        parent.layui.table.reload('test');
                    });
                }
            });
            return false;
        });
    });

</script>

<script>

    /**13位时间戳转换成 年月日 上午 时间  2018-12-27 15：35 */
    function setTime(v) {
        return new Date(parseInt(v)).toLocaleString()
    }

    Date.prototype.toLocaleString = function () {
        var y = this.getFullYear();
        var m = this.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = this.getDate();
        d = d < 10 ? ("0" + d) : d;
        var h = this.getHours();
        h = h < 10 ? ("0" + h) : h;
        return y + "-" + m + "-" + d;
        //返回的时间格式 2018-12-27 15：35
        //自定义时间格式直接更改return格式即可
    };

    /**
     * 将秒转换为 分:秒
     * s int 秒数
     */
    function minutes(s) {
        //计算分钟
        //算法：将秒数除以60，然后下舍入，既得到分钟数
        var h;
        h = Math.floor(s / 60);
        //计算秒
        //将变量转换为字符串
        h += '';
        //如果只有一位数，前面增加一个0
        h = (h.length == 1) ? '0' + h : h;
        return h;
    }

    function getVideoTime(a) {
        var hh = parseInt(a / 3600);
        if (hh < 10) hh = "0" + hh;
        var mm = parseInt((a - hh * 3600) / 60);
        if (mm < 10) mm = "0" + mm;
        var ss = parseInt((a - hh * 3600) % 60);
        if (ss < 10) ss = "0" + ss;
        var length = "";
        if (parseInt(hh) !== 0) {
            length = hh + ":" + mm + ":" + ss;
        } else {
            length = mm + ":" + ss;
        }
        if (a > 0) {
            return hh;
        } else {
            return "NaN";
        }
    }
</script>


