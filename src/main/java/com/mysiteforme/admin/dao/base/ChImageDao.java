package com.mysiteforme.admin.dao.base;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.base.ChImage;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.INTERNAL;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-05-10
 */
public interface ChImageDao extends BaseMapper<ChImage> {

    String getImageUrl(String paymentId);

    Integer addBillImage(ChImage chImage);

    Integer updateBillImage(ChImage chImage);

    Integer deleteBillImage(String key);
}
