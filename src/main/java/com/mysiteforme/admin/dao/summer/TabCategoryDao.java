package com.mysiteforme.admin.dao.summer;

import com.mysiteforme.admin.entity.TabCategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品分类表 Mapper 接口
 * </p>
 *
 * @author wangl
 * @since 2019-04-11
 */
public interface TabCategoryDao extends BaseMapper<TabCategory> {

}
