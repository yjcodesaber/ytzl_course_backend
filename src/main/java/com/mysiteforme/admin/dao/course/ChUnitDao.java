package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.course.ChUnit;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChUnitDao extends BaseMapper<ChUnit> {

    Integer insertUnit(ChUnit chUnit);

    Integer updateUnitById(ChUnit chUnit);

    Integer deleteUnitById(String id);
}
