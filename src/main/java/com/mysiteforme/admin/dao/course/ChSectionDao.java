package com.mysiteforme.admin.dao.course;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.course.ChSection;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChSectionDao extends BaseMapper<ChSection> {

    Integer insertSection(ChSection chSection);

    Integer updateSectionById(ChSection chSection);

    Integer deleteSectionById(String id);
}
