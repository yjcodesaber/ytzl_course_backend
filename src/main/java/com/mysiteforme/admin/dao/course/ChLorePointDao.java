package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.course.ChLorePoint;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChLorePointDao extends BaseMapper<ChLorePoint> {

    Integer insertLorePoint(ChLorePoint chLorePoint);

    Integer updateLorePointById(ChLorePoint chLorePoint);

    Integer deleteLorePointById(String id);

    String getVideoUrl(String id);

}
