package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.course.ChSubjectLinkMajor;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChSubjectLinkMajorDao extends BaseMapper<ChSubjectLinkMajor> {

    boolean addList(@Param("infoList") String infoList);


    boolean deleteByMajorId(String majorId);


}
