package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.course.ChMajor;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChMajorDao extends BaseMapper<ChMajor> {

    boolean add(ChMajor chMajor);



    boolean editById(ChMajor chMajor);

    boolean cutById(String id);

    List<ChMajor> getMajorList();
}
