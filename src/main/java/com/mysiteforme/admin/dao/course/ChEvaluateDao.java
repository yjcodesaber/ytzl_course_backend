package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.course.ChEvaluate;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
public interface ChEvaluateDao extends BaseMapper<ChEvaluate> {

    List<ChEvaluate> selectByWrapper(Page<ChEvaluate> objectPage,@Param("ew") EntityWrapper<ChEvaluate> wrapper);

    Integer updateEvaluateById(ChEvaluate chEvaluate);

    Integer updateErrorById(String id);

    Integer deleteEvaluateById(String id);

    Integer selectStartById(String chEvaluateId);


    Integer selectCountEvalueate();

}
