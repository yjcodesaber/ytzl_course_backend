package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.course.ChProblem;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
public interface ChProblemDao extends BaseMapper<ChProblem> {

    List<ChProblem> selectByWrapperToProblem(Page<ChProblem> objectPage,@Param("ew") EntityWrapper<ChProblem> wrapper);

    Integer deleteProblemById(String id);

    Integer updateProblemById(ChProblem chProblem);

    Integer selectCountProblem();
}
