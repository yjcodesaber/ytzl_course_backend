package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.course.ChSubject;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChSubjectDao extends BaseMapper<ChSubject> {


    boolean add(ChSubject chSubject);

    boolean editById(ChSubject chSubject);

    boolean cutById(String id);

    List<ChSubject> findAll();

    List<ChSubject> findByMajorId(String majorId);
}
