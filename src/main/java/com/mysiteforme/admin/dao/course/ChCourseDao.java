package com.mysiteforme.admin.dao.course;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.course.ChCourse;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChCourseDao extends BaseMapper<ChCourse> {

    Integer insertCourse(ChCourse chCourse);

    Integer updateCourseById(ChCourse chCourse);

    Integer deleteCourseById(String id);

    String getVideoUrl(String id);
}
