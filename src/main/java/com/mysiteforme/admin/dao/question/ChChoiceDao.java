package com.mysiteforme.admin.dao.question;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.question.ChChoice;
import org.apache.ibatis.annotations.Param;
import org.omg.CORBA.INTERNAL;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
public interface ChChoiceDao extends BaseMapper<ChChoice> {

    Integer deleteChoiceById(String id);

    Integer correctById(String id);

    Integer wrongById(String id);

    Integer insertChoice(ChChoice chChoice);

    Integer insertFourAnwser(@Param("choice") String forChoice);

    Integer insertChoiceExcel(@Param("info") String info);

    Integer delChoiceByQuestionId(@Param("id") String id);

    Integer delChoiceByQuestionIds(@Param("ids") String ids);
}
