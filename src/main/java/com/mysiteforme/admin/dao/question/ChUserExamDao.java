package com.mysiteforme.admin.dao.question;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.VO.UserExamDto;
import com.mysiteforme.admin.entity.question.ChUserExam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
public interface ChUserExamDao extends BaseMapper<ChUserExam> {

    /**
     * @param param
     * @return
     */
    List<UserExamDto> selectStuExam(Map<String, Object> param);
}
