package com.mysiteforme.admin.dao.question;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.question.ChQuestion;
import com.mysiteforme.admin.entity.question.ChQuestionVO;
import freemarker.template.SimpleDate;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
public interface ChQuestionDao extends BaseMapper<ChQuestion> {

    Integer deleteQuestionById(@Param("id") String id ,@Param("modifyDate") String modifyDate);

    Integer updateQuestoinById(ChQuestion chQuestion);

    Integer insertQuestion(ChQuestion chQuestion);

    Integer deleteQuestionByIds(@Param("ids") String ids,@Param("modifyDate") Date modifyDate);

    String getType(String questionId);

    Integer insertExcel(@Param("info") String info);
}
