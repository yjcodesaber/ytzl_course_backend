package com.mysiteforme.admin.dao.data;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.data.Data;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface StudyDataDao extends BaseMapper<Data> {


    /**
     * @param page
     * @return
     */
    List<Data> selectData(Page<Data> page, Map<String,Object> param);
}
