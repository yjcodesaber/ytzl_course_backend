package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChClass;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChClassDao extends BaseMapper<ChClass> {

    List<ChClass> selectByEntityWrapper(Page<ChClass> objectPage, @Param("ew") EntityWrapper<ChClass> wrapper);

    boolean addInfo(ChClass chClass);

    boolean editById(ChClass chClass);

    boolean cutById(String id);

    ChClass findById(String id);
    /**
     * @param param
     * @return
     */
    List<ChClass> selectClassName(Map<String, Object> param);

    /**
     * 动态获取班级
     */
    List<ChClass> selectClass(@Param("ew") EntityWrapper<ChClass> wrapper);
}
