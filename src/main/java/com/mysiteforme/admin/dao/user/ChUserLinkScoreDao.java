package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChUserLinkScore;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChUserLinkScoreDao extends BaseMapper<ChUserLinkScore> {

    List<ChUserLinkScore> selectByEntityWrapper(Page<ChUserLinkScore> objectPage, @Param("ew") EntityWrapper<ChUserLinkScore> wrapper);

    Integer deleteScoreById(String id);

    Integer insertScore(ChUserLinkScore chUserLinkScore);

    Integer updateScore(ChUserLinkScore chUserLinkScore);


    List<ChUserLinkScore> selectScoreByIds(String ids);
}
