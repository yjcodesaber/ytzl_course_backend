package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChPayment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChPaymentDao extends BaseMapper<ChPayment> {

    Integer insertPayment(ChPayment chPayment);

    Integer updatePaymentById(ChPayment chPayment);

    Integer deletePaymentById(String id);

    Integer examine(String id);

    List<ChPayment> selectByEntityWrapper(Page<ChPayment> objectPage, @Param("ew") EntityWrapper<ChPayment> wrapper);

    Integer selectCountChPayment();
}
