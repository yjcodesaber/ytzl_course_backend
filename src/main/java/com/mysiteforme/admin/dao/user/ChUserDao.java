package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChUserDao extends BaseMapper<ChUser> {

    List<ChUser> selectByEntityWrapper(Page<ChUser> objectPage, @Param("ew") EntityWrapper<ChUser> entityWrapper);

    boolean active(Map<String,Object> map);

    Integer wake(@Param("id") String id, @Param("is_activated") int i);


    /**
     * 查询学生
     *
     * @param classId
     * @return
     */
    List<ChUser> selectStudent(@Param("class_id") String classId);

    List<ChUser> findByLinkId(@Param("ew") EntityWrapper<ChUser> entityWrapper);

    ChUser findByUserNumber(String userNumber);

    Integer editor(Map<String, Object> param);

    Integer distributionClass(Map<String, Object> param);

    ChUser selectByStudentInfo(@Param("id") String id);

    Integer selectUserNumberCount(@Param("userNumber") String userNumber);

    Integer selectUserAccountCount(@Param("userAccount") String userAccount);

    Integer selectIdCardCount(@Param("idCard") String idCard);
}


