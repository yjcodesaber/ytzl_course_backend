package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.UserLinkCourse;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChUserLinkCourseDao extends BaseMapper<UserLinkCourse> {
    boolean insertList(@Param("insertInfo") String insertInfo);

    List<UserLinkCourse> findByUserId(String userId);

    boolean deleteInfo(String linkId);

    List<UserLinkCourse> selectByEntityWrapper(Page<UserLinkCourse> objectPage, @Param("ew") EntityWrapper<UserLinkCourse> wrapper);
}
