package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.user.ChStudyVideoRecord;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface ChStudyVideoRecordDao extends BaseMapper<ChStudyVideoRecord> {


    /**
     * @param param
     * @return
     */
    List<ChStudyVideoRecord> selectStudeyVideo(Map<String,Object> param);
}
