package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChDepartment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
public interface ChDepartmentDao extends BaseMapper<ChDepartment> {

    List<ChDepartment> selectByEntityWrapper(Page<ChDepartment> objectPage, @Param("ew") EntityWrapper<ChDepartment> entityWrapper);


    boolean addInfo(ChDepartment chDepartment);

    ChDepartment findById(String id);

    boolean editById(ChDepartment chDepartment);

    boolean cutById(String id);
}
