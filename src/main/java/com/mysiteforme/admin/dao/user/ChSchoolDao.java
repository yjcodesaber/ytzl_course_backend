package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChSchool;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChSchoolDao extends BaseMapper<ChSchool> {

    List<ChSchool> selectByEntityWrapper(Page<ChSchool> objectPage, @Param("ew") EntityWrapper<ChSchool> entityWrapper);

    boolean addInfo(ChSchool chSchool);

    boolean editById(ChSchool chSchool);

    boolean cutById(String id);
}
