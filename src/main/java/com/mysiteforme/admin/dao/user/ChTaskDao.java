package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChTask;
import com.mysiteforme.admin.entity.user.ChClass;
import com.mysiteforme.admin.entity.user.ChUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 */
public interface ChTaskDao extends BaseMapper<ChTask> {


    /**
     * @param page
     * @param entityWrapper
     * @return
     */
    List<ChTask> selectTask(Page<ChTask> page, @Param("ew") EntityWrapper<ChTask> entityWrapper);

    /**
     * @param teacherId
     * @return
     */
    List<ChClass> selectClassInfo(Page<Object> page, String teacherId);

    /**
     * @param ids
     * @return
     */
    List<ChUser> selectClassUser(@Param(value = "id") String ids);


    /**
     * 添加任务
     * @param chTask
     */
    void addTask(ChTask chTask);



}
