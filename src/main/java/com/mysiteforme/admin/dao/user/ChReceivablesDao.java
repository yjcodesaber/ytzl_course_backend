package com.mysiteforme.admin.dao.user;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChReceivables;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author yj
 * @since 2019-05-07
 */
public interface ChReceivablesDao extends BaseMapper<ChReceivables> {

    List<ChReceivables> selectPageByWrapper(Page<ChReceivables> objectPage, @Param("ew") EntityWrapper<ChReceivables> wrapper);

    Integer updateReceivablesById(Map<String, Object> param);

    Integer insertReceivables(Map<String, Object> param);

    Integer updateDisable(@Param(value = "id") String id);

    List<ChReceivables> getReceivablesInfoById(String payeeId);

    List<ChReceivables> getReceivablesByMode(Integer payMode);
}
