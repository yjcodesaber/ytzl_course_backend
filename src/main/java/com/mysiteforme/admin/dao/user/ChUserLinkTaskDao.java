package com.mysiteforme.admin.dao.user;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mysiteforme.admin.entity.user.UserLinkTask;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 */
public interface ChUserLinkTaskDao extends BaseMapper<UserLinkTask> {


    /**
     * @param id  用户id
     * @param id1  关联表id
     * @param taskId 任务id
     */
    void addUserTask(@Param(value = "user_id") String id,
                     @Param(value = "id") String id1,
                     @Param(value = "task_id") String taskId);

    void addUserTaskList(List<UserLinkTask> userLinkTasks);

    void approved(@Param(value = "id") String userLinkTaskId,
                  @Param(value = "task_comments") String taskComments);

    /**
     * 判断状态是否修改
     * @return /
     */
    UserLinkTask  select(@Param("id") String userLinkTaskId);

    /**
     * 统计数量
     * @return
     */
    Integer selectCountChTask();
}
