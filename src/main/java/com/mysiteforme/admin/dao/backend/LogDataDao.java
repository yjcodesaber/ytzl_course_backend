package com.mysiteforme.admin.dao.backend;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.ChErrorLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 */
public interface LogDataDao extends BaseMapper<ChErrorLog> {


    /**
     * @param page
     * @return
     */
    List<ChErrorLog> selectLogData(Page<ChErrorLog> page,@Param(value = "ew") EntityWrapper<ChErrorLog> wrapper);
}
