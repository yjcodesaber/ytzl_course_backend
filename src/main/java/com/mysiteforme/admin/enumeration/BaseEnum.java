package com.mysiteforme.admin.enumeration;

/**
 * 枚举，基类。
 *
 * @param <T> 数据库存储的java类型
 */
public interface BaseEnum<T> {

    /**
     * 存取到数据库中的值.
     *
     * @return 结果。
     */
    public T getCode();

    /**
     * 获取文本。
     *
     * @return 结果。
     */
    public String getText();

}
