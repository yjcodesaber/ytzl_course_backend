package com.mysiteforme.admin.enumeration;

import java.util.Objects;

/**
 * 枚举，商品状态。
 */
public enum ProductStatus implements BaseEnum<Integer> {

    PRODUCT_STATUS_SELLING(100, "在售中"),
    PRODUCT_STATUS_SELL_OVER(200, "已售罄"),
    PRODUCT_STATUS_SELL_END(300, "已下架");

    private Integer code;

    private String text;

    ProductStatus(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public static ProductStatus fromCode(Integer code) {
        for (ProductStatus item : ProductStatus.values()) {
            if (Objects.equals(code, item.getCode())) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getText() {
        return text;
    }}
