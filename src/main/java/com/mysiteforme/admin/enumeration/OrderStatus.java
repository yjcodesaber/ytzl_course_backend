package com.mysiteforme.admin.enumeration;


import java.util.Objects;

/**
 * 枚举，订单状态。
 */
public enum OrderStatus implements BaseEnum<Integer> {
    ORDER_STATUS_UNPAID(100, "未付款"),
    ORDER_STATUS_PAID(200, "已付款"),
    ORDER_STATUS_COMPLETED(300, "已确认"),
    ORDER_STATUS_INVALID(400, "已失效");

    private Integer code;

    private String text;

    OrderStatus(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public static OrderStatus fromCode(Integer code) {
        for (OrderStatus item : OrderStatus.values()) {
            if (Objects.equals(code, item.getCode())) {
                return item;
            }
        }
        throw new IllegalArgumentException();
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getText() {
        return text;
    }}
