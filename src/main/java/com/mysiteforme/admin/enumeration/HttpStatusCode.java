package com.mysiteforme.admin.enumeration;


/**
 * 枚举，HTTP请求状态码。
 */
public enum HttpStatusCode {
    HTTP_SUCCESS(200, "请求成功"),
    HTTP_BAD_REQUEST(400, "请求错误"),
    HTTP_NOT_FOUND(404, "资源不存在"),
    HTTP_INTERNAL_SERVER_ERROR(500, "服务器发生错误");

    private final int code;

    private final String message;

    HttpStatusCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }}
