package com.mysiteforme.admin.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 商品分类表
 * </p>
 *
 * @author wangl
 * @since 2019-04-11
 */
@TableName("tab_category")
public class TabCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品分类编号，主键自增。
     */
    @TableId(value = "c_id", type = IdType.AUTO)
    private Integer cId;
    /**
     * 分类名称。
     */
    @TableField("c_name")
    private String cName;

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }


    @Override
    public String toString() {
        return "TabCategory{" +
                ", cId=" + cId +
                ", cName=" + cName +
                "}";
    }
}
