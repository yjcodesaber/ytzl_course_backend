package com.mysiteforme.admin.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author wangl
 * @since 2019-04-11
 */
@TableName("tab_product")
public class TabProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品编号，主键自增。
     */
    @TableId(value = "p_id", type = IdType.AUTO)
    private Integer pId;

    /**
     * 创建时间。
     */
    @TableField("p_creation_time")
    private Date pCreationTime;
    /**
     * 轮播图片地址，多个用英文逗号隔开。
     */
    @TableField("p_banner")
    private String pBanner;
    /**
     * 证书图片地址，多个用英文逗号隔开。
     */
    @TableField("p_certificate")
    private String pCertificate;
    /**
     * 封面图片地址
     */
    @TableField("p_poster")
    private String pPoster;
    /**
     * 详情图片地址，多个用英文逗号隔开。
     */
    @TableField("p_detail")
    private String pDetail;
    /**
     * 折扣率。
     */
    @TableField("p_discount")
    private BigDecimal pDiscount;
    /**
     * 更新时间。
     */
    @TableField("p_modify_time")
    private Date pModifyTime;
    /**
     * 商品名称。
     */
    @TableField("p_name")
    private String pName;


    /**
     * 商品介绍视频
     */
    @TableField("p_introduction_video")
    private String pIntroductionVideo;
    /**
     * 规格参数，JSON。
     */
    @TableField("p_parameters")
    private String pParameters;

    /**
     * 商品单价(分)。
     */
    @TableField("p_price")
    private Long pPrice;
    /**
     * 商品的销售码。
     */
    @TableField("p_sale_id")
    private String pSaleId;
    /**
     * 商品状态，枚举值。100
     */
    @TableField("p_status")
    private Integer pStatus;
    /**
     * 外键，所属商品分类编号。
     */
    @TableField("c_id")
    private Integer cId;
    /**
     * 排序
     */
    @TableField("p_sort")
    private Integer pSort;

    public String getpIntroductionVideo() {
        return pIntroductionVideo;
    }

    public void setpIntroductionVideo(String pIntroductionVideo) {
        this.pIntroductionVideo = pIntroductionVideo;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getpBanner() {
        return pBanner;
    }

    public void setpBanner(String pBanner) {
        this.pBanner = pBanner;
    }

    public String getpCertificate() {
        return pCertificate;
    }

    public void setpCertificate(String pCertificate) {
        this.pCertificate = pCertificate;
    }

    public Date getpCreationTime() {
        return pCreationTime;
    }

    public void setpCreationTime(Date pCreationTime) {
        this.pCreationTime = pCreationTime;
    }

    public String getpDetail() {
        return pDetail;
    }

    public void setpDetail(String pDetail) {
        this.pDetail = pDetail;
    }

    public BigDecimal getpDiscount() {
        return pDiscount;
    }

    public void setpDiscount(BigDecimal pDiscount) {
        this.pDiscount = pDiscount;
    }

    public Date getpModifyTime() {
        return pModifyTime;
    }

    public void setpModifyTime(Date pModifyTime) {
        this.pModifyTime = pModifyTime;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpParameters() {
        return pParameters;
    }

    public void setpParameters(String pParameters) {
        this.pParameters = pParameters;
    }

    public String getpPoster() {
        return pPoster;
    }

    public void setpPoster(String pPoster) {
        this.pPoster = pPoster;
    }

    public Long getpPrice() {
        return pPrice;
    }

    public void setpPrice(Long pPrice) {
        this.pPrice = pPrice;
    }

    public String getpSaleId() {
        return pSaleId;
    }

    public void setpSaleId(String pSaleId) {
        this.pSaleId = pSaleId;
    }

    public Integer getpStatus() {
        return pStatus;
    }

    public void setpStatus(Integer pStatus) {
        this.pStatus = pStatus;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public Integer getpSort() {
        return pSort;
    }

    public void setpSort(Integer pSort) {
        this.pSort = pSort;
    }


    @Override
    public String toString() {
        return "TabProduct{" +
                ", pId=" + pId +
                ", pBanner=" + pBanner +
                ", pCertificate=" + pCertificate +
                ", pCreationTime=" + pCreationTime +
                ", pDetail=" + pDetail +
                ", pDiscount=" + pDiscount +
                ", pModifyTime=" + pModifyTime +
                ", pName=" + pName +
                ", pParameters=" + pParameters +
                ", pPoster=" + pPoster +
                ", pPrice=" + pPrice +
                ", pSaleId=" + pSaleId +
                ", pStatus=" + pStatus +
                ", cId=" + cId +
                ", pSort=" + pSort +
                "}";
    }
}
