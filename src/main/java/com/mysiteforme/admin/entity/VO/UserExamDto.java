package com.mysiteforme.admin.entity.VO;

import java.util.Date;

/**
 * 试题统计数据返回dto
 * @author Administrator
 */
public class UserExamDto {
    /**
     * 正确数
     */
    private Integer correctNumber;
    /**
     * 总数
     */
    private Integer totalNumber;
    /**
     * 时间
     */
    private Date searchDate;

    public Integer getCorrectNumber() {
        return correctNumber;
    }

    public void setCorrectNumber(Integer correctNumber) {
        this.correctNumber = correctNumber;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
    }

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }
}
