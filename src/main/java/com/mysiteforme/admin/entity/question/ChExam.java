package com.mysiteforme.admin.entity.question;

import java.io.Serializable;

/**
 * json转化list实体类
 * @author Administrator
 */
public class ChExam implements Serializable {
    //编号
   private String  no;
   //选项
   private String  options;
   //答案
   private String answer;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
