package com.mysiteforme.admin.entity.question;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;


public class ChQuestionVO implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     * 主键编号
     */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String keyWords;


    private String anwser1;

    private String anwser2;

    private String anwser3;

    private String anwser4;

    private String a1ok;

    private String a2ok;

    private String a3ok;

    private String a4ok;

    public String getA1ok() {
        return a1ok;
    }

    public void setA1ok(String a1ok) {
        this.a1ok = a1ok;
    }

    public String getA2ok() {
        return a2ok;
    }

    public void setA2ok(String a2ok) {
        this.a2ok = a2ok;
    }

    public String getA3ok() {
        return a3ok;
    }

    public void setA3ok(String a3ok) {
        this.a3ok = a3ok;
    }

    public String getA4ok() {
        return a4ok;
    }

    public void setA4ok(String a4ok) {
        this.a4ok = a4ok;
    }

    public String getAnwser1() {
        return anwser1;
    }

    public void setAnwser1(String anwser1) {
        this.anwser1 = anwser1;
    }

    public String getAnwser2() {
        return anwser2;
    }

    public void setAnwser2(String anwser2) {
        this.anwser2 = anwser2;
    }

    public String getAnwser3() {
        return anwser3;
    }

    public void setAnwser3(String anwser3) {
        this.anwser3 = anwser3;
    }

    public String getAnwser4() {
        return anwser4;
    }

    public void setAnwser4(String anwser4) {
        this.anwser4 = anwser4;
    }

    /**
     * 所属知识点编号
     */
    @TableField("lore_point_id")
    private String lorePointId;
    /**
     * 题目描述
     */
    @TableField("question_describe")
    private String questionDescribe;

    /**
     * 类型（1，单选2，多选）
     */
    @TableField("question_type")
    private Integer questionType;


    public String getLorePointId() {
        return lorePointId;
    }

    public void setLorePointId(String lorePointId) {
        this.lorePointId = lorePointId;
    }

    public String getQuestionDescribe() {
        return questionDescribe;
    }


    public void setQuestionDescribe(String questionDescribe) {
        this.questionDescribe = questionDescribe;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public String getQuestionAnalysis() {
        return questionAnalysis;
    }

    public void setQuestionAnalysis(String questionAnalysis) {
        this.questionAnalysis = questionAnalysis;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(String creationBy) {
        this.creationBy = creationBy;
    }

    public Date getModifyedDate() {
        return modifyedDate;
    }

    public void setModifyedDate(Date modifyedDate) {
        this.modifyedDate = modifyedDate;
    }

    public String getModifyedBy() {
        return modifyedBy;
    }

    public void setModifyedBy(String modifyedBy) {
        this.modifyedBy = modifyedBy;
    }

    /**
     * 答案解析
     */
    @TableField("question_analysis")
    private String questionAnalysis;
    /**
     * 创建时间
     */
    @TableField("creation_date")
    private Date creationDate;
    /**
     * 创建人
     */
    @TableField("creation_by")
    private String creationBy;
    /**
     * 修改时间
     */
    @TableField("modifyed_date")
    private Date modifyedDate;
    /**
     * 修改人
     */
    @TableField("modifyed_by")
    private String modifyedBy;

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 删除标记
     */
    @TableField("del_flag")
    private Integer delFlag;

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }
}

