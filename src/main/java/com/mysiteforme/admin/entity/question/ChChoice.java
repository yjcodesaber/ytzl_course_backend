package com.mysiteforme.admin.entity.question;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@TableName("ch_choice")
public class ChChoice implements Serializable {

    private static final long serialVersionUID = 1L;


	/**
	 * 主键编号
	 */
	@TableId("id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    /**
     * 选项描述
     */
	@TableField("choice_describe")
	private String choiceDescribe;
    /**
     * 题库编号
     */
	@TableField("question_id")
	private String questionId;
    /**
     * 是否答案（1，是2，不是）
     */
	@TableField("is_answer")
	private Integer isAnswer;
    /**
     * 创建时间
     */
	@TableField("creation_date")
	private Date creationDate;
    /**
     * 创建人
     */
	@TableField("creation_by")
	private String creationBy;
    /**
     * 修改时间
     */
	@TableField("modifyed_date")
	private Date modifyedDate;
    /**
     * 修改人
     */
	@TableField("modifyed_by")
	private String modifyedBy;

	public String getChoiceDescribe() {
		return choiceDescribe;
	}

	public void setChoiceDescribe(String choiceDescribe) {
		this.choiceDescribe = choiceDescribe;
	}
	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public Integer getIsAnswer() {
		return isAnswer;
	}

	public void setIsAnswer(Integer isAnswer) {
		this.isAnswer = isAnswer;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}


	@Override
	public String toString() {
		return "ChChoice{" +
			", choiceDescribe=" + choiceDescribe +
			", questionId=" + questionId +
			", isAnswer=" + isAnswer +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
