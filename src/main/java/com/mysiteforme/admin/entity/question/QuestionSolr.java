package com.mysiteforme.admin.entity.question;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author jayden
 */
@Data
public class QuestionSolr implements Serializable {

    @Field
    private String id;
    @Field
    private String keywords;
    @Field
    private String lorePointId;
    @Field
    private String lorePointName;
    @Field
    private String sectionId;
    @Field
    private String sectionName;
    @Field
    private String unitId;
    @Field
    private String unitName;
    @Field
    private String courseId;
    @Field
    private String courseName;
    @Field
    private String questionDescribe;
    @Field
    private Integer questionType;
    @Field
    private String questionAnalysis;
    @Field
    private Date creationDate;
    @Field
    private List<String> choiceList;


}
