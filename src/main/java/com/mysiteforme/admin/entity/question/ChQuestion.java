package com.mysiteforme.admin.entity.question;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@TableName("ch_question")
public class ChQuestion implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键编号
	 */
	@TableId("id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@TableId("key_words")
	private String keyWords;

	/**
	 * 所属知识点编号
	 */
	@TableField("lore_point_id")
	private String lorePointId;
	/**
	 * 题目描述
	 */
	@TableField("question_describe")
	private String questionDescribe;

	/**
	 * 类型（1，单选2，多选）
	 */
	@TableField("question_type")
	private Integer questionType;


	public String getLorePointId() {
		return lorePointId;
	}

	public void setLorePointId(String lorePointId) {
		this.lorePointId = lorePointId;
	}

	public String getQuestionDescribe() {
		return questionDescribe;
	}

	@Override
	public String toString() {
		return "ChQuestion{" +
				"id='" + id + '\'' +
				", lorePointId='" + lorePointId + '\'' +
				", questionDescribe='" + questionDescribe + '\'' +
				", questionType=" + questionType +
				", questionAnalysis='" + questionAnalysis + '\'' +
				", creationDate=" + creationDate +
				", creationBy='" + creationBy + '\'' +
				", modifyedDate=" + modifyedDate +
				", modifyedBy='" + modifyedBy + '\'' +
				", delFlag=" + delFlag +
				'}';
	}

	public void setQuestionDescribe(String questionDescribe) {
		this.questionDescribe = questionDescribe;
	}

	public Integer getQuestionType() {
		return questionType;
	}

	public void setQuestionType(Integer questionType) {
		this.questionType = questionType;
	}

	public String getQuestionAnalysis() {
		return questionAnalysis;
	}

	public void setQuestionAnalysis(String questionAnalysis) {
		this.questionAnalysis = questionAnalysis;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}

	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}

	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}

	/**
	 * 答案解析
	 */
	@TableField("question_analysis")
	private String questionAnalysis;
	/**
	 * 创建时间
	 */
	@TableField("creation_date")
	private Date creationDate;
	/**
	 * 创建人
	 */
	@TableField("creation_by")
	private String creationBy;
	/**
	 * 修改时间
	 */
	@TableField("modifyed_date")
	private Date modifyedDate;
	/**
	 * 修改人
	 */
	@TableField("modifyed_by")
	private String modifyedBy;

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	/**
	 * 删除标记
	 */
	@TableField("del_flag")
	private Integer delFlag;

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
}
