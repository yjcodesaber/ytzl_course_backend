package com.mysiteforme.admin.entity.question;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@TableName("ch_user_exam")
public class ChUserExam implements Serializable {

    private static final long serialVersionUID = 1L;


	/**
	 * 主键编号
	 */
	@TableId("id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    /**
     * 用户编号
     */
	@TableField("user_id")
	private String userId;
    /**
     * 自测卷名称
     */
	@TableField("exam_name")
	private String examName;
    /**
     * 所属知识体系编号（课程id:单元id:章节id:知识点id）
     */
	@TableField("catalogue_id")
	private String catalogueId;
    /**
     * 自测类型（1，模拟练习2，单元复习3，知识点巩固）
     */
	private Integer type;
    /**
     * json：[{编号:题目编号
     */
	@TableField("exam_list")
	private String examList;
    /**
     * [a
     */
	@TableField("answer_result")
	private String answerResult;
    /**
     * 是否完成答卷(1，完成2，未完成)
     */
	@TableField("is_complete")
	private Integer isComplete;
    /**
     * 创建时间
     */
	@TableField("creation_date")
	private Date creationDate;
    /**
     * 创建人
     */
	@TableField("creation_by")
	private String creationBy;
    /**
     * 修改时间
     */
	@TableField("modifyed_date")
	private Date modifyedDate;
    /**
     * 修改人
     */
	@TableField("modifyed_by")
	private String modifyedBy;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}
	public String getCatalogueId() {
		return catalogueId;
	}

	public void setCatalogueId(String catalogueId) {
		this.catalogueId = catalogueId;
	}
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	public String getExamList() {
		return examList;
	}

	public void setExamList(String examList) {
		this.examList = examList;
	}
	public String getAnswerResult() {
		return answerResult;
	}

	public void setAnswerResult(String answerResult) {
		this.answerResult = answerResult;
	}
	public Integer getIsComplete() {
		return isComplete;
	}

	public void setIsComplete(Integer isComplete) {
		this.isComplete = isComplete;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}


	@Override
	public String toString() {
		return "ChUserExam{" +
			", userId=" + userId +
			", examName=" + examName +
			", catalogueId=" + catalogueId +
			", type=" + type +
			", examList=" + examList +
			", answerResult=" + answerResult +
			", isComplete=" + isComplete +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
