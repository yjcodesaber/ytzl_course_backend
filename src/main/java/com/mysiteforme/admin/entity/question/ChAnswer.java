package com.mysiteforme.admin.entity.question;

import java.io.Serializable;

/**
 * json转化list实体类
 * @author Administrator
 */
public class ChAnswer implements Serializable {
    //
    private String res;
    //
    private Boolean isSuccess;

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }


}
