package com.mysiteforme.admin.entity.data;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * 学习数据实体类
 * @author Administrator
 */
@TableName("ch_inside_user")
public class Data implements Serializable {

    @TableField(value = "real_name")
    private String realName;

    @TableField(value = "study_time_long",exist = false)
    private String studyTimeLong;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getStudyTimeLong() {
        return studyTimeLong;
    }

    public void setStudyTimeLong(String studyTimeLong) {
        this.studyTimeLong = studyTimeLong;
    }
    @Override
    public String toString() {
        return "Data{" +
                "realName='" + realName + '\'' +
                ", studyTimeLong='" + studyTimeLong + '\'' +
                '}';
    }
}
