package com.mysiteforme.admin.entity.base;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-05-10
 */
@TableName("ch_image")
public class ChImage implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableField("id")
    private String id;


    /**
     * 目标id
     */
	@TableField("target_id")
	private String targetId;
    /**
     * 图片地址
     */
	@TableField("image_url")
	private String imageUrl;
    /**
     * 图片类型 (1:用户头像 2:收款收据图片)
     */
	private Integer type;
    /**
     * 位置
     */
	private Integer position;
    /**
     * 创建日期
     */
	@TableField("creation_date")
	private Date creationDate;
    /**
     * 修改时间
     */
	@TableField("modify_date")
	private Date modifyDate;

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}


	@Override
	public String toString() {
		return "ChImage{" +
			", targetId=" + targetId +
			", imageUrl=" + imageUrl +
			", type=" + type +
			", position=" + position +
			", creationDate=" + creationDate +
			", modifyDate=" + modifyDate +
			"}";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
