package com.mysiteforme.admin.entity.course;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@TableName("ch_section")
public class ChSection implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId("id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
    /**
     * 章节名称
     */
	@TableField("section_name")
	private String sectionName;
    /**
     * 章节描述
     */
	@TableField("section_describe")
	private String sectionDescribe;
    /**
     * 单元Id
     */
	@TableField("unit_id")
	private String unitId;
    /**
     * 激活状态(1:已激活
     */
	@TableField("is_activated")
	private Integer isActivated;
    /**
     * 创建时间
     */
	@TableField("creation_date")
	private Date creationDate;
    /**
     * 创建人
     */
	@TableField("creation_by")
	private String creationBy;
    /**
     * 修改时间
     */
	@TableField("modifyed_date")
	private Date modifyedDate;
    /**
     * 修改人
     */
	@TableField("modifyed_by")
	private String modifyedBy;

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getSectionDescribe() {
		return sectionDescribe;
	}

	public void setSectionDescribe(String sectionDescribe) {
		this.sectionDescribe = sectionDescribe;
	}
	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Integer getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Integer isActivated) {
		this.isActivated = isActivated;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}


	@Override
	public String toString() {
		return "ChSection{" +
			", sectionName=" + sectionName +
			", sectionDescribe=" + sectionDescribe +
			", unitId=" + unitId +
			", isActivated=" + isActivated +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
