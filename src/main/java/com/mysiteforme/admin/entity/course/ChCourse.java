package com.mysiteforme.admin.entity.course;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 *
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@TableName("ch_course")
public class ChCourse implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程id
     */
    @TableId("id")
    private String id;

    /**
     * 课程名称
     */
    @TableField("course_name")
    private String courseName;
    /**
     * 课程描述
     */
    @TableField("course_describe")
    private String courseDescribe;
    /**
     * 激活状态(1:已激活,2:未激活)
     */
    @TableField("is_activated")
    private Integer isActivated;
    /**
     * 创建时间
     */
    @TableField("creation_date")
    private Date creationDate;
    /**
     * 创建人
     */
    @TableField("creation_by")
    private String creationBy;
    /**
     * 修改时间
     */
    @TableField("modifyed_date")
    private Date modifyedDate;
    /**
     * 修改人
     */
    @TableField("modifyed_by")
    private String modifyedBy;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseDescribe() {
        return courseDescribe;
    }

    public void setCourseDescribe(String courseDescribe) {
        this.courseDescribe = courseDescribe;
    }

    public Integer getIsActivated() {
        return isActivated;
    }

    public void setIsActivated(Integer isActivated) {
        this.isActivated = isActivated;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(String creationBy) {
        this.creationBy = creationBy;
    }

    public Date getModifyedDate() {
        return modifyedDate;
    }

    public void setModifyedDate(Date modifyedDate) {
        this.modifyedDate = modifyedDate;
    }

    public String getModifyedBy() {
        return modifyedBy;
    }

    public void setModifyedBy(String modifyedBy) {
        this.modifyedBy = modifyedBy;
    }


    @Override
    public String toString() {
        return "ChCourse{" +
                ", id=" + id +
                ", courseName=" + courseName +
                ", courseDescribe=" + courseDescribe +
                ", isActivated=" + isActivated +
                ", creationDate=" + creationDate +
                ", creationBy=" + creationBy +
                ", modifyedDate=" + modifyedDate +
                ", modifyedBy=" + modifyedBy +
                "}";
    }
}
