package com.mysiteforme.admin.entity.course;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@TableName("ch_evaluate")
public class ChEvaluate implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableField("id")
    private String id;

    /**
     * 评价问题
     */
	@TableField("evaluate_content")
	private String evaluateContent;
    /**
     * 用户id
     */
	@TableField("user_id")
	private String userId;
    /**
     * 知识点id
     */
	@TableField("point_id")
	private String pointId;
    /**
     * 星级(1
     */
	@TableField("evaluate_star")
	private Integer evaluateStar;
    /**
     * 是否激活（显示评价）
     */
	@TableField("is_activated")
	private Integer isActivated;
    /**
     * 评价时间
     */
	@TableField("creation_date")
	private Date creationDate;

	/**
	 * 审核时间
	 */
	@TableField("check_date")
	private Date checkDate;

	@TableField(exist = false)
	private String lorePointName;

	@TableField(exist = false)
	private String realName;
	/*
	*点赞
	 */
	@TableField("evaluate_like")
	private Long evaluateLike;




	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEvaluateContent() {
		return evaluateContent;
	}

	public void setEvaluateContent(String evaluateContent) {
		this.evaluateContent = evaluateContent;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPointId() {
		return pointId;
	}

	public void setPointId(String pointId) {
		this.pointId = pointId;
	}

	public Integer getEvaluateStar() {
		return evaluateStar;
	}

	public void setEvaluateStar(Integer evaluateStar) {
		this.evaluateStar = evaluateStar;
	}

	public Integer getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Integer isActivated) {
		this.isActivated = isActivated;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public Long getEvaluateLike() {
		return evaluateLike;
	}

	public void setEvaluateLike(Long evaluateLike) {
		this.evaluateLike = evaluateLike;
	}

	public String getLorePointName() {
		return lorePointName;
	}

	public void setLorePointName(String lorePointName) {
		this.lorePointName = lorePointName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}

	@Override
	public String toString() {
		return "ChEvaluate{" +
				"id='" + id + '\'' +
				", evaluateContent='" + evaluateContent + '\'' +
				", userId='" + userId + '\'' +
				", pointId='" + pointId + '\'' +
				", evaluateStar=" + evaluateStar +
				", isActivated=" + isActivated +
				", creationDate=" + creationDate +
				", checkDate=" + checkDate +
				", lorePointName='" + lorePointName + '\'' +
				", evaluateLike='" + evaluateLike + '\'' +
				", realName='" + realName + '\'' +
				'}';
	}
}
