package com.mysiteforme.admin.entity.course;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@TableName("ch_major")
public class ChMajor implements Serializable {

    private static final long serialVersionUID = 1L;


	@TableField("id")
	private String id;
    /**
     * 专业名称
     */
	@TableField("major_name")
	private String majorName;

	@TableField("creation_date")
	private Date creationDate;

	@TableField("creation_by")
	private String creationBy;

	@TableField("modifyed_date")
	private Date modifyedDate;

	@TableField("modifyed_by")
	private String modifyedBy;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}


	@Override
	public String toString() {
		return "ChMajor{" +
			", majorName=" + majorName +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
