package com.mysiteforme.admin.entity.course;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@TableName("ch_problem")
public class ChProblem implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableField("id")
    private String id;

	/**
     * 问题标题
     */
	@TableField("problem_title")
	private String problemTitle;
    /**
     * 问题描述
     */
	@TableField("problem_description")
	private String problemDescription;
    /**
     * 用户id
     */
	@TableField("user_id")
	private String userId;
    /**
     * 知识点id
     */
	@TableField("point_id")
	private String pointId;
    /**
     * 回复内容
     */
	@TableField("answer_content")
	private String answerContent;



	/**
     * 回复人id
     */
	@TableField("answer_id")
	private String answerId;

	/**
	 * 问题状态
	 */
	@TableField("problem_status")
	private Integer problemStatus;
    /**
     * 提问时间
     */
	@TableField("creation_date")
	private Date creationDate;
	/**
	 * 知识点名称
	 */
	@TableField(exist = false)
	private String lorePointName;
	/**
	 * 用户名称
	 */
	@TableField(exist = false)
	private String realName;
	/**
	 * 回复人名称
	 */
	@TableField(exist = false)
	private String loginName;

	@TableField(exist = false)
	private String sectionName;

	@TableField(exist = false)
	private String unitName;

	@TableField(exist = false)
	private String courseName;


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProblemTitle() {
		return problemTitle;
	}

	public void setProblemTitle(String problemTitle) {
		this.problemTitle = problemTitle;
	}

	public String getProblemDescription() {
		return problemDescription;
	}

	public void setProblemDescription(String problemDescription) {
		this.problemDescription = problemDescription;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPointId() {
		return pointId;
	}

	public void setPointId(String pointId) {
		this.pointId = pointId;
	}

	public String getAnswerContent() {
		return answerContent;
	}

	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}

	public String getAnswerId() {
		return answerId;
	}

	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getLorePointName() {
		return lorePointName;
	}

	public void setLorePointName(String lorePointName) {
		this.lorePointName = lorePointName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getProblemStatus() {
		return problemStatus;
	}

	public void setProblemStatus(Integer problemStatus) {
		this.problemStatus = problemStatus;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	@Override
	public String toString() {
		return "ChProblem{" +
				"id='" + id + '\'' +
				", problemTitle='" + problemTitle + '\'' +
				", problemDescription='" + problemDescription + '\'' +
				", userId='" + userId + '\'' +
				", pointId='" + pointId + '\'' +
				", answerContent='" + answerContent + '\'' +
				", answerId='" + answerId + '\'' +
				", problemStatus=" + problemStatus +
				", creationDate=" + creationDate +
				", lorePointName='" + lorePointName + '\'' +
				", realName='" + realName + '\'' +
				", loginName='" + loginName + '\'' +
				", sectionName='" + sectionName + '\'' +
				", unitName='" + unitName + '\'' +
				", courseName='" + courseName + '\'' +
				'}';
	}
}
