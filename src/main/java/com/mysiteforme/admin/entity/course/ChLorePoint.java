package com.mysiteforme.admin.entity.course;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@TableName("ch_lore_point")
public class ChLorePoint implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId("id")
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 视频时间长度
	 */
	@TableField("video_time_long")
	private long videoTimeLong;

	public long getVideoTimeLong() {
		return videoTimeLong;
	}

	public void setVideoTimeLong(long videoTimeLong) {
		this.videoTimeLong = videoTimeLong;
	}

	/**
     * 知识点名称
     */
	@TableField("lore_point_name")
	private String lorePointName;
    /**
     * 知识点描述
     */
	@TableField("lore_point_describe")
	private String lorePointDescribe;
    /**
     * 视频地址
     */
	@TableField("video_url")
	private String videoUrl;
    /**
     * 章节Id
     */
	@TableField("section_id")
	private String sectionId;
    /**
     * 激活状态(1:已激活
     */
	@TableField("is_activated")
	private Integer isActivated;
    /**
     * 创建时间
     */
	@TableField("creation_date")
	private Date creationDate;
    /**
     * 创建人
     */
	@TableField("creation_by")
	private String creationBy;
    /**
     * 修改时间
     */
	@TableField("modifyed_date")
	private Date modifyedDate;
    /**
     * 修改人
     */
	@TableField("modifyed_by")
	private String modifyedBy;

	public String getLorePointName() {
		return lorePointName;
	}

	public void setLorePointName(String lorePointName) {
		this.lorePointName = lorePointName;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getLorePointDescribe() {
		return lorePointDescribe;
	}

	public void setLorePointDescribe(String lorePointDescribe) {
		this.lorePointDescribe = lorePointDescribe;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public String getSectionId() {
		return sectionId;
	}

	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}
	public Integer getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Integer isActivated) {
		this.isActivated = isActivated;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}


	@Override
	public String toString() {
		return "ChLorePoint{" +
			", lorePointName=" + lorePointName +
			", lorePointDiscrible=" + lorePointDescribe +
			", videoUrl=" + videoUrl +
			", sectionId=" + sectionId +
			", isActivated=" + isActivated +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
