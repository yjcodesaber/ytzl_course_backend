package com.mysiteforme.admin.entity.course;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@TableName("ch_subject")
public class ChSubject implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableField("id")
	private String id;
    /**
     * 课程名称
     */
	@TableField("subject_name")
	private String subjectName;
	@TableField("creation_date")
	private Date creationDate;
	@TableField("creation_by")
	private String creationBy;
	@TableField("modifyed_date")
	private Date modifyedDate;
	@TableField("modifyed_by")
	private String modifyedBy;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}


	@Override
	public boolean equals(Object obj) {
		return ((ChSubject)obj).getId().equals(this.id);
	}

	@Override
	public String toString() {
		return "ChSubject{" +
				"id='" + id + '\'' +
				", subjectName='" + subjectName + '\'' +
				", creationDate=" + creationDate +
				", creationBy='" + creationBy + '\'' +
				", modifyedDate=" + modifyedDate +
				", modifyedBy='" + modifyedBy + '\'' +
				'}';
	}
}
