package com.mysiteforme.admin.entity.course;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@TableName("ch_subject_link_major")
public class ChSubjectLinkMajor implements Serializable {

    private static final long serialVersionUID = 1L;


	@TableField("id")
	private String id;
    /**
     * 课程Id
     */
	@TableField("subject_id")
	private String subjectId;
    /**
     * 专业Id
     */
	@TableField("major_id")
	private String majorId;
	@TableField("creation_date")
	private Date creationDate;
	@TableField("creation_by")
	private String creationBy;
	@TableField("modifyed_date")
	private Date modifyedDate;
	@TableField("modifyed_by")
	private String modifyedBy;

	@TableField(exist = false)
	private String subjectName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	@Override
	public String toString() {
		return "ChSubjectLinkMajor{" +
			", subjectId=" + subjectId +
			", majorId=" + majorId +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
