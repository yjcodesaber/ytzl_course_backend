package com.mysiteforme.admin.entity.course;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 *
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@TableName("ch_unit")
public class ChUnit implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键ID
     */
    @TableId("id")
    private String id;


    /**
     * 单元名称
     */
    @TableField("unit_name")
    private String unitName;
    /**
     * 单元描述
     */
    @TableField("unit_describe")
    private String unitDescribe;
    /**
     * 课程Id
     */
    @TableField("course_id")
    private String courseId;
    /**
     * 激活状态(1:已激活
     */
    @TableField("is_activated")
    private Integer isActivated;
    /**
     * 创建时间
     */
    @TableField("creation_date")
    private Date creationDate;
    /**
     * 创建人
     */
    @TableField("creation_by")
    private String creationBy;
    /**
     * 修改时间
     */
    @TableField("modifyed_date")
    private Date modifyedDate;
    /**
     * 修改人
     */
    @TableField("modifyed_by")
    private String modifyedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitDescribe() {
        return unitDescribe;
    }

    public void setUnitDescribe(String unitDescribe) {
        this.unitDescribe = unitDescribe;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public Integer getIsActivated() {
        return isActivated;
    }

    public void setIsActivated(Integer isActivated) {
        this.isActivated = isActivated;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(String creationBy) {
        this.creationBy = creationBy;
    }

    public Date getModifyedDate() {
        return modifyedDate;
    }

    public void setModifyedDate(Date modifyedDate) {
        this.modifyedDate = modifyedDate;
    }

    public String getModifyedBy() {
        return modifyedBy;
    }

    public void setModifyedBy(String modifyedBy) {
        this.modifyedBy = modifyedBy;
    }


    @Override
    public String toString() {
        return "ChUnit{" +
                ", id=" + id +
                ", unitName=" + unitName +
                ", unitDescribe=" + unitDescribe +
                ", courseId=" + courseId +
                ", isActivated=" + isActivated +
                ", creationDate=" + creationDate +
                ", creationBy=" + creationBy +
                ", modifyedDate=" + modifyedDate +
                ", modifyedBy=" + modifyedBy +
                "}";
    }
}
