package com.mysiteforme.admin.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 日志实体类
 *
 * @author Administrator
 */
@TableName("ch_error_log")
public class ChErrorLog implements Serializable {

    @TableField(value = "id")
    private String id;
    @TableField(value = "module_name")
    private String moduleName;
    @TableField(value = "request_ip")
    private String requestIp;
    @TableField(value = "request_url")
    private String requestUrl;
    @TableField(value = "request_param")
    private String requestParam;
    @TableField(value = "error_msg")
    private String errorMsg;
    @TableField(value = "creation_date")
    private Date creationDate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getRequestIp() {
        return requestIp;
    }

    public void setRequestIp(String requestIp) {
        this.requestIp = requestIp;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestParam() {
        return requestParam;
    }

    public void setRequestParam(String requestParam) {
        this.requestParam = requestParam;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    @Override
    public String toString() {
        return "ChErrorLog{" +
                "id='" + id + '\'' +
                ", moduleName='" + moduleName + '\'' +
                ", requestIp='" + requestIp + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", requestParam='" + requestParam + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
