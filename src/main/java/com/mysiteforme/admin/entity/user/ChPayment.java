package com.mysiteforme.admin.entity.user;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.mysiteforme.admin.base.DataEntity;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@TableName("ch_payment")
public class ChPayment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学号
     */
	@TableField("user_number")
	private String userNumber;

	/**
	 * 主键编号
	 */
	@TableField("id")
	private String id;

    @TableField("receivables_id")
    private String receivablesId;

	@TableField("payee_name")
	private String payeeName;

    /**
     * 支付方式1支付宝，2微信，3银行卡，4现金
     */
	@TableField("payment_mode")
	private Integer paymentMode;

	public ChPayment() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
     * 下次缴费日期
     */
	@TableField("payment_next_time")
	private Date paymentNextTime;
    /**
     * 剩余缴费金额
     */
	@TableField("payment_spare_money")
	private Long paymentSpareMoney;
    /**
     * 支付金额
     */
	@TableField("payment_money")
	private Long paymentMoney;
    /**
     * 学生审核状态1审核，2未审核
     */
	@TableField("payment_student_status")
	private Integer paymentStudentStatus;
	@TableField("creation_date")
	private Date creationDate;
	@TableField("creation_by")
	private String creationBy;
	@TableField("modifyed_date")
	private Date modifyedDate;
	@TableField("modifyed_by")
	private String modifyedBy;
    /**
     * 支付类型1全款，2分期
     */
	@TableField("payment_type")
	private Integer paymentType;
    /**
     * 缴费日期
     */
	@TableField("payment_now_time")
	private Date paymentNowTime;
    /**
     * 校长审核状态1审核，2未审核
     */
	@TableField("payment_principal_status")
	private Integer paymentPrincipalStatus;
    /**
     * 支付的学期
     */
	@TableField("payment_term")
	private Integer paymentTerm;

	@TableField(exist = false)
	private String receivablesName;

	@TableField(exist = false)
	private String receivablesAccount;

	@TableField(exist = false)
	private String realName;

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}
	public Integer getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(Integer paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Date getPaymentNextTime() {
		return paymentNextTime;
	}

	public void setPaymentNextTime(Date paymentNextTime) {
		this.paymentNextTime = paymentNextTime;
	}
	public Long getPaymentSpareMoney() {
		return paymentSpareMoney;
	}

	public void setPaymentSpareMoney(Long paymentSpareMoney) {
		this.paymentSpareMoney = paymentSpareMoney;
	}
	public Long getPaymentMoney() {
		return paymentMoney;
	}

	public void setPaymentMoney(Long paymentMoney) {
		this.paymentMoney = paymentMoney;
	}
	public Integer getPaymentStudentStatus() {
		return paymentStudentStatus;
	}

	public void setPaymentStudentStatus(Integer paymentStudentStatus) {
		this.paymentStudentStatus = paymentStudentStatus;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}
	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}
	public Date getPaymentNowTime() {
		return paymentNowTime;
	}

	public void setPaymentNowTime(Date paymentNowTime) {
		this.paymentNowTime = paymentNowTime;
	}
	public Integer getPaymentPrincipalStatus() {
		return paymentPrincipalStatus;
	}

	public void setPaymentPrincipalStatus(Integer paymentPrincipalStatus) {
		this.paymentPrincipalStatus = paymentPrincipalStatus;
	}
	public Integer getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(Integer paymentTerm) {
		this.paymentTerm = paymentTerm;
	}


	@Override
	public String toString() {
		return "ChPayment{" +
			", userNumber=" + userNumber +
			", paymentMode=" + paymentMode +
			", paymentNextTime=" + paymentNextTime +
			", paymentSpareMoney=" + paymentSpareMoney +
			", paymentMoney=" + paymentMoney +
			", paymentStudentStatus=" + paymentStudentStatus +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			", paymentType=" + paymentType +
			", paymentNowTime=" + paymentNowTime +
			", paymentPrincipalStatus=" + paymentPrincipalStatus +
			", paymentTerm=" + paymentTerm +
			"}";
	}


	public String getReceivablesId() {
		return receivablesId;
	}

	public void setReceivablesId(String receivablesId) {
		this.receivablesId = receivablesId;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getReceivablesName() {
		return receivablesName;
	}

	public void setReceivablesName(String receivablesName) {
		this.receivablesName = receivablesName;
	}

	public String getReceivablesAccount() {
		return receivablesAccount;
	}

	public void setReceivablesAccount(String receivablesAccount) {
		this.receivablesAccount = receivablesAccount;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
}
