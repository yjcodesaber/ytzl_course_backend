package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author yj
 * @since 2019-05-07
 */
@TableName("ch_receivables")
public class ChReceivables implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("id")
    private String id;
    /**
     * 收款人姓名
     */
    @TableField("receivables_name")
    private String receivablesName;
    /**
     * 收款类型1支付宝，2微信，3银行卡，4现金
     */
    @TableField("receivables_type")
    private Integer receivablesType;
    /**
     * 收款账户
     */
    @TableField("receivables_account")
    private String receivablesAccount;
    /**
     * 银行名称
     */
    @TableField("bank_name")
    private String bankName;

    @TableField("creation_date")
    private Date creationDate;

    @TableField("creation_by")
    private String creationBy;

    @TableField("modifyed_date")
    private Date modifyedDate;

    @TableField("modifyed_by")
    private String modifyedBy;

    @TableField("disable")
    private Integer disable;

    public String getReceivablesName() {
        return receivablesName;
    }

    public void setReceivablesName(String receivablesName) {
        this.receivablesName = receivablesName;
    }

    public Integer getReceivablesType() {
        return receivablesType;
    }

    public void setReceivablesType(Integer receivablesType) {
        this.receivablesType = receivablesType;
    }

    public String getReceivablesAccount() {
        return receivablesAccount;
    }

    public void setReceivablesAccount(String receivablesAccount) {
        this.receivablesAccount = receivablesAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationBy() {
        return creationBy;
    }

    public void setCreationBy(String creationBy) {
        this.creationBy = creationBy;
    }

    public Date getModifyedDate() {
        return modifyedDate;
    }

    public void setModifyedDate(Date modifyedDate) {
        this.modifyedDate = modifyedDate;
    }

    public String getModifyedBy() {
        return modifyedBy;
    }

    public void setModifyedBy(String modifyedBy) {
        this.modifyedBy = modifyedBy;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ChReceivables{" +
                "id='" + id + '\'' +
                ", receivablesName='" + receivablesName + '\'' +
                ", receivablesType=" + receivablesType +
                ", receivablesAccount='" + receivablesAccount + '\'' +
                ", bankName='" + bankName + '\'' +
                ", creationDate=" + creationDate +
                ", creationBy='" + creationBy + '\'' +
                ", modifyedDate=" + modifyedDate +
                ", modifyedBy='" + modifyedBy + '\'' +
                ", disable=" + disable +
                '}';
    }
}
