package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务实体类
 *
 * @author Administrator
 */
@TableName("ch_task")
public class ChTask implements Serializable {

    private static final long serialVersionUID = 1L;
    //编号
    @TableField("id")
    private String id;
    //任务名称
    @TableField("task_name")
    private String taskName;
    //任务描述
    @TableField("task_describe")
    private String taskDescribe;
    //任务类型 1.视频学习 2.题库测验
    @TableField("task_type")
    private Integer taskType;
    //任务开始时间
    @TableField("task_start_time")
    private Date taskStartTime;
    //任务结束时间
    @TableField("task_end_time")
    private Date taskEndTime;
    //指派用户id
    @TableField("sys_user_id")
    private String sysUserId;
    //创建时间
    @TableField("task_create_time")
    private Date createTime;
    //完成时间
    @TableField(value = "complete_time", exist = false)
    private Date completeTime;
    //用户名
    @TableField(value = "real_name", exist = false)
    private String realName;
    //老师评语
    @TableField(value = "task_comments", exist = false)
    private String taskComments;
    //任务状态
    @TableField(value = "task_status", exist = false)
    private Integer taskStatus;
    //完成备注
    @TableField(value = "task_note", exist = false)
    private String taskNote;
    //学生id
    @TableField(value = "user_id", exist = false)
    private String userId;

    @TableField(exist = false)
    private Integer isShowExamine;

    @TableField(exist = false)
    private String  userLinkTaskId;

    public String getUserLinkTaskId() {
        return userLinkTaskId;
    }

    public void setUserLinkTaskId(String userLinkTaskId) {
        this.userLinkTaskId = userLinkTaskId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescribe() {
        return taskDescribe;
    }

    public void setTaskDescribe(String taskDescribe) {
        this.taskDescribe = taskDescribe;
    }

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public Date getTaskStartTime() {
        return taskStartTime;
    }

    public void setTaskStartTime(Date taskStartTime) {
        this.taskStartTime = taskStartTime;
    }

    public Date getTaskEndTime() {
        return taskEndTime;
    }

    public void setTaskEndTime(Date taskEndTime) {
        this.taskEndTime = taskEndTime;
    }

    public String getSysUserId() {
        return sysUserId;
    }

    public void setSysUserId(String sysUserId) {
        this.sysUserId = sysUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getTaskComments() {
        return taskComments;
    }

    public void setTaskComments(String taskComments) {
        this.taskComments = taskComments;
    }

    public Integer getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Integer taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskNote() {
        return taskNote;
    }

    public void setTaskNote(String taskNote) {
        this.taskNote = taskNote;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getIsShowExamine() {
        return isShowExamine;
    }

    public void setIsShowExamine(Integer isShowExamine) {
        this.isShowExamine = isShowExamine;
    }

    @Override
    public String toString() {
        return "ChTask{" +
                "id='" + id + '\'' +
                ", taskName='" + taskName + '\'' +
                ", taskDescribe='" + taskDescribe + '\'' +
                ", taskType=" + taskType +
                ", taskStartTime=" + taskStartTime +
                ", taskEndTime=" + taskEndTime +
                ", sysUserId='" + sysUserId + '\'' +
                ", createTime=" + createTime +
                ", completeTime=" + completeTime +
                ", realName='" + realName + '\'' +
                ", taskComments='" + taskComments + '\'' +
                ", taskStatus=" + taskStatus +
                ", taskNote='" + taskNote + '\'' +
                ", studentId='" + userId + '\'' +
                ", isShowExamine=" + isShowExamine +
                '}';
    }
}
