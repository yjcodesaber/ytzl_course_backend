package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 学习时长实体类
 * @author Administrator
 */
@TableName("ch_study_video_record")
public class ChStudyVideoRecord  implements Serializable {

    @TableField(value = "id")
    private String id;
    @TableField(value = "user_id")
    private String userId;

    @TableField(value = "study_time_long")
    private Integer studyTimeLong;

    @TableField(value = "creation_date")
    private Date creationDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getStudyTimeLong() {
        return studyTimeLong;
    }

    public void setStudyTimeLong(Integer studyTimeLong) {
        this.studyTimeLong = studyTimeLong;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }



}
