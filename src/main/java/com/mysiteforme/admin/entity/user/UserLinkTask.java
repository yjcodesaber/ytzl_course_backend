package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Administrator
 */
@TableName("ch_user_link_task")
public class UserLinkTask  implements Serializable {

    @TableField("id")
    private String id;
    @TableField("user_id")
    private String userId;
    @TableField("task_id")
    private String  taskId;
    @TableField("task_comments")
    private String taskComments;
    @TableField("task_status")
    private Integer taskStatus;
    @TableField("task_note")
    private String taskNote;
    @TableField("complete_time")
    private Date completeTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskComments() {
        return taskComments;
    }

    public void setTaskComments(String taskComments) {
        this.taskComments = taskComments;
    }

    public Integer getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(Integer taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskNote() {
        return taskNote;
    }

    public void setTaskNote(String taskNote) {
        this.taskNote = taskNote;
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }


    @Override
    public String toString() {
        return "UserLinkTask{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", taskId='" + taskId + '\'' +
                ", taskComments='" + taskComments + '\'' +
                ", taskStatus=" + taskStatus +
                ", taskNote='" + taskNote + '\'' +
                ", completeTime=" + completeTime +
                '}';
    }
}
