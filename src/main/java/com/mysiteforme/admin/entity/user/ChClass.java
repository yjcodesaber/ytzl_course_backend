package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@TableName("ch_class")
public class ChClass implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableField("id")
	private String id;
	@TableField("class_name")
	private String className;
	@TableField("teacher_id")
	private String teacherId;
	@TableField("head_teacher_id")
	private String headTeacherId;
	@TableField("department_id")
	private String departmentId;
	@TableField("creation_date")
	private Date creationDate;
	@TableField("creation_by")
	private String creationBy;
	@TableField("modifyed_date")
	private Date modifyedDate;
	@TableField("modifyed_by")
	private String modifyedBy;
	@TableField(exist = false)
	private String schoolId;
	@TableField(exist = false)
	private String schoolName;
	@TableField(exist = false)
	private String departmentName;
	@TableField(exist = false)
	private String headTeacherName;
	@TableField(exist = false)
	private String teacherName;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getHeadTeacherName() {
		return headTeacherName;
	}

	public void setHeadTeacherName(String headTeacherName) {
		this.headTeacherName = headTeacherName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getHeadTeacherId() {
		return headTeacherId;
	}

	public void setHeadTeacherId(String headTeacherId) {
		this.headTeacherId = headTeacherId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	@Override
	public String toString() {
		return "ChClass{" +
				"id='" + id + '\'' +
				", className='" + className + '\'' +
				", teacherId='" + teacherId + '\'' +
				", headTeacherId='" + headTeacherId + '\'' +
				", departmentId='" + departmentId + '\'' +
				", creationDate=" + creationDate +
				", creationBy='" + creationBy + '\'' +
				", modifyedDate=" + modifyedDate +
				", modifyedBy='" + modifyedBy + '\'' +
				'}';
	}
}
