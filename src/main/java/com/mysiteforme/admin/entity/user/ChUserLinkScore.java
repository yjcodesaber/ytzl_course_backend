package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@TableName("ch_user_link_score")
public class ChUserLinkScore implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableField("id")
	private String id;
    /**
     * 学生学号
     */
	@TableField("user_number")
	private String userNumber;
	/**
	 * 专业编号
	 */
	@TableField("major_id")
	private String majorId;
    /**
     * 课程编号
     */
	@TableField("subject_id")
	private String subjectId;
    /**
     * 学期
     */
	@TableField("school_term")
	private Integer schoolTerm;
    /**
     * 上机成绩
     */
	@TableField("machine_test_result")
	private String machineTestResult;
    /**
     * 笔试成绩
     */
	@TableField("written_test_result")
	private String writtenTestResult;

	@TableField("creation_date")
	private Date creationDate;
	@TableField("creation_by")
	private String creationBy;
	@TableField("modifyed_date")
	private Date modifyedDate;
	@TableField("modifyed_by")
	private String modifyedBy;

	@TableField(exist = false)
	private String subjectName;
	@TableField(exist = false)
	private String majorName;

	@TableField(exist = false)
	private String realName;


	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}
	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public Integer getSchoolTerm() {
		return schoolTerm;
	}

	public void setSchoolTerm(Integer schoolTerm) {
		this.schoolTerm = schoolTerm;
	}
	public String getMachineTestResult() {
		return machineTestResult;
	}

	public void setMachineTestResult(String machineTestResult) {
		this.machineTestResult = machineTestResult;
	}
	public String getWrittenTestResult() {
		return writtenTestResult;
	}

	public void setWrittenTestResult(String writtenTestResult) {
		this.writtenTestResult = writtenTestResult;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	@Override
	public String toString() {
		return "ChUserLinkScore{" +
			", userNumber=" + userNumber +
			", subjectId=" + subjectId +
			", schoolTerm=" + schoolTerm +
			", machineTestResult=" + machineTestResult +
			", writtenTestResult=" + writtenTestResult +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
