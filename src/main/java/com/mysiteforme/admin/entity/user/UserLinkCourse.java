package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@TableName("ch_user_link_course")
public class UserLinkCourse implements Serializable {

    private static final long serialVersionUID = 1L;


	@TableField("id")
	private String id;
    /**
     * 用户Id
     */
	@TableField("user_id")
	private String userId;
    /**
     * 课程Id
     */
	@TableField("course_id")
	private String courseId;
    /**
     * 过期时间
     */
	@TableField("exp_date")
	private Date expDate;
    /**
     * 创建时间
     */
	@TableField("creation_date")
	private Date creationDate;
    /**
     * 创建人
     */
	@TableField("creation_by")
	private String creationBy;
    /**
     * 修改时间
     */
	@TableField("modifyed_date")
	private Date modifyedDate;
    /**
     * 修改人
     */
	@TableField("modifyed_by")
	private String modifyedBy;

	//课程名称
	@TableField(exist = false)
	private String courseName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	@Override
	public String toString() {
		return "UserLinkCourse{" +
			", userId=" + userId +
			", courseId=" + courseId +
			", expDate=" + expDate +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
