package com.mysiteforme.admin.entity.user;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@TableName("ch_department")
public class ChDepartment implements Serializable {

    private static final long serialVersionUID = 1L;

	@TableField("id")
	private String id;
    /**
     * 院系名称
     */
	@TableField("department_name")
	private String departmentName;
	/*系主任Id*/
	@TableField("dean_id")
	private String deanId;

    /**
     * 学校编号
     */
	@TableField("school_id")
	private String schoolId;
	@TableField("creation_date")
	private Date creationDate;
	@TableField("creation_by")
	private String creationBy;
	@TableField("modifyed_date")
	private Date modifyedDate;
	@TableField("modifyed_by")
	private String modifyedBy;

	@TableField(exist = false)
	private String schoolName;
	@TableField(exist = false)
	private String deanName;   //系主任名称

	public String getDeanId() {
		return deanId;
	}

	public void setDeanId(String deanId) {
		this.deanId = deanId;
	}

	public String getDeanName() {
		return deanName;
	}

	public void setDeanName(String deanName) {
		this.deanName = deanName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}


	@Override
	public String toString() {
		return "ChDepartment{" +
			", departmentName=" + departmentName +
			", schoolId=" + schoolId +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}
}
