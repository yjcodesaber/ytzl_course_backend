package com.mysiteforme.admin.entity.user;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@TableName("ch_inside_user")
public class ChUser implements Serializable {

    private static final long serialVersionUID = 1L;

	/**
	 * 学生编号
	 */
	@TableField("id")
	private String id;

	@TableField("user_number")
	private String userNumber;

    /**
     * 登录账号（手机号）
     */
	@TableField("user_account")
	private String userAccount;
    /**
     * 登录密码
     */
	@TableField("user_password")
	private String userPassword;
    /**
     * 真实姓名
     */
	@TableField("real_name")
	private String realName;
    /**
     * 身份证号
     */
	@TableField("id_card")
	private String idCard;
	@TableField("class_id")
	private String classId;

    /**
     * 入学时间
     */

	@TableField("in_school_date")
	private Date inSchoolDate;
    /**
     * 毕业时间
     */

	@TableField("out_school_date")
	private Date outSchoolDate;
    /**
     * 激活状态(1:已激活
     */
	@TableField("is_activated")
	private Integer isActivated;
    /**
     * 创建时间
     */
	@TableField("creation_date")
	private Date creationDate;
    /**
     * 创建人
     */
	@TableField("creation_by")
	private String creationBy;
    /**
     * 修改时间
     */
	@TableField("modifyed_date")
	private Date modifyedDate;
    /**
     * 修改人
     */
	@TableField("modifyed_by")
	private String modifyedBy;

	/*外键*/
	@TableField(exist = false)
	private String className;

	@TableField(exist = false)
	private String schoolName;

	@TableField(exist = false)
	private String schoolId;

	@TableField(exist = false)
	private String departmentName;

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public Date getInSchoolDate() {
		return inSchoolDate;
	}

	public void setInSchoolDate(Date inSchoolDate) {
		this.inSchoolDate = inSchoolDate;
	}
	public Date getOutSchoolDate() {
		return outSchoolDate;
	}

	public void setOutSchoolDate(Date outSchoolDate) {
		this.outSchoolDate = outSchoolDate;
	}
	public Integer getIsActivated() {
		return isActivated;
	}

	public void setIsActivated(Integer isActivated) {
		this.isActivated = isActivated;
	}
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationBy() {
		return creationBy;
	}

	public void setCreationBy(String creationBy) {
		this.creationBy = creationBy;
	}
	public Date getModifyedDate() {
		return modifyedDate;
	}

	public void setModifyedDate(Date modifyedDate) {
		this.modifyedDate = modifyedDate;
	}
	public String getModifyedBy() {
		return modifyedBy;
	}

	public void setModifyedBy(String modifyedBy) {
		this.modifyedBy = modifyedBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(String userNumber) {
		this.userNumber = userNumber;
	}

	@Override
	public String toString() {
		return "ChUser{" +
			", userAccount=" + userAccount +
			", userPassword=" + userPassword +
			", realName=" + realName +
			", idCard=" + idCard +
			", classId=" + classId +
			", inSchoolDate=" + inSchoolDate +
			", outSchoolDate=" + outSchoolDate +
			", isActivated=" + isActivated +
			", creationDate=" + creationDate +
			", creationBy=" + creationBy +
			", modifyedDate=" + modifyedDate +
			", modifyedBy=" + modifyedBy +
			"}";
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
}
