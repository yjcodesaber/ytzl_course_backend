package com.mysiteforme.admin.controller.course;

import com.mysiteforme.admin.entity.course.ChUnit;
import com.mysiteforme.admin.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mysiteforme.admin.service.course.ChUnitService;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.mysiteforme.admin.annotation.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/chUnit")
public class ChUnitController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChUnitController.class);

    @Autowired
    private ChUnitService chUnitService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzlcourse/chUnit/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChUnit> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                  @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                  ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        if (!map.isEmpty()) {
        }
        LayerData<ChUnit> layerData = new LayerData<>();
        EntityWrapper<ChUnit> wrapper = new EntityWrapper<>();
        if (map.get("unitName") != null && map.get("unitName") != "") {
            wrapper.like("unit_name", (String) map.get("unitName"));
        }
        if (map.get("courseId") != null && map.get("courseId") != "") {
            wrapper.eq("course_id", (String) map.get("courseId"));
        }

        Page<ChUnit> pageData = chUnitService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @PostMapping("/getUnitClass")
    @ResponseBody
    @SysLog("请求单元类型列表")
    public List<ChUnit> listData(ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        EntityWrapper<ChUnit> wrapper = new EntityWrapper<>();
        if (map.get("unitId") != null && map.get("unitId") != "")
            wrapper.eq("id", (String) map.get("unitId"));
        if (map.get("courseId") != null && map.get("courseId") != "") {
            wrapper.eq("course_id", (String) map.get("courseId"));
        }
        return chUnitService.selectList(wrapper);
    }


    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add() {
        return "/ytzlcourse/chUnit/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChUnit chUnit) {
        if (null == chUnit.getCourseId() || "" == chUnit.getCourseId()) {
            return RestResponse.failure("所属课程不能为空");
        }
        chUnit.setId(IdWorker.getId());
        chUnit.setCreationDate(new Date());
        boolean insert = chUnitService.insertUnit(chUnit);
        if (insert) {
            return RestResponse.success();
        } else return RestResponse.failure("添加失败");
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit() {
        return "/ytzlcourse/chUnit/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChUnit chUnit) {
        if (null == chUnit.getId() || "" == chUnit.getId()) {
            return RestResponse.failure("ID不能为空");
        }
        chUnit.setModifyedDate(new Date());
        boolean update = chUnitService.updateUnitById(chUnit);
        if (update) {
            return RestResponse.success();
        } else return RestResponse.failure("添加失败");
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id", required = false) String id) {
        if (null == id || "" == id) {
            return RestResponse.failure("ID不能为空");
        }
        boolean del = chUnitService.deleteUnitById(id);
        if (del) {
            return RestResponse.success();
        }else
            return RestResponse.failure("系统繁忙");
    }

}