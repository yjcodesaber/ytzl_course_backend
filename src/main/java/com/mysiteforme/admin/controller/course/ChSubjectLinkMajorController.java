package com.mysiteforme.admin.controller.course;

import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.service.course.ChSubjectLinkMajorService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Controller
@RequestMapping("/chSubjectLinkMajor")
public class ChSubjectLinkMajorController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChSubjectLinkMajorController.class);

    @Autowired
    private ChSubjectLinkMajorService chSubjectLinkMajorService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list(){
        return "/ytzlcourse/chSubjectLinkMajor/list";
    }



    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(@RequestParam(value = "subjectIds")String subjectIds,
                            @RequestParam(value = "majorId")String majorId){
        chSubjectLinkMajorService.deleteByMajorId(majorId);
        if(subjectIds==null || subjectIds==""){
            return RestResponse.success("该专业删除相关课程成功");
        }
        if(majorId==null || majorId==""){
            return RestResponse.failure("专业获取失败");
        }

        String[] subjectIdList = subjectIds.split(",");
        StringBuffer buffer=new StringBuffer();
        for(int i=0;i<subjectIdList.length;i++){
            buffer.append("('"+IdWorker.getId()+"','"+subjectIdList[i]+"','"+majorId+"')");
            if(i<subjectIdList.length-1){
                buffer.append(",");
            }
        }
        System.out.println(buffer.toString());
        chSubjectLinkMajorService.addList(buffer.toString());
        return RestResponse.success();
    }




}