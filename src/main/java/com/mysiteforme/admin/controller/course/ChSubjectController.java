package com.mysiteforme.admin.controller.course;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.course.ChSubject;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.course.ChSubjectService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Controller
@RequestMapping("/chSubject")
public class ChSubjectController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChSubjectController.class);

    @Autowired
    private ChSubjectService chSubjectService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzlcourse/chSubject/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChSubject> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                     @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                     ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChSubject> layerData = new LayerData<>();
        EntityWrapper<ChSubject> wrapper = new EntityWrapper<>();
        if (map.get("searchInfo") != null && map.get("searchInfo") != "") {
            String searchInfo = (String) map.get("searchInfo");
            wrapper.like("id", "%" + searchInfo + "%")
                    .or().like("subject_name", "%" + searchInfo + "%");
        }
        Page<ChSubject> pageData = chSubjectService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("findAll")
    @ResponseBody
    @SysLog("请求列表数据")
    public List<ChSubject> findSubject(String majorId) {
        List<ChSubject> subjectList = chSubjectService.findAll();
        List<ChSubject> list = chSubjectService.findByMajorId(majorId);
        subjectList.removeAll(list);
        return subjectList;
    }

    @PostMapping("findSubjectName")
    @SysLog("查找专业相关课程名称数据")
    @ResponseBody
    public List<ChSubject> findByMajorId(@RequestParam(value = "majorId")String majorId) {
        if (majorId != null && majorId != "") {
            List<ChSubject> list = chSubjectService.findByMajorId(majorId);
            return list;
        }
        return null;
    }


    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add() {
        return "/ytzlcourse/chSubject/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChSubject chSubject) {
        chSubject.setId(IdWorker.getId());
        chSubject.setCreationDate(new Date());
        chSubject.setCreationBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chSubjectService.add(chSubject);
        return RestResponse.success();
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit() {
        return "/ytzlcourse/chSubject/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChSubject chSubject) {
        if (null == chSubject.getId() || "" == chSubject.getId()) {
            return RestResponse.failure("ID不能为空");
        }
        chSubject.setModifyedDate(new Date());
        chSubject.setModifyedBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chSubjectService.editById(chSubject);
        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id", required = false) String id) {
        if (null == id || "" == id) {
            return RestResponse.failure("ID不能为空");
        }
        chSubjectService.cutById(id);
        return RestResponse.success();
    }

}