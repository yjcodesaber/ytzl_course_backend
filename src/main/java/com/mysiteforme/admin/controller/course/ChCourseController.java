package com.mysiteforme.admin.controller.course;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.course.ChCourse;
import com.mysiteforme.admin.service.course.ChCourseService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/chCourse")
public class ChCourseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChCourseController.class);

    @Autowired
    private ChCourseService chCourseService;



    @GetMapping("list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzlcourse/chCourse/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChCourse> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                    ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChCourse> layerData = new LayerData<>();
        EntityWrapper<ChCourse> wrapper = new EntityWrapper<>();
        wrapper.isWhere(false);
        if (map.get("course_name") != null) {
            wrapper.like("course_name", (String) map.get("course_name"));
        }
        if (!map.isEmpty()) {
        }
        Page<ChCourse> pageData = chCourseService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @PostMapping("/getCourseClass")
    @ResponseBody
    @SysLog("请求课程类型列表")
    public List<ChCourse> listData() {
        EntityWrapper<ChCourse> wrapper = new EntityWrapper<>();
        return chCourseService.selectList(wrapper);
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add() {
        return "/ytzlcourse/chCourse/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChCourse chCourse) {

        chCourse.setId(IdWorker.getId());
        chCourse.setCreationDate(new Date());
        Boolean insert = chCourseService.insertCourse(chCourse);
        if (insert) {
            return RestResponse.success();
        } else
            return RestResponse.failure("系统繁忙");

    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit() {
        return "/ytzlcourse/chCourse/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChCourse chCourse) {
        if (null == chCourse.getId() || "" == chCourse.getId()) {
            return RestResponse.failure("ID不能为空");
        }
        System.out.println(chCourse.toString());
        chCourse.setModifyedDate(new Date());
        Boolean update = chCourseService.updateCourseById(chCourse);
        if (update) {
            return RestResponse.success();
        } else
            return RestResponse.failure("系统繁忙");

    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id", required = false) String id) {
        if (null == id || "" == id) {
            return RestResponse.failure("ID不能为空");
        }

        Boolean del = chCourseService.deleteCourseById(id);
        if (del) {
            return RestResponse.success();
        } else
            return RestResponse.failure("系统繁忙");

    }


}