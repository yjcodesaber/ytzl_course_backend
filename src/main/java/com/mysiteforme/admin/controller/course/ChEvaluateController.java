package com.mysiteforme.admin.controller.course;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mysiteforme.admin.entity.course.ChEvaluate;
import com.mysiteforme.admin.service.course.ChEvaluateService;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.mysiteforme.admin.annotation.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@Controller
@RequestMapping("/chEvaluate")
public class ChEvaluateController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChEvaluateController.class);

    @Autowired
    private ChEvaluateService chEvaluateService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list(){
        return "/ytzlcourse/chEvaluate/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChEvaluate> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                      @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                      @RequestParam(value = "condition",required = false) String condition,
                                      @RequestParam(value = "isActivated",required = false) String isActivated){
        LayerData<ChEvaluate> layerData = new LayerData<>();
        EntityWrapper<ChEvaluate> wrapper = new EntityWrapper<>();
        wrapper.isWhere(true);
        if (StringUtils.isNotEmpty(condition)){
            wrapper.like("real_name","%"+condition+"%").or().like("lore_point_name","%"+condition+"%");
        }
        if (StringUtils.isNotEmpty(isActivated) && isActivated!=""){
           wrapper.andNew().eq("e.is_activated",isActivated);
        }
        Page<ChEvaluate> pageData = chEvaluateService.selectByWrapper(new Page<>(page,limit),wrapper);
        //处理数据

        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add(){
        return "/ytzlcourse/chEvaluate/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChEvaluate chEvaluate){
        if(chEvaluate.getEvaluateStar() == null){
            return RestResponse.failure("星级(1不能为空");
        }
        chEvaluateService.insert(chEvaluate);
        return RestResponse.success();
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(){
        return "/ytzlcourse/chEvaluate/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChEvaluate chEvaluate){

        if(chEvaluate.getId() == null||chEvaluate.getId()==""){
            return RestResponse.failure("ID不能为空");
        }
        chEvaluate.setCheckDate(new Date());
       // DbContextHolder.setDbType(DBTypeEnum.db_summer);

        String chEvaluateId = chEvaluate.getId();
        Integer selectStartById = chEvaluateService.selectStartById(chEvaluateId);
        if (selectStartById==1){
            return RestResponse.failure("不能审核，该评论已审核");
        }
        Boolean updateEvaluateById = chEvaluateService.updateEvaluateById(chEvaluate);
       // if(updateEvaluateById){
            return RestResponse.success();
//        }else{
//            return RestResponse.failure("系统繁忙");
//        }
    }

    @PostMapping("disabled")
    @ResponseBody
    @SysLog("修改为禁用")
    public RestResponse disabled(@RequestParam(value = "id",required = false)String id){
        System.out.println("就是看看进来不");
        if(null == id || "" == id){
            return RestResponse.failure("ID不能为空");
        }
        Boolean updateErrorById = chEvaluateService.updateErrorById(id);
        // chEvaluateService.updateById(chEvaluate);
        if (updateErrorById){
            return RestResponse.success();
        }else{
            return RestResponse.failure("系统繁忙");
        }

    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除评论")
    public RestResponse delete(@RequestParam(value = "id",required = false)String id){
        //System.out.println("就是看看进来不");
        if(null == id || "" == id){
            return RestResponse.failure("ID不能为空");
        }
        Boolean deleteEvaluateById = chEvaluateService.deleteEvaluateById(id);
        // chEvaluateService.updateById(chEvaluate);
        if (deleteEvaluateById){
            return RestResponse.success();
        }else{
            return RestResponse.failure("系统繁忙");
        }
    }


}