package com.mysiteforme.admin.controller.course;

import com.mysiteforme.admin.realm.AuthRealm;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mysiteforme.admin.entity.course.ChProblem;
import com.mysiteforme.admin.service.course.ChProblemService;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.mysiteforme.admin.annotation.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@Controller
@RequestMapping("/chProblem")
public class ChProblemController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChProblemController.class);

    @Autowired
    private ChProblemService chProblemService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list(){
        return "/ytzlcourse/chProblem/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChProblem> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                     @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                     @RequestParam(value = "realName",required = false) String realName,
                                     @RequestParam(value = "problemTitle",required = false) String problemTitle,
                                     @RequestParam(value = "problemStatus",required = false) String problemStatus){
        LayerData<ChProblem> layerData = new LayerData<>();
        EntityWrapper<ChProblem> wrapper = new EntityWrapper<>();
        wrapper.isWhere(true);
        if (StringUtils.isNotEmpty(realName)){
            wrapper.like("real_name","%"+realName+"%");
        }
        if(StringUtils.isNotEmpty(problemTitle)){
            wrapper.like("problem_title","%"+problemTitle+"%");
        }
        if (StringUtils.isNotEmpty(problemStatus) && !problemStatus.equals("99")){
            wrapper.andNew().eq("problem_status",problemStatus);
        }
        Page<ChProblem> pageData = chProblemService.selectByWrapperToProblem(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add(){
        return "/ytzlcourse/chProblem/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChProblem chProblem){
        chProblemService.insert(chProblem);
        return RestResponse.success();
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(){
        return "/ytzlcourse/chProblem/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChProblem chProblem){
        if(null == chProblem.getId() || "" == chProblem.getId()){
            return RestResponse.failure("ID不能为空");
        }
        chProblem.setAnswerId(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        Boolean updateProblemById = chProblemService.updateProblemById(chProblem);
        if(updateProblemById){
            return RestResponse.success();
        }else{
            return RestResponse.failure("系统繁忙");
        }
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id",required = false)String id){
        if(null == id || ""== id){
            return RestResponse.failure("ID不能为空");
        }
        Boolean del = chProblemService.deleteProblemById(id);
        //chProblemService.updateById(chProblem);
        if (del){
            return RestResponse.success();
        }else{
            return RestResponse.failure("系统繁忙");
        }
    }

}