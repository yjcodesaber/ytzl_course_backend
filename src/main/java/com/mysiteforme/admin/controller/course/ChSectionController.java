package com.mysiteforme.admin.controller.course;

import com.mysiteforme.admin.entity.course.ChSection;
import com.mysiteforme.admin.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mysiteforme.admin.service.course.ChSectionService;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.mysiteforme.admin.annotation.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/chSection")
public class ChSectionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChSectionController.class);

    @Autowired
    private ChSectionService chSectionService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzlcourse/chSection/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChSection> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                     @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                     ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChSection> layerData = new LayerData<>();
        EntityWrapper<ChSection> wrapper = new EntityWrapper<>();
        if (!map.isEmpty()) {
        }
        if (map.get("sectionName") != null && map.get("sectionName") != "") {
            wrapper.like("section_name", (String) map.get("sectionName"));
        }
        if (map.get("unitId") != null && map.get("unitId") != "") {
            wrapper.eq("unit_id", (String) map.get("unitId"));
        }
        Page<ChSection> pageData = chSectionService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @PostMapping("/getSectionClass")
    @ResponseBody
    @SysLog("请求章节列类型列表")
    public List<ChSection> listData(ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        EntityWrapper<ChSection> wrapper = new EntityWrapper<>();
        if (map.get("sectionId") != null && map.get("sectionId") != "")
            wrapper.eq("id", (String) map.get("sectionId"));
        if (map.get("unitId") != null && map.get("unitId") != "") {
            wrapper.eq("unit_id", (String) map.get("unitId"));
        }
        return chSectionService.selectList(wrapper);
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add() {
        return "/ytzlcourse/chSection/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChSection chSection) {
        if (null == chSection.getUnitId() || "" == chSection.getUnitId()) {
            return RestResponse.failure("所属单元不能为空");
        }
        chSection.setId(IdWorker.getId());
        chSection.setCreationDate(new Date());
        boolean insert = chSectionService.insertSection(chSection);
        if (insert) {
            return RestResponse.success();
        }else
            return RestResponse.failure("系统繁忙");
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit() {
        return "/ytzlcourse/chSection/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChSection chSection) {
        if (null == chSection.getId() || "" == chSection.getId()) {
            return RestResponse.failure("ID不能为空");
        }
        chSection.setModifyedDate(new Date());
        boolean update = chSectionService.updateSectionById(chSection);
        if (update) {
            return RestResponse.success();
        }else
            return RestResponse.failure("系统繁忙");
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id", required = false) String id) {
        if (null == id || "" == id) {
            return RestResponse.failure("ID不能为空");
        }
        boolean del = chSectionService.deleteSectionById(id);
        if (del) {
            return RestResponse.success();
        }else
            return RestResponse.failure("系统繁忙");
    }

}