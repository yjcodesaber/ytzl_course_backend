package com.mysiteforme.admin.controller.course;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.course.ChMajor;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.course.ChMajorService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Controller
@RequestMapping("/chMajor")
public class ChMajorController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChMajorController.class);

    @Autowired
    private ChMajorService chMajorService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzlcourse/chMajor/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChMajor> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                   @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                   ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");

        if (!map.isEmpty()) {
        }
        LayerData<ChMajor> layerData = new LayerData<>();
        EntityWrapper<ChMajor> wrapper = new EntityWrapper<>();
        if (map.get("searchInfo") != null && map.get("searchInfo") != "") {
            String searchInfo = (String) map.get("searchInfo");
            wrapper.like("id", "%" + searchInfo + "%")
                    .or().like("major_name", "%" + searchInfo + "%");
        }
        Page<ChMajor> pageData = chMajorService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("getMajorList")
    @ResponseBody
    @SysLog("获取专业列表")
    public List<ChMajor> getMajorList() {
        List<ChMajor> majorList = chMajorService.getMajorList();
        return majorList;
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add() {
        return "/ytzlcourse/chMajor/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChMajor chMajor) {
        chMajor.setId(IdWorker.getId());
        chMajor.setCreationDate(new Date());
        chMajor.setCreationBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chMajorService.add(chMajor);
        return RestResponse.success();
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(String id, Model model) {
        ChMajor chMajor = chMajorService.selectById(id);
        model.addAttribute("chMajor", chMajor);
        return "/ytzlcourse/chMajor/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChMajor chMajor) {
        if (null == chMajor.getId() || "" == chMajor.getId()) {
            return RestResponse.failure("ID不能为空");
        }
        chMajor.setModifyedDate(new Date());
        chMajor.setModifyedBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chMajorService.editById(chMajor);

        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id", required = false) String id) {
        if (null == id || "" == id) {
            return RestResponse.failure("ID不能为空");
        }
        chMajorService.cutById(id);
        return RestResponse.success();
    }

}