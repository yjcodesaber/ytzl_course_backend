package com.mysiteforme.admin.controller.system;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.ChErrorLog;
import com.mysiteforme.admin.service.base.LogDataService;
import com.mysiteforme.admin.util.LayerData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.toolkit.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @author Administrator
 */
@Controller
@RequestMapping("/chLog")
public class LogDataController {

    @Autowired
    private LogDataService logDataService;

    @GetMapping("/list")
    @SysLog("跳转列表")
    public String list() {
        return "/admin/system/chlogdata/list";
    }

    @PostMapping("/list")
    @SysLog("获取数据")
    @ResponseBody
    public LayerData<ChErrorLog> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                      @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                      @RequestParam(value = "moduleName", required = false) String moduleName) {
        LayerData<ChErrorLog> layerData = new LayerData<>();
        EntityWrapper<ChErrorLog> entityWrapper = new EntityWrapper<>();
        entityWrapper.isWhere(true);
        if (StringUtils.isNotEmpty(moduleName) && !"".equals(moduleName)) {
            entityWrapper.like("module_name", "%" + moduleName + "%");
        }
        Page<ChErrorLog> chErrorLogList = logDataService.selectLogData(new Page<>(page, limit),entityWrapper);
        System.out.println("日志数据======================" + JSON.toJSONString(chErrorLogList.getRecords()));
        layerData.setData(chErrorLogList.getRecords());
        layerData.setCount(chErrorLogList.getTotal());
        return layerData;
    }


}
