package com.mysiteforme.admin.controller.user;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.StringUtils;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.user.ChReceivables;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.user.ChReceivablesService;
import com.mysiteforme.admin.util.BankUtil;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-05-07
 */
@Controller
@RequestMapping("/chReceivables")
public class ChReceivablesController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChReceivablesController.class);

    @Autowired
    private ChReceivablesService chReceivablesService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list(){
        return "/ytzluser/chReceivables/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChReceivables> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                         @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                         @RequestParam(value = "userName", required = false) String userName,
                                         ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChReceivables> layerData = new LayerData<>();
        EntityWrapper<ChReceivables> wrapper = new EntityWrapper<>();
        if(!map.isEmpty()){
        }
        wrapper.isWhere(true);
        wrapper.andNew().eq("disable", 1);
        if (StringUtils.isNotEmpty(userName)) {
            wrapper.like("cr.receivables_name", "%" + userName + "%");
        }
        Page<ChReceivables> pageData = chReceivablesService.selectPageByWrapper(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add(){
        return "/ytzluser/chReceivables/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(String receivablesName, String receivablesType, String receivablesAccount) {
        if ("".equals(receivablesName) && receivablesName == null) {
            RestResponse.failure("收货人姓名不能为空");
        }
        if ("".equals(receivablesType) && receivablesType == null) {
            RestResponse.failure("收款类型不能为空");
        }
        if ("".equals(receivablesAccount) && receivablesAccount == null) {
            RestResponse.failure("收款账号不能为空");
        }
        Map<String, Object> param = new HashMap<>();
        if ("3".equals(receivablesType)) {
            String nameOfBank = BankUtil.getNameOfBank(receivablesAccount);
            param.put("bankName", nameOfBank);
        }
        param.put("receivablesName", receivablesName);
        param.put("receivablesType", receivablesType);
        param.put("receivablesAccount", receivablesAccount);
        chReceivablesService.insertReceivables(param);
        return RestResponse.success();

    }





    @PostMapping("getReceivablesInfo")
    @SysLog("获取收款账户信息")
    @ResponseBody
    public RestResponse getReceivablesInfo(String receivablesId){
        List<ChReceivables> receivablesInfo = chReceivablesService.getReceivablesInfoById(receivablesId);
        if (receivablesInfo!=null)return RestResponse.success().setData(receivablesInfo);
        else return RestResponse.failure("系统繁忙");
    }

    @PostMapping("getReceivablesByType")
    @SysLog("获取收款账户信息")
    @ResponseBody
    public RestResponse getReceivablesByMode(@RequestParam("payMode") Integer payMode){
        List<ChReceivables> receivablesInfo = chReceivablesService.getReceivablesByMode(payMode);
        if (receivablesInfo!=null)return RestResponse.success().setData(receivablesInfo);
        else return RestResponse.failure("系统繁忙");
    }


    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(){
        return "/ytzluser/chReceivables/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(String receivablesName, String receivablesType, String receivablesAccount, String id) {
        System.out.println("===============" + id + receivablesName + receivablesType + receivablesAccount + "============");
        if ("".equals(id) && id == null) {
            return RestResponse.failure("ID不能为空");
        }
        if ("".equals(receivablesName) && receivablesName == null) {
            RestResponse.failure("收货人姓名不能为空");
        }
        if ("".equals(receivablesType) && receivablesType == null) {
            RestResponse.failure("收款类型不能为空");
        }
        if ("".equals(receivablesAccount) && receivablesAccount == null) {
            RestResponse.failure("收款账号不能为空");
        }
        Map<String, Object> param = new HashMap<>();
        String modifyedBy = ((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString();
        String nameOfBank = BankUtil.getNameOfBank(receivablesAccount);
        param.put("id", id);
        param.put("receivablesName", receivablesName);
        param.put("receivablesType", receivablesType);
        param.put("receivablesAccount", receivablesAccount);
        param.put("modifyedDate", new Date());
        param.put("modifyedBy", modifyedBy);
        param.put("bankName", nameOfBank);
        chReceivablesService.updateReceivablesById(param);
        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("禁用数据")
    public RestResponse delete(String id) {
        if (null == id && "".equals(id)) {
            return RestResponse.failure("ID不能为空");
        }
        chReceivablesService.updateDisable(id);
        return RestResponse.success();
    }

}