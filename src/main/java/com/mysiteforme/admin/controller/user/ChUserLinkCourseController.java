package com.mysiteforme.admin.controller.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.user.UserLinkCourse;
import com.mysiteforme.admin.service.user.ChUserLinkCourseService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/userLinkCourse")
public class ChUserLinkCourseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChUserLinkCourseController.class);

    @Autowired
    private ChUserLinkCourseService chUserLinkCourseService;


    @GetMapping("/detail")
    @SysLog("跳转学生详情课程页面")
    public String courseDetail(){
        return "/ytzluser/chUserLinkCourse/detail";
    }

    @GetMapping("getList")
    @ResponseBody
    @SysLog("展示某个学生课程列表")
    public LayerData<UserLinkCourse> getList(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                          @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                             @RequestParam(value = "userId")String userId){
        LayerData<UserLinkCourse> layerData = new LayerData<>();
        EntityWrapper<UserLinkCourse> wrapper = new EntityWrapper<>();
        wrapper.isWhere(true);
        if(userId!=null && userId!=""){
            wrapper.eq("ulc.user_id",userId);
        }
        Page<UserLinkCourse> pageData = chUserLinkCourseService.selectByEntityWrapper(new Page<>(page,limit),wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }




    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add(){
        return "/ytzluser/chUserLinkCourse/add";
    }


    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(@RequestParam("userIds") String userIds,
                            @RequestParam("courseId")String courseId,
                            @RequestParam("expDate")String expDate){
        String[] ids= userIds.split(",");
        List<String> idList = new ArrayList<>();
        for (String str:ids){
            idList.add(str);
        }
        //判断学生已有课程中有没有该课程，并比较过期时间
        for(int i=0;i<idList.size();i++){
            String userId=idList.get(i);
            List<UserLinkCourse> courseList = chUserLinkCourseService.findByUserId(userId);
            for(int j=0;j<courseList.size();j++){
                String courseId1 = courseList.get(j).getCourseId();
                Date expDate1 = courseList.get(j).getExpDate();
                if(courseId1.equals(courseId) &&  expDate1.after(new Date())){
                    idList.remove(i);
                }else if(courseId1.equals(courseId) && expDate1.before(new Date())){
                    String linkId=courseList.get(j).getId();
                    chUserLinkCourseService.deleteInfo(linkId);
                }

            }
        }
        if(idList.size()==0){
            return RestResponse.failure("所选的学生早已拥有该课程且没有过期");
        }

        StringBuffer insertInfo = new StringBuffer();
        for(int i=0;i<idList.size();i++) {
            insertInfo.append("("+IdWorker.getId()+","+idList.get(i)+","+courseId+","+"'"+expDate+"')");
            if(i<ids.length-1){
                insertInfo.append(",");
            }
        }
        boolean insert = chUserLinkCourseService.insertList(insertInfo.toString());
        if(insert)
            return RestResponse.success();
        else
            return RestResponse.failure("系统繁忙");
    }

}