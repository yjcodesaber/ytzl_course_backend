package com.mysiteforme.admin.controller.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.user.ChSchool;
import com.mysiteforme.admin.entity.user.ChUser;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.user.ChSchoolService;
import com.mysiteforme.admin.service.user.ChUserService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/school")
public class ChSchoolController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChSchoolController.class);

    @Autowired
    private ChSchoolService chSchoolService;

    @Resource
    private ChUserService chUserService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list(){
        return "/ytzluser/chSchool/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChSchool> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                      @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                      ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChSchool> layerData = new LayerData<>();
        EntityWrapper<ChSchool> entityWrapper = new EntityWrapper<>();
        entityWrapper.isWhere(true);
        if (map.get("searchInfo")!=null&&map.get("searchInfo")!="") {
            String condition =(String)map.get("searchInfo");
            entityWrapper.like("id","%"+condition+"%")
                    .or().like("school_name","%"+condition+"%");
        }
        Page<ChSchool> pageData = chSchoolService.selectByEntityWrapper(new Page<>(page,limit),entityWrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @PostMapping("getSchoolName")
    @SysLog("查找学校名称")
    @ResponseBody
    public List<ChSchool> listData(ServletRequest request){
        Map map=WebUtils.getParametersStartingWith(request,"");
        EntityWrapper<ChSchool> wrapper=new EntityWrapper<>();
        if(map.get("schoolId")!=null && map.get("schoolId")!=""){
            wrapper.eq("id",(String)map.get("schoolId"));
        }
        return chSchoolService.selectList(wrapper);


    }


    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add(){
        return "/ytzluser/chSchool/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChSchool chSchool){
        chSchool.setId(IdWorker.getId());
        chSchool.setCreationDate(new Date());
        chSchool.setCreationBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chSchoolService.addInfo(chSchool);
        return RestResponse.success();
    }

    @RequestMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(){
        return "/ytzluser/chSchool/edit";
    }



    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChSchool chSchool){
        if(null == chSchool.getId() || ""== chSchool.getId()){
            return RestResponse.failure("ID不能为空");
        }
        chSchool.setModifyedDate(new Date());
        chSchool.setModifyedBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chSchoolService.editById(chSchool);
        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id",required = false)String id){
        if(null == id || "" == id){
            return RestResponse.failure("ID不能为空");
        }
        EntityWrapper<ChUser> wrapper=new EntityWrapper<>();
        wrapper.isWhere(true);
        wrapper.eq("s.id",id);
        List<ChUser> chUserList = chUserService.findByLinkId(wrapper);
        if(chUserList.size()!=0){
            return RestResponse.failure("该学校拥有相关学生信息，不能删除");
        }
        boolean res = chSchoolService.cutById(id);
        if(res){
            return RestResponse.success();
        } else{
            return RestResponse.failure("系统繁忙，请稍后再试");
        }
    }

}