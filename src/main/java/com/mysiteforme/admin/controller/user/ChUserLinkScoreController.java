package com.mysiteforme.admin.controller.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.user.ChUserLinkScore;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.course.ChMajorService;
import com.mysiteforme.admin.service.course.ChSubjectService;
import com.mysiteforme.admin.service.user.ChUserLinkScoreService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Controller
@RequestMapping("chUserLinkScore")
public class ChUserLinkScoreController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChUserLinkScoreController.class);

    @Autowired
    private ChUserLinkScoreService chUserLinkScoreService;

    @Resource
    private ChSubjectService chSubjectService;

    @Resource
    private ChMajorService chMajorService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzluser/chUserLinkScore/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChUserLinkScore> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                           @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                           ServletRequest request) {

        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChUserLinkScore> layerData = new LayerData<>();
        EntityWrapper<ChUserLinkScore> wrapper = new EntityWrapper<>();
        wrapper.isWhere(true);
        if (map.get("selectInfo") != null && map.get("selectInfo") != "") {
            wrapper.like("culs.user_number", "%" + (String) map.get("selectInfo") + "%")
                    .or().like("ciu.real_name", "%" + (String) map.get("selectInfo") + "%");
        }
        if (map.get("schoolTerm") != null && map.get("schoolTerm") != "") {
            wrapper.andNew().eq("culs.school_term", (String) map.get("schoolTerm"));
        }
        if (map.get("majorId") != null && map.get("majorId") != "") {
            wrapper.andNew().eq("culs.major_id", (String) map.get("majorId"));
        }
        if (map.get("subjectId") != null && map.get("subjectId") != "") {
            wrapper.andNew().eq("culs.subject_id", (String) map.get("subjectId"));
        }
        if (map.get("classId") != null && map.get("classId") != "") {
            wrapper.andNew().eq("ciu.class_id", (String) map.get("classId"));
        }
        Page<ChUserLinkScore> pageData = chUserLinkScoreService.selectByEntityWrapper(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add() {
        return "/ytzluser/chUserLinkScore/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChUserLinkScore chUserLinkScore) {
        chUserLinkScore.setId(IdWorker.getId());
        chUserLinkScore.setCreationDate(new Date());
        chUserLinkScore.setCreationBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        boolean add = chUserLinkScoreService.insertScore(chUserLinkScore);
        if (add) return RestResponse.success();
        else return RestResponse.failure("系统繁忙");
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit() {
        return "/ytzluser/chUserLinkScore/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChUserLinkScore chUserLinkScore) {
        if (null == chUserLinkScore.getId() || "" == chUserLinkScore.getId()) {
            return RestResponse.failure("ID不能为空");
        }
        chUserLinkScore.setModifyedBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chUserLinkScore.setModifyedDate(new Date());
        boolean update = chUserLinkScoreService.updateScore(chUserLinkScore);
        if (update) return RestResponse.success();
        else return RestResponse.failure("系统繁忙");
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id", required = false) String id) {
        if (null == id || "" == id) {
            return RestResponse.failure("ID不能为空");
        }
        boolean del = chUserLinkScoreService.deleteScoreById(id);
        if (del) return RestResponse.success();
        else return RestResponse.failure("系统繁忙");
    }

}