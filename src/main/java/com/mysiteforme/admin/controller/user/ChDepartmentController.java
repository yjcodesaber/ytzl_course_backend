package com.mysiteforme.admin.controller.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.user.ChDepartment;
import com.mysiteforme.admin.entity.user.ChUser;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.system.UserService;
import com.mysiteforme.admin.service.user.ChDepartmentService;
import com.mysiteforme.admin.service.user.ChUserService;
import com.mysiteforme.admin.util.IdWorker;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@Controller
@RequestMapping("/department")
public class ChDepartmentController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChDepartmentController.class);

    @Autowired
    private ChDepartmentService chDepartmentService;

    @Autowired
    private UserService userService;

    @Resource
    private ChUserService chUserService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list(){
        LOGGER.info("----------------------");
        return "/ytzluser/chDepartment/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChDepartment> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                      @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                      ServletRequest request){

        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChDepartment> layerData = new LayerData<>();
        EntityWrapper<ChDepartment> wrapper = new EntityWrapper<>();
        wrapper.isWhere(true);
        if (map.get("searchInfo") != null && map.get("searchInfo") !="") {
            String searchInfo = (String) map.get("searchInfo");
            wrapper.like("d.id", "%" + searchInfo + "%")
                    .or().like("d.department_name", "%" + searchInfo + "%");
        }
        if (map.get("schoolId") != null && map.get("schoolId") !="") {
            String schoolId = (String) map.get("schoolId");
            wrapper.andNew().eq("s.id", schoolId);
        }
        Page<ChDepartment> pageData = chDepartmentService.selectByEntityWrapper(new Page<>(page,limit),wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }


    @PostMapping("getDepartmentName")
    @SysLog("查找院系名称")
    @ResponseBody
    public List<ChDepartment> listData(ServletRequest request){
        Map map=WebUtils.getParametersStartingWith(request,"");
        EntityWrapper<ChDepartment> wrapper=new EntityWrapper<>();
        if(map.get("schoolId")!=null && map.get("schoolId")!=""){
            wrapper.eq("school_id",(String)map.get("schoolId"));
        }
        if(map.get("departmentId")!=null && map.get("departmentId")!=""){
            wrapper.eq("id",(String)map.get("departmentId"));
        }
        return chDepartmentService.selectList(wrapper);


    }


    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add(){
        return "/ytzluser/chDepartment/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChDepartment chDepartment){
        if(chDepartment==null){
            return RestResponse.failure("添加失败");
        }
        chDepartment.setId(IdWorker.getId());
        chDepartment.setCreationDate(new Date());
        chDepartment.setCreationBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chDepartmentService.addInfo(chDepartment);
        return RestResponse.success();
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(String id,Model model){
        ChDepartment chDepartment = chDepartmentService.selectById(id);
        model.addAttribute("chDepartment",chDepartment);
        return "/ytzluser/chDepartment/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChDepartment chDepartment){
        if(null == chDepartment.getId() || "" == chDepartment.getId()){
            return RestResponse.failure("ID不能为空");
        }
        chDepartment.setModifyedDate(new Date());
        chDepartment.setModifyedBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chDepartmentService.editById(chDepartment);
        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id",required = false)String id){
        if(null == id || "" == id){
            return RestResponse.failure("ID不能为空");
        }
        EntityWrapper<ChUser> wrapper=new EntityWrapper<>();
        wrapper.isWhere(true);
        wrapper.eq("d.id",id);
        List<ChUser> chUserList = chUserService.findByLinkId(wrapper);
        if(chUserList.size()!=0) return RestResponse.failure("该院系拥有相关学生信息，不能删除");
        chDepartmentService.cutById(id);
        return RestResponse.success();
    }

}