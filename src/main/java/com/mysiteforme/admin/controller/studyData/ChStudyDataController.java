package com.mysiteforme.admin.controller.studyData;

import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.annotation.SysLog;
import com.mysiteforme.admin.entity.data.Data;
import com.mysiteforme.admin.service.data.StudyDataService;
import com.mysiteforme.admin.util.LayerData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 学习数据
 * @author Administrator
 */
@Controller
@RequestMapping("/chData")
public class ChStudyDataController {

    @Autowired
    private StudyDataService studyDataService;

    @GetMapping("/list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzlData/chStudyTime/list";
    }

    @PostMapping("/list")
    @SysLog("请求列表数据")
    @ResponseBody
    public LayerData<Data>  list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                 @RequestParam(value = "limit", defaultValue = "10")Integer limit,
                                 @RequestParam(value = "className",required = false)String classId,
                                 @RequestParam(value = "startTime",required = false)String startTime,
                                 @RequestParam(value = "endTime",required = false)String endTime){
        LayerData<Data> layerData = new LayerData<>();
        Map<String,Object> map  =new HashMap<>();
        if (!classId.equals("0")&&classId!=null){
            map.put("classId",classId);
        }
        if (startTime!=null&&endTime!=null){
            map.put("startTime",startTime);
            map.put("endTime",endTime);
        }
        //AND user.class_id ="639983077690445824"
        //AND record.creation_date BETWEEN DATE_FORMAT("2019-04-21", "%Y-%m-%d")AND date_add(DATE_FORMAT("2019-04-21", "%Y-%m-%d"),INTERVAL 1 DAY)
        Page <Data>  dataPage  = studyDataService.selectData(new Page<>(page, limit),map);
        layerData.setData(dataPage.getRecords());
        layerData.setCount(dataPage.getTotal());
        return  layerData;
    }










}
