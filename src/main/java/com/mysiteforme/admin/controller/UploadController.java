package com.mysiteforme.admin.controller;

import com.mysiteforme.admin.service.system.UploadService;
import com.mysiteforme.admin.util.RestResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author jayden
 */
@RestController
@RequestMapping("/upload")
public class UploadController {


    @Resource
    private UploadService uploadService;


    /**
     * 获取临时秘钥
     *
     * @return
     */
    @GetMapping("/server/sts")
    public RestResponse serverSts(@RequestParam("bucket") String bucket, @RequestParam("region") String region) {
        Map<String, Object> resultMap = uploadService.serverSts(bucket, region);
        return RestResponse.success().setData(resultMap);
    }


}
