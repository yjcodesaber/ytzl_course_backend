package com.mysiteforme.admin.controller;

import com.mysiteforme.admin.entity.Dict;
import com.mysiteforme.admin.service.system.DictService;
import com.mysiteforme.admin.util.Constants;
import com.mysiteforme.admin.util.RestResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author jayden
 */
@RestController
@RequestMapping("/video")
public class VideoController {


    @Resource
    private DictService dictService;


    /**
     * 获取临时秘钥
     *
     * @return
     */
    @GetMapping("/properties")
    public RestResponse properties() {
        Map<String, Object> resultMap = new TreeMap<>();
        //查询图片展示地址和图片展示样式
        List<Dict> dictList = dictService.getDictByType("cos");
        for (Dict dict : dictList) {
            if (dict.getId().equals(Constants.DICT_COS_VIDEO_HOST)) {
                resultMap.put("videoHost", dict.getValue());
            } else if (dict.getId().equals(Constants.DICT_COS_VIDEO_STYLE)) {
                resultMap.put("videoStyle", dict.getValue());
            } else if (dict.getId().equals(Constants.DICT_COS_VIDEO_STYLE_630x400r)) {
                resultMap.put("videoStyle630x400r", dict.getValue());
            }
        }
        return RestResponse.success().setData(resultMap);
    }

}
