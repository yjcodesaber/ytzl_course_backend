package com.mysiteforme.admin.controller.question;

import com.mysiteforme.admin.entity.question.ChChoice;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.question.ChChoiceService;
import com.mysiteforme.admin.util.IdWorker;
import com.xiaoleilu.hutool.date.DateUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.mysiteforme.admin.annotation.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@Controller
@RequestMapping("/chChoice")
public class ChChoiceController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChChoiceController.class);

    @Autowired
    private ChChoiceService chChoiceService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list() {
        return "/ytzlquestion/chChoice/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChChoice> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                    @RequestParam(value = "limit", defaultValue = "10") Integer limit,
                                    ServletRequest request) {
        Map map = WebUtils.getParametersStartingWith(request, "");
        LayerData<ChChoice> layerData = new LayerData<>();
        EntityWrapper<ChChoice> wrapper = new EntityWrapper<>();
        if (!map.isEmpty()) {
            if (map.get("questionId") != null && map.get("questionId") != "") {
                wrapper.eq("question_id", (String) map.get("questionId"));
            }
        }
        Page<ChChoice> pageData = chChoiceService.selectPage(new Page<>(page, limit), wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }


    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(@RequestParam("questionId") String questionId,@RequestParam("choiceDescribe")String choiceDescribe) {
        ChChoice chChoice = new ChChoice();
        chChoice.setChoiceDescribe(choiceDescribe);
        chChoice.setId(IdWorker.getId());
        chChoice.setQuestionId(questionId);
        chChoice.setCreationDate(new Date());
        chChoice.setCreationBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        boolean add = chChoiceService.insertChoice(chChoice);
        if(add)return RestResponse.success();
        else return RestResponse.failure("系统繁忙");
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(Long id, Model model) {
        ChChoice chChoice = chChoiceService.selectById(id);
        model.addAttribute("chChoice", chChoice);
        return "/ytzlquestion/chChoice/edit";
    }

    @PostMapping("/correct")
    @ResponseBody
    @SysLog("设置正确答案")
    public RestResponse correct(String id) {
        boolean correct = chChoiceService.correctById(id);
        if (correct) return RestResponse.success();
        else return RestResponse.failure("系统繁忙");
    }

    @PostMapping("/wrong")
    @ResponseBody
    @SysLog("设置错误答案")
    public RestResponse wrong(String id ) {
        boolean wrong = chChoiceService.wrongById(id);
        if (wrong) return RestResponse.success();
        else return RestResponse.failure("系统繁忙");
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChChoice chChoice) {
        if (null == chChoice.getId() || "" == chChoice.getId()) {
            return RestResponse.failure("ID不能为空");
        }
        chChoice.setModifyedBy(((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString());
        chChoice.setModifyedDate(new Date());
        chChoiceService.updateById(chChoice);
        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id", required = false) String id) {
        if (null == id || "" == id) {
            return RestResponse.failure("ID不能为空");
        }
        boolean del = chChoiceService.deleteChoiceById(id);
        if (del) return RestResponse.success();
        else return RestResponse.failure("系统繁忙");
    }

}