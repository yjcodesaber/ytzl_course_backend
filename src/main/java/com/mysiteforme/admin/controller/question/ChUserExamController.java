package com.mysiteforme.admin.controller.question;

import com.mysiteforme.admin.entity.question.ChUserExam;
import com.mysiteforme.admin.service.question.ChUserExamService;
import com.xiaoleilu.hutool.date.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.util.LayerData;
import com.mysiteforme.admin.util.RestResponse;
import com.mysiteforme.admin.annotation.SysLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *   前端控制器
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@Controller
@RequestMapping("/admin/chUserExam")
public class ChUserExamController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChUserExamController.class);

    @Autowired
    private ChUserExamService chUserExamService;

    @GetMapping("list")
    @SysLog("跳转列表")
    public String list(){
        return "/ytzl/chUserExam/list";
    }

    @PostMapping("list")
    @ResponseBody
    @SysLog("请求列表数据")
    public LayerData<ChUserExam> list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                      @RequestParam(value = "limit",defaultValue = "10")Integer limit,
                                      ServletRequest request){
        Map map = WebUtils.getParametersStartingWith(request, "s_");
        LayerData<ChUserExam> layerData = new LayerData<>();
        EntityWrapper<ChUserExam> wrapper = new EntityWrapper<>();
        wrapper.eq("del_flag",false);
        if(!map.isEmpty()){
        }
        Page<ChUserExam> pageData = chUserExamService.selectPage(new Page<>(page,limit),wrapper);
        layerData.setData(pageData.getRecords());
        layerData.setCount(pageData.getTotal());
        return layerData;
    }

    @GetMapping("add")
    @SysLog("跳转新增页面")
    public String add(){
        return "/ytzl/chUserExam/add";
    }

    @PostMapping("add")
    @SysLog("保存新增数据")
    @ResponseBody
    public RestResponse add(ChUserExam chUserExam){
        if(StringUtils.isBlank(chUserExam.getExamList())){
            return RestResponse.failure("json：[{编号:题目编号不能为空");
        }
        if(StringUtils.isBlank(chUserExam.getAnswerResult())){
            return RestResponse.failure("[a不能为空");
        }
        chUserExamService.insert(chUserExam);
        return RestResponse.success();
    }

    @GetMapping("edit")
    @SysLog("跳转编辑页面")
    public String edit(Long id,Model model){
        ChUserExam chUserExam = chUserExamService.selectById(id);
        model.addAttribute("chUserExam",chUserExam);
        return "/ytzl/chUserExam/edit";
    }

    @PostMapping("edit")
    @ResponseBody
    @SysLog("保存编辑数据")
    public RestResponse edit(ChUserExam chUserExam){
        if(null == chUserExam.getId() || "" == chUserExam.getId()){
            return RestResponse.failure("ID不能为空");
        }
        if(StringUtils.isBlank(chUserExam.getExamList())){
            return RestResponse.failure("json：[{编号:题目编号不能为空");
        }
        if(StringUtils.isBlank(chUserExam.getAnswerResult())){
            return RestResponse.failure("[a不能为空]");
        }
        chUserExamService.updateById(chUserExam);
        return RestResponse.success();
    }

    @PostMapping("delete")
    @ResponseBody
    @SysLog("删除数据")
    public RestResponse delete(@RequestParam(value = "id",required = false)Long id){
        if(null == id || 0 == id){
            return RestResponse.failure("ID不能为空");
        }
        ChUserExam chUserExam = chUserExamService.selectById(id);
        chUserExamService.updateById(chUserExam);
        return RestResponse.success();
    }

}