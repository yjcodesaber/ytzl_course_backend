package com.mysiteforme.admin.datasource;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.lang.reflect.ParameterizedType;

/**
 * @author jayden
 */
@Component
@Order(value = 0)
@Aspect
public class DataSourceSwitchAspect {

    private Logger log = LoggerFactory.getLogger(DataSourceSwitchAspect.class);

//    @Pointcut("execution(* com.mysiteforme.admin.dao.backend..*.*(..))")
//    private void dbBackendAspect() {
//    }
//
//    @Pointcut("execution(* com.mysiteforme.admin.dao.user..*.*(..))")
//    private void dbUserAspect() {
//    }
//
//    //  @Pointcut("execution(* com.mysiteforme.admin.dao.course..*.*(..))")
//    @Pointcut("execution(* com.mysiteforme.admin.dao.course..*.*(..))")
//    private void dbCourseAspect() {
//    }
//
////  @Pointcut("execution(* com.mysiteforme.admin.dao.course..*.*(..))")
//    @Pointcut("execution(* com.mysiteforme.admin.dao.question..*.*(..))")
//    private void dbQuestionAspect() {
//    }
//
//
//    @Before("dbBackendAspect()")
//    public void dbBackend(JoinPoint jp) {
//        log.info("切换到db_backend 数据源...");
//        DbContextHolder.setDbType(DBTypeEnum.db_backend);
//    }
//
//    @Before("dbUserAspect()")
//    public void dbUser() {
//        log.info("切换到db_user 数据源...");
//        DbContextHolder.setDbType(DBTypeEnum.db_user);
//    }
//
//    @Before("dbCourseAspect()")
//    public void dbCourse() {
//        log.info("切换到db_course 数据源...");
//        DbContextHolder.setDbType(DBTypeEnum.db_course);
//    }
//    @After("execution(* com.mysiteforme.admin.dao..*.*(..))")
//    public void dbChAfter(JoinPoint jp) {
//        DbContextHolder.clearDbType();
//    }
//    @Before("dbQuestionAspect()")
//    public void dbQuestion() {
//        log.info("切换到db_course 数据源...");
//        DbContextHolder.setDbType(DBTypeEnum.db_question);
//    }
//
    @Before("execution(* com.baomidou.mybatisplus.service..*.*(..))")
    public void dbChange(JoinPoint jp) {
        Class clazz = (Class) ((ParameterizedType) jp.getTarget().getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        String name = clazz.getName();
        log.info("切换到 数据源...-----------------dbChange---"+name);
        if (name.contains("backend")) {
            DbContextHolder.setDbType(DBTypeEnum.db_backend);
        } else if (name.contains("course")) {
            DbContextHolder.setDbType(DBTypeEnum.db_course);
        } else if (name.contains("user")) {
            DbContextHolder.setDbType(DBTypeEnum.db_user);
        } else if (name.contains("question")) {
            DbContextHolder.setDbType(DBTypeEnum.db_question);
        }
    }
    @After("execution(* com.baomidou.mybatisplus.service..*.*(..))")
    public void dbAfter(JoinPoint jp) {
        log.info("清理数据源名称");
        DbContextHolder.clearDbType();
    }
}