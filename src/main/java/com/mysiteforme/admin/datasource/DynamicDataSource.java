package com.mysiteforme.admin.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @author jayden
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    private static Logger LOG  = LoggerFactory.getLogger(DynamicDataSource.class);
    @Override
    protected Object determineCurrentLookupKey() {
        LOG.info("--determineCurrentLookupKey---"+DbContextHolder.getDbType());
        return DbContextHolder.getDbType();
    }
}
