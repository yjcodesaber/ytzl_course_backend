package com.mysiteforme.admin.datasource;

public enum DBTypeEnum {
    db_backend("db_backend"), db_user("db_user"),
    db_course("db_course"), db_question("db_question"),db_base("ch_base");


    private String value;

    DBTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
