package com.mysiteforme.admin.config;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import javax.annotation.Resource;

@Configuration
public class SolrConfiguration {
    @Resource
    private Environment environment;

    @Bean
    public SolrClient solrClient() {
        HttpSolrClient solrClient = new HttpSolrClient(environment.getRequiredProperty("spring.data.solr.host"));
        solrClient.setParser(new XMLResponseParser());
        solrClient.setConnectionTimeout(100000);
        return solrClient;
    }
}
