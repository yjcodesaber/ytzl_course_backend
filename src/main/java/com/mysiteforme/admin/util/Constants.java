package com.mysiteforme.admin.util;

public class Constants {
    /**
     * shiro采用加密算法
     */
    public static final String HASH_ALGORITHM = "SHA-1";
    /**
     * 生成Hash值的迭代次数
     */
    public static final int HASH_INTERATIONS = 1024;
    /**
     * 生成盐的长度
     */
    public static final int SALT_SIZE = 8;

    /**
     * 验证码
     */
    public static final String VALIDATE_CODE = "validateCode";

    /**
     * 系统用户默认密码
     */
    public static final String DEFAULT_PASSWORD = "123456";

    /**
     * 定时任务状态:正常
     */
    public static final Integer QUARTZ_STATUS_NOMAL = 0;
    /**
     * 定时任务状态:暂停
     */
    public static final Integer QUARTZ_STATUS_PUSH = 1;

    /**
     * 评论类型：1文章评论
     */
    public static final Integer COMMENT_TYPE_ARTICLE_COMMENT = 1;
    /**
     * 评论类型：2.系统留言
     */
    public static final Integer COMMENT_TYPE_LEVING_A_MESSAGE = 2;

    /**
     * 腾讯云COS 字典表中 host地址Id
     */
    public static final Long DICT_COS_VIDEO_HOST = 125L;
    /**
     * 腾讯云COS 字典表中 style Id
     */
    public static final Long DICT_COS_VIDEO_STYLE = 126L;
    /**
     * 腾讯云COS 字典表中 style Id
     */
    public static final Long DICT_COS_VIDEO_STYLE_630x400r = 127L;
}
