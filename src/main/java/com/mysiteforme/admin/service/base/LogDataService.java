package com.mysiteforme.admin.service.base;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.ChErrorLog;

/**
 * @author Administrator
 */
public interface LogDataService extends IService<ChErrorLog> {


    /**
     * 查询日志
     * @param page
     * @return
     */
    Page<ChErrorLog> selectLogData(Page<ChErrorLog> page, EntityWrapper<ChErrorLog> wrapper);
}
