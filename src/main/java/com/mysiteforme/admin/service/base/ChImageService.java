package com.mysiteforme.admin.service.base;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.base.ChImage;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-05-10
 */
public interface ChImageService extends IService<ChImage> {

    String getImageUrl(String paymentId);

    Boolean addBillImage(ChImage chImage);

    Boolean updateBillImage(ChImage chImage);

    Boolean deleteBillImage(String key);
}
