package com.mysiteforme.admin.service.impl.userimpl;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChReceivablesDao;
import com.mysiteforme.admin.entity.user.ChReceivables;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.user.ChReceivablesService;
import com.mysiteforme.admin.util.IdWorker;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-05-07
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChReceivablesServiceImpl extends ServiceImpl<ChReceivablesDao, ChReceivables> implements ChReceivablesService {

    @Resource
    private ChReceivablesDao chReceivablesDao;

    @Override
    public Page<ChReceivables> selectPageByWrapper(Page<ChReceivables> objectPage, EntityWrapper<ChReceivables> wrapper) {
        objectPage.setRecords(chReceivablesDao.selectPageByWrapper(objectPage,wrapper));
        return objectPage;
    }

    @Override
    public boolean updateReceivablesById(Map<String, Object> param) {
        return chReceivablesDao.updateReceivablesById(param)>0?true:false;
    }




    @Override
    public boolean insertReceivables(Map<String,Object> param) {
        String id = IdWorker.getId();
        String creationBy = ((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString();
        param.put("id",id);
        param.put("creationDate",new Date());
        param.put("creationBy",creationBy);
        return chReceivablesDao.insertReceivables(param)>0?true:false;
    }

    @Override
    public boolean updateDisable(String id) {
        return chReceivablesDao.updateDisable(id)>0?true:false;
    }

    @Override
    public List<ChReceivables> getReceivablesInfoById(String payeeId) {
        return chReceivablesDao.getReceivablesInfoById(payeeId);
    }

    @Override
    public List<ChReceivables> getReceivablesByMode(Integer payMode) {
        return chReceivablesDao.getReceivablesByMode(payMode);
    }
}
