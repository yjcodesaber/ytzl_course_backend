package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChStudyVideoRecordDao;
import com.mysiteforme.admin.entity.user.ChStudyVideoRecord;
import com.mysiteforme.admin.service.user.ChStudyVideoRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChStudyVideoRecordServiceImpl  extends ServiceImpl<ChStudyVideoRecordDao, ChStudyVideoRecord> implements ChStudyVideoRecordService {


    @Autowired
    private ChStudyVideoRecordDao chStudyVideoRecordDao;
    @Override
    public List<ChStudyVideoRecord> selectStudeyVideo(Map<String ,Object> param) {
          return   chStudyVideoRecordDao.selectStudeyVideo(param);
    }
}
