package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChUserLinkTaskDao;
import com.mysiteforme.admin.entity.user.UserLinkTask;
import com.mysiteforme.admin.service.user.ChUserLinkTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class ChUserLinkTaskServiceImpl extends ServiceImpl<ChUserLinkTaskDao,UserLinkTask> implements ChUserLinkTaskService {

    @Autowired
    private ChUserLinkTaskDao chUserLinkTaskDao;


    @Override
    public void approved(String userLinkTaskId, String taskComments) {
        chUserLinkTaskDao.approved(userLinkTaskId, taskComments);
    }

    @Override
    public UserLinkTask select(String userLinkTaskId) {
        return chUserLinkTaskDao.select(userLinkTaskId);
    }
}
