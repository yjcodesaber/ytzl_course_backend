package com.mysiteforme.admin.service.impl.baseimpl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.base.ChImageDao;
import com.mysiteforme.admin.entity.base.ChImage;
import com.mysiteforme.admin.service.base.ChImageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-05-10
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChImageServiceImpl extends ServiceImpl<ChImageDao, ChImage> implements ChImageService {

    @Resource
    private ChImageDao chImageDao;

    @Override
    public String getImageUrl(String paymentId) {
        return chImageDao.getImageUrl(paymentId);
    }

    @Override
    public Boolean addBillImage(ChImage chImage) {
        return chImageDao.addBillImage(chImage)>0?true:false;
    }

    @Override
    public Boolean updateBillImage(ChImage chImage) {
        return chImageDao.updateBillImage(chImage)>0?true:false;
    }

    @Override
    public Boolean deleteBillImage(String key) {
        return chImageDao.deleteBillImage(key)>0?true:false;
    }
}
