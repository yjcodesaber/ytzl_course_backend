package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.dao.user.ChUserLinkCourseDao;
import com.mysiteforme.admin.entity.user.UserLinkCourse;
import com.mysiteforme.admin.service.user.ChUserLinkCourseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChUserLinkCourseServiceImpl extends ServiceImpl<ChUserLinkCourseDao, UserLinkCourse> implements ChUserLinkCourseService {

    @Resource
    private ChUserLinkCourseDao chUserLinkCourseDao;

    @Override
    public boolean insertList(String insertInfo) {
        return chUserLinkCourseDao.insertList(insertInfo);
    }

    @Override
    public List<UserLinkCourse> findByUserId(String userId) {
        return chUserLinkCourseDao.findByUserId(userId);
    }

    @Override
    public boolean deleteInfo(String linkId) {
        return chUserLinkCourseDao.deleteInfo(linkId);
    }

    @Override
    public Page<UserLinkCourse> selectByEntityWrapper(Page<UserLinkCourse> objectPage, EntityWrapper<UserLinkCourse> wrapper) {
        objectPage.setRecords(chUserLinkCourseDao.selectByEntityWrapper(objectPage,wrapper));
        return objectPage;
    }
}
