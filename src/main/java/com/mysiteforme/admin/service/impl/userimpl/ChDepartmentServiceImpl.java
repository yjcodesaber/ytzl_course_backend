package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChDepartmentDao;
import com.mysiteforme.admin.entity.user.ChDepartment;
import com.mysiteforme.admin.service.user.ChDepartmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChDepartmentServiceImpl extends ServiceImpl<ChDepartmentDao, ChDepartment> implements ChDepartmentService {

    @Resource
    private ChDepartmentDao chDepartmentDao;

    @Override
    public Page<ChDepartment> selectByEntityWrapper(Page<ChDepartment> objectPage, EntityWrapper<ChDepartment> entityWrapper) {
        objectPage.setRecords(baseMapper.selectByEntityWrapper(objectPage, entityWrapper));
        return objectPage;
    }

    @Override
    public boolean addInfo(ChDepartment chDepartment) {
        return chDepartmentDao.addInfo(chDepartment);
    }

    @Override
    public ChDepartment findById(String id) {
        return chDepartmentDao.findById(id);
    }

    @Override
    public boolean editById(ChDepartment chDepartment) {
        return chDepartmentDao.editById(chDepartment);
    }

    @Override
    public boolean cutById(String id) {
        return chDepartmentDao.cutById(id);
    }
}
