package com.mysiteforme.admin.service.impl.courseimpl;

import com.mysiteforme.admin.dao.course.ChCourseDao;
import com.mysiteforme.admin.entity.course.ChCourse;
import com.mysiteforme.admin.service.course.ChCourseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChCourseServiceImpl extends ServiceImpl<ChCourseDao, ChCourse> implements ChCourseService {

    @Resource
    private ChCourseDao chCourseDao;

    @Override
    public Boolean insertCourse(ChCourse chCourse) {
       return chCourseDao.insertCourse(chCourse)>0?true:false;
    }

    @Override
    public Boolean updateCourseById(ChCourse chCourse) {
        return chCourseDao.updateCourseById(chCourse)>0?true:false;
    }

    @Override
    public Boolean deleteCourseById(String id) {
        return chCourseDao.deleteCourseById(id)>0?true:false;
    }

    @Override
    public String getVideoUrl(String id) {
       return chCourseDao.getVideoUrl(id);
    }
}
