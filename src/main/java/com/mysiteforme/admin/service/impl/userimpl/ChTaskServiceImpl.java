package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChTaskDao;
import com.mysiteforme.admin.dao.user.ChUserLinkTaskDao;
import com.mysiteforme.admin.entity.user.ChClass;
import com.mysiteforme.admin.entity.user.ChTask;
import com.mysiteforme.admin.entity.user.UserLinkTask;
import com.mysiteforme.admin.service.user.ChTaskService;
import com.mysiteforme.admin.util.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChTaskServiceImpl extends ServiceImpl<ChTaskDao, ChTask> implements ChTaskService {

    @Autowired
    private ChTaskDao chTaskDao;

    @Autowired
    private ChUserLinkTaskDao chUserLinkTaskDao;

    @Override
    public Page<ChTask> selectTask(Page<ChTask> page, EntityWrapper<ChTask> entityWrapper) {
        page.setRecords(baseMapper.selectTask(page,entityWrapper));
        return page;
    }

    @Override
    public List<ChClass> selectClassInfo(Page<Object> page, String teacherId) {
        return chTaskDao.selectClassInfo(page, teacherId);
    }


    @Override
    public void addTask(ChTask chTask, String ids) {
        chTaskDao.addTask(chTask);
        String[] split = ids.split(",");
        List<UserLinkTask> userLinkTasks = new ArrayList<>();
        for (int i = 0; i < split.length; i++) {
            UserLinkTask userLinkTask = new UserLinkTask();
            userLinkTask.setId(IdWorker.getId());
            userLinkTask.setTaskId(chTask.getId());
            userLinkTask.setTaskStatus(1);
            userLinkTask.setUserId(split[i]);
            userLinkTasks.add(userLinkTask);
            if (i % 30 == 0 || (split.length - i < 30 && i == split.length - 1)) {
                chUserLinkTaskDao.addUserTaskList(userLinkTasks);
                userLinkTasks.clear();
            }
        }
    }

    @Override
    public Integer selectCountChTask() {
        return chUserLinkTaskDao.selectCountChTask();
    }


}
