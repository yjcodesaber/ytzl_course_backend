package com.mysiteforme.admin.service.impl.courseimpl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.course.ChSubjectLinkMajorDao;
import com.mysiteforme.admin.entity.course.ChSubjectLinkMajor;
import com.mysiteforme.admin.service.course.ChSubjectLinkMajorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChSubjectLinkMajorServiceImpl extends ServiceImpl<ChSubjectLinkMajorDao, ChSubjectLinkMajor> implements ChSubjectLinkMajorService {

    @Resource
    private ChSubjectLinkMajorDao chSubjectLinkMajorDao;


    @Override
    public boolean addList(String infoList) {
        return chSubjectLinkMajorDao.addList(infoList);
    }

    @Override
    public boolean deleteByMajorId(String majorId) {
        return chSubjectLinkMajorDao.deleteByMajorId(majorId);
    }



}
