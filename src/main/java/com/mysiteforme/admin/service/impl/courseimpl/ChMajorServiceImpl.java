package com.mysiteforme.admin.service.impl.courseimpl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.course.ChMajorDao;
import com.mysiteforme.admin.entity.course.ChMajor;
import com.mysiteforme.admin.service.course.ChMajorService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChMajorServiceImpl extends ServiceImpl<ChMajorDao, ChMajor> implements ChMajorService {

    @Resource
    private ChMajorDao chMajorDao;

    @Override
    public boolean add(ChMajor chMajor) {
        return chMajorDao.add(chMajor);
    }



    @Override
    public boolean editById(ChMajor chMajor) {
        return chMajorDao.editById(chMajor);
    }

    @Override
    public boolean cutById(String id) {
        return chMajorDao.cutById(id);
    }

    @Override
    public List<ChMajor> getMajorList() {
        return chMajorDao.getMajorList();
    }
}
