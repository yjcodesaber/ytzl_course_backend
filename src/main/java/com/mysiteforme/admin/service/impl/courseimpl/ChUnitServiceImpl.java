package com.mysiteforme.admin.service.impl.courseimpl;

import com.mysiteforme.admin.dao.course.ChUnitDao;
import com.mysiteforme.admin.entity.course.ChUnit;
import com.mysiteforme.admin.service.course.ChUnitService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChUnitServiceImpl extends ServiceImpl<ChUnitDao, ChUnit> implements ChUnitService {

    @Resource
    private ChUnitDao chUnitDao;

    @Override
    public boolean insertUnit(ChUnit chUnit) {
        return chUnitDao.insertUnit(chUnit)>0?true:false;
    }

    @Override
    public boolean updateUnitById(ChUnit chUnit) {
        return chUnitDao.updateUnitById(chUnit)>0?true:false;
    }

    @Override
    public boolean deleteUnitById(String id) {
        return chUnitDao.deleteUnitById(id)>0?true:false;
    }
}
