package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.dao.user.ChUserDao;
import com.mysiteforme.admin.entity.user.ChUser;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.service.user.ChUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChUserServiceImpl extends ServiceImpl<ChUserDao, ChUser> implements ChUserService {

    @Resource
    private ChUserDao chUserDao;

    @Override
    public Page<ChUser> selectByEntityWrapper(Page<ChUser> objectPage, EntityWrapper<ChUser> entityWrapper) {
        objectPage.setRecords(chUserDao.selectByEntityWrapper(objectPage, entityWrapper));
        return objectPage;
    }

    @Override
    public boolean active(Map<String, Object> map) {
        return chUserDao.active(map);
    }

    @Override
    public Boolean wake(String id, int i) {
        return chUserDao.wake(id, i)>0 ? true : false;
    }

    @Override
    public ChUser findByUserNumber(String userNumber) {
        return chUserDao.findByUserNumber(userNumber);
    }

    @Override
    public Integer editor(Map<String, Object> param) {
        return chUserDao.editor(param);
    }

    @Override
    public Integer distributionClass(Map<String, Object> param) {
        return chUserDao.distributionClass(param);
    }

    @Override
    public ChUser selectByStudentInfo(String id) {
        return chUserDao.selectByStudentInfo(id);
    }

    @Override
    public Integer selectUserNumberCount(String userNumber) {
        return chUserDao.selectUserNumberCount(userNumber);
    }

    @Override
    public Integer selectUserAccountCount(String userAccount) {
        return chUserDao.selectUserAccountCount(userAccount);
    }

    @Override
    public Integer selectIdCardCount(String idCard) {
        return chUserDao.selectIdCardCount(idCard);
    }


    @Override
    public List<ChUser> findByLinkId(EntityWrapper<ChUser> entityWrapper) {
        return chUserDao.findByLinkId(entityWrapper);
    }
}
