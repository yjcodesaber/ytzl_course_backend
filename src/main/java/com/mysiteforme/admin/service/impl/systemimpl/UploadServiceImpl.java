package com.mysiteforme.admin.service.impl.systemimpl;

import com.alibaba.fastjson.JSON;
import com.mysiteforme.admin.properties.TencentCosProperties;
import com.mysiteforme.admin.service.system.UploadService;
import com.tencent.cloud.CosStsClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author jayden
 */
@Service("uploadService")
public class UploadServiceImpl implements UploadService {


    private Logger LOG = LoggerFactory.getLogger(UploadServiceImpl.class);

    @Resource
    private TencentCosProperties tencentCosProperties;

    @Override
    public Map<String, Object> serverSts(String bucket, String region) {
        TreeMap<String, Object> config = new TreeMap<>();
        try {
            // 替换为您的 SecretId
            config.put("SecretId", tencentCosProperties.getSecretId());
            // 替换为您的 SecretKey
            config.put("SecretKey", tencentCosProperties.getSecretKey());

            // 临时密钥有效时长，单位是秒
            config.put("durationSeconds", 1800);

            // 换成您的 bucket
            config.put("bucket", bucket);
            // 换成 bucket 所在地区
            config.put("region", region);

            // 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的目录，例子：* 或者 doc/* 或者 picture.jpg
            config.put("allowPrefix", "*");

            // 密钥的权限列表。简单上传、表单上传和分片上传需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
            String[] allowActions = new String[]{
                    // 简单上传
                    "name/cos:PutObject",
                    // 表单上传、小程序上传
                    "name/cos:PostObject",
                    // 分片上传
                    "name/cos:InitiateMultipartUpload",
                    "name/cos:ListMultipartUploads",
                    "name/cos:ListParts",
                    "name/cos:UploadPart",
                    "name/cos:DeleteObject",
                    "name/cos:CompleteMultipartUpload"
            };
            config.put("allowActions", allowActions);

            JSONObject credential = CosStsClient.getCredential(config);
            //成功返回临时密钥信息，如下打印密钥信息
            LOG.info("get secret success {}", credential);
            return (Map<String, Object>) JSON.parseObject(credential.toString(), Map.class);
        } catch (Exception e) {
            //失败抛出异常
            throw new IllegalArgumentException("no valid secret !");
        }
    }

    public static void main(String[] args) {
        TreeMap<String, Object> config = new TreeMap<>();
        try {
            // 替换为您的 SecretId
            config.put("SecretId", "AKIDnPwLZMxQoLhEpUFwPkT2dAIdq050jSSI");
            // 替换为您的 SecretKey
            config.put("SecretKey", "rcCX6844vIV5Zs2mQBk30qW3KTTOLtIW");

            // 临时密钥有效时长，单位是秒
            config.put("durationSeconds", 1800);

            // 换成您的 bucket
            config.put("bucket", "xmzb-1259015134");
            // 换成 bucket 所在地区
            config.put("region", "ap-beijing");

            // 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的目录，例子：* 或者 doc/* 或者 picture.jpg
            config.put("allowPrefix", "*");

            // 密钥的权限列表。简单上传、表单上传和分片上传需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
            String[] allowActions = new String[]{
                    // 简单上传
                    "name/cos:PutObject",
                    // 表单上传、小程序上传
                    "name/cos:PostObject",
                    // 分片上传
                    "name/cos:InitiateMultipartUpload",
                    "name/cos:ListMultipartUploads",
                    "name/cos:ListParts",
                    "name/cos:UploadPart",
                    "name/cos:CompleteMultipartUpload"
            };
            config.put("allowActions", allowActions);

            JSONObject credential = CosStsClient.getCredential(config);
            //成功返回临时密钥信息，如下打印密钥信息
            System.out.println(credential);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}