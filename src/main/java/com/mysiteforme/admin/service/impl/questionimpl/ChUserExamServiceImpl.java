package com.mysiteforme.admin.service.impl.questionimpl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.question.ChUserExamDao;
import com.mysiteforme.admin.entity.VO.UserExamDto;
import com.mysiteforme.admin.entity.question.ChUserExam;
import com.mysiteforme.admin.service.question.ChUserExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChUserExamServiceImpl extends ServiceImpl<ChUserExamDao, ChUserExam> implements ChUserExamService {

    @Autowired
    private ChUserExamDao chUserExamDao;
    @Override
    public List<UserExamDto> selectStuExam(Map<String,Object> param) {

        List<UserExamDto> stuExam = chUserExamDao.selectStuExam(param);
        return  stuExam;
    }
}
