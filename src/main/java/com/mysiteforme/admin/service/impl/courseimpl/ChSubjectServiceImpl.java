package com.mysiteforme.admin.service.impl.courseimpl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.course.ChSubjectDao;
import com.mysiteforme.admin.entity.course.ChSubject;
import com.mysiteforme.admin.service.course.ChSubjectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChSubjectServiceImpl extends ServiceImpl<ChSubjectDao, ChSubject> implements ChSubjectService {

    @Resource
    private ChSubjectDao chSubjectDao;

    @Override
    public boolean add(ChSubject chSubject) {
        return chSubjectDao.add(chSubject);
    }

    @Override
    public boolean editById(ChSubject chSubject) {
        return chSubjectDao.editById(chSubject);
    }

    @Override
    public boolean cutById(String id) {
        return chSubjectDao.cutById(id);
    }

    @Override
    public List<ChSubject> findAll() {
        return chSubjectDao.findAll();
    }

    @Override
    public List<ChSubject> findByMajorId(String majorId) {
        return chSubjectDao.findByMajorId(majorId);
    }


}
