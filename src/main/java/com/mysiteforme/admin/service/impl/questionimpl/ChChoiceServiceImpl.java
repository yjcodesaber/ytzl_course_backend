package com.mysiteforme.admin.service.impl.questionimpl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.question.ChChoiceDao;
import com.mysiteforme.admin.entity.question.ChChoice;
import com.mysiteforme.admin.service.question.ChChoiceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChChoiceServiceImpl extends ServiceImpl<ChChoiceDao, ChChoice> implements ChChoiceService {

    @Resource
    private ChChoiceDao chChoiceDao;

    @Override
    public boolean deleteChoiceById(String id) {
        return chChoiceDao.deleteChoiceById(id) > 0 ? true : false;
    }

    @Override
    public boolean correctById(String id) {
        return chChoiceDao.correctById(id)>0?true:false;
    }

    @Override
    public boolean wrongById(String id) {
        return chChoiceDao.wrongById(id)>0?true:false;
    }

    @Override
    public boolean insertChoice(ChChoice chChoice) {
        return chChoiceDao.insertChoice(chChoice)>0?true:false;
    }
}
