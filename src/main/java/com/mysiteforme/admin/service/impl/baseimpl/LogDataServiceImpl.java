package com.mysiteforme.admin.service.impl.baseimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.backend.LogDataDao;
import com.mysiteforme.admin.entity.ChErrorLog;
import com.mysiteforme.admin.service.base.LogDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class LogDataServiceImpl extends ServiceImpl<LogDataDao, ChErrorLog> implements LogDataService {

    @Autowired
    private LogDataDao logDataDao;

    @Override
    public Page<ChErrorLog> selectLogData(Page<ChErrorLog> page, EntityWrapper<ChErrorLog> wrapper) {
        return page.setRecords(baseMapper.selectLogData(page,wrapper));
    }
}
