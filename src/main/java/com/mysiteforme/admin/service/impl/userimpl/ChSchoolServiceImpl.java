package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.dao.user.ChSchoolDao;
import com.mysiteforme.admin.entity.user.ChSchool;
import com.mysiteforme.admin.service.user.ChSchoolService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChSchoolServiceImpl extends ServiceImpl<ChSchoolDao, ChSchool> implements ChSchoolService {

    @Resource
    private ChSchoolDao chSchoolDao;

    @Override
    public Page<ChSchool> selectByEntityWrapper(Page<ChSchool> objectPage, EntityWrapper<ChSchool> entityWrapper) {
        objectPage.setRecords(baseMapper.selectByEntityWrapper(objectPage,entityWrapper));
        return objectPage;
    }

    @Override
    public boolean addInfo(ChSchool chSchool) {
        return chSchoolDao.addInfo(chSchool);
    }

    @Override
    public boolean editById(ChSchool chSchool) {
        return chSchoolDao.editById(chSchool);
    }

    @Override
    public boolean cutById(String id) {
        return chSchoolDao.cutById(id);
    }
}
