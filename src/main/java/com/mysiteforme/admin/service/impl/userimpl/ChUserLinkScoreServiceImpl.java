package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChUserLinkScoreDao;

import com.mysiteforme.admin.entity.user.ChUserLinkScore;
import com.mysiteforme.admin.service.user.ChUserLinkScoreService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChUserLinkScoreServiceImpl extends ServiceImpl<ChUserLinkScoreDao, ChUserLinkScore> implements ChUserLinkScoreService {


    @Resource
    private ChUserLinkScoreDao chUserLinkScoreDao;

    @Override
    public Page<ChUserLinkScore> selectByEntityWrapper(Page<ChUserLinkScore> objectPage, EntityWrapper<ChUserLinkScore> wrapper) {
        objectPage.setRecords(chUserLinkScoreDao.selectByEntityWrapper(objectPage,wrapper));
        return objectPage;
    }

    @Override
    public boolean deleteScoreById(String id) {
        return chUserLinkScoreDao.deleteScoreById(id)>0?true:false;
    }

    @Override
    public boolean insertScore(ChUserLinkScore chUserLinkScore) {
        return chUserLinkScoreDao.insertScore(chUserLinkScore)>0?true:false;
    }

    @Override
    public boolean updateScore(ChUserLinkScore chUserLinkScore) {
        return chUserLinkScoreDao.updateScore(chUserLinkScore)>0?true:false;
    }

    @Override
    public List<ChUserLinkScore> selectScoreByIds(String ids) {

        return chUserLinkScoreDao.selectScoreByIds(ids);
    }
}
