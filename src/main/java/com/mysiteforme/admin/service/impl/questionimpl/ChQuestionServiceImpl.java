package com.mysiteforme.admin.service.impl.questionimpl;

import com.mysiteforme.admin.dao.question.ChChoiceDao;
import com.mysiteforme.admin.dao.question.ChQuestionDao;
import com.mysiteforme.admin.entity.question.ChChoice;
import com.mysiteforme.admin.entity.question.ChQuestion;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.entity.question.ChQuestionVO;
import com.mysiteforme.admin.realm.AuthRealm;
import com.mysiteforme.admin.service.question.ChQuestionService;
import com.mysiteforme.admin.util.IdWorker;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChQuestionServiceImpl extends ServiceImpl<ChQuestionDao, ChQuestion> implements ChQuestionService {

    @Resource
    private ChQuestionDao chQuestionDao;

    @Resource
    private ChChoiceDao chChoiceDao;

    @Override
    public boolean deleteQuestionById(String id) {
        int delQ = chQuestionDao.deleteQuestionById(id, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        int delA = chChoiceDao.delChoiceByQuestionId(id);
        if (delA > 0 && delQ > 0)
            return true;
        else
            return false;
    }

    @Override
    public boolean updateQuestoinById(ChQuestion chQuestion) {
        return chQuestionDao.updateQuestoinById(chQuestion) > 0 ? true : false;
    }

    @Override
    public boolean insertQuestion(ChQuestion chQuestion) {
        return chQuestionDao.insertQuestion(chQuestion) > 0 ? true : false;
    }

    @Override
    public boolean deleteQuestionByIds(String ids) {
        int delQ = chQuestionDao.deleteQuestionByIds(ids, new Date());
        int delA = chChoiceDao.delChoiceByQuestionIds(ids);
        if (delA > 0 && delQ > 0)
            return true;
        else
            return false;
    }

    @Override
    public String getType(String questionId) {
        return chQuestionDao.getType(questionId);
    }

    @Override
    public boolean insertExcel(List<ChQuestionVO> chQuestionVOs) {
        StringBuffer questionSB = new StringBuffer();
        StringBuffer anwserSB = new StringBuffer();
        for (ChQuestionVO cq : chQuestionVOs) {
            questionSB.append("(").append("\"" + cq.getId() + "\",").
                    append("\"" + cq.getKeyWords() + "\",").
                    append("\"" + cq.getLorePointId() + "\",").
                    append("\"" + cq.getQuestionDescribe() + "\",").
                    append(cq.getQuestionType() + ",").
                    append("\"" + cq.getQuestionAnalysis() + "\",").
                    append("\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\"").
                    append(",1),");
            anwserSB.append("(\"").append(IdWorker.getId()).append("\",\"").append(cq.getAnwser1() + "\",").
                    append("\"" + cq.getId() + "\",").append(cq.getA1ok() + ",").
                    append("\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\"),").
                    append("(\"").append(IdWorker.getId()).append("\",\"").append(cq.getAnwser2() + "\",").
                    append("\"" + cq.getId() + "\",").append(cq.getA2ok() + ",").
                    append("\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\"),").
                    append("(\"").append(IdWorker.getId()).append("\",\"").append(cq.getAnwser3() + "\",").
                    append("\"" + cq.getId() + "\",").append(cq.getA3ok() + ",").
                    append("\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\"),").
                    append("(\"").append(IdWorker.getId()).append("\",\"").append(cq.getAnwser4() + "\",").
                    append("\"" + cq.getId() + "\",").append(cq.getA4ok() + ",").
                    append("\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\"),");
        }
        questionSB = questionSB.deleteCharAt(questionSB.length() - 1);
        int question = chQuestionDao.insertExcel(questionSB.toString());
        anwserSB = anwserSB.deleteCharAt(anwserSB.length() - 1);
        int choice = chChoiceDao.insertChoiceExcel(anwserSB.toString());
        if (question > 0 && choice > 0) {
            return true;
        } else
            return false;
    }

    @Override
    public boolean creatQandA(ChQuestionVO chQuestionVO) {
        ChQuestion chQuestion = new ChQuestion();
        BeanUtils.copyProperties(chQuestionVO, chQuestion);
        int upQ = chQuestionDao.insertQuestion(chQuestion);
        if (upQ <= 0) {
            return false;
        }
        int a1 = 2, a2 = 2, a3 = 2, a4 = 2;
        if (chQuestionVO.getA1ok() != null) {
            a1 = 1;
        }
        if (chQuestionVO.getA2ok() != null) {
            a2 = 1;
        }
        if (chQuestionVO.getA3ok() != null) {
            a3 = 1;
        }
        if (chQuestionVO.getA4ok() != null) {
            a4 = 1;
        }

        String questionId = chQuestionVO.getId();
        String creationBy = ((AuthRealm.ShiroUser) SecurityUtils.getSubject().getPrincipal()).getId().toString();
        StringBuffer sb = new StringBuffer();
        sb.append("(\"" + IdWorker.getId() + "\"," + '\"' + chQuestionVO.getAnwser1() + '\"'
                + ",\"" + questionId + "\"," + a1 + "," + '\"' + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + '\"' + "," + '\"' + creationBy + '\"' + "),(\"" + IdWorker.getId() + "\"," + '\"' + chQuestionVO.getAnwser2() + '\"'
                + ",\"" + questionId + "\"," + a2 + "," + '\"' + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + '\"' + "," + '\"' + creationBy + '\"' + "),(\"" + IdWorker.getId() + "\"," + '\"' + chQuestionVO.getAnwser3() + '\"'
                + ",\"" + questionId + "\"," + a3 + "," + '\"' + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + '\"' + "," + '\"' + creationBy + '\"' + "),(\"" + IdWorker.getId() + "\"," + '\"' + chQuestionVO.getAnwser4() + '\"'
                + ",\"" + questionId + "\"," + a4 + "," + '\"' + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + '\"' + "," + '\"' + creationBy + '\"' + ")")
        ;
        int upA = chChoiceDao.insertFourAnwser(sb.toString());
        if (upQ > 0 && upA > 0)
            return true;
        else
            return false;
    }

}
