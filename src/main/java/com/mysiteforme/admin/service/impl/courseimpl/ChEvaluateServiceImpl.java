package com.mysiteforme.admin.service.impl.courseimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.dao.course.ChEvaluateDao;
import com.mysiteforme.admin.entity.course.ChEvaluate;
import com.mysiteforme.admin.service.course.ChEvaluateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChEvaluateServiceImpl extends ServiceImpl<ChEvaluateDao, ChEvaluate> implements ChEvaluateService {

    @Override
    public Page<ChEvaluate> selectByWrapper(Page<ChEvaluate> objectPage, EntityWrapper<ChEvaluate> wrapper) {
       objectPage.setRecords(baseMapper.selectByWrapper(objectPage,wrapper));
       return objectPage;
    }

    @Override
    public Boolean updateEvaluateById(ChEvaluate chEvaluate) {
        //Integer selectStartById = baseMapper.selectStartById(chEvaluate.getId());
        return baseMapper.updateEvaluateById(chEvaluate)>0?true:false;
    }

    @Override
    public Boolean updateErrorById(String id) {
        //Integer selectStartById = baseMapper.selectStartById(id);
        //if (selectStartById==1) {
            return baseMapper.updateErrorById(id) > 0 ? true : false;
//        }else{
//            return null;
//        }
    }

    @Override
    public Boolean deleteEvaluateById(String id) {

        return baseMapper.deleteEvaluateById(id)>0?true:false;
    }

    @Override
    public Integer selectStartById(String chEvaluateId) {
        return baseMapper.selectStartById(chEvaluateId);
    }

    @Override
    public Integer selectCountEvalueate() {
        return baseMapper.selectCountEvalueate();
    }


}
