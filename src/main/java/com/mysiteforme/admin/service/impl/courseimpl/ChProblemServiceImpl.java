package com.mysiteforme.admin.service.impl.courseimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.dao.course.ChProblemDao;
import com.mysiteforme.admin.entity.course.ChProblem;
import com.mysiteforme.admin.service.course.ChProblemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChProblemServiceImpl extends ServiceImpl<ChProblemDao, ChProblem> implements ChProblemService {

    @Override
    public Page<ChProblem> selectByWrapperToProblem(Page<ChProblem> objectPage, EntityWrapper<ChProblem> wrapper) {
        objectPage.setRecords(baseMapper.selectByWrapperToProblem(objectPage,wrapper));
        return objectPage;
    }

    @Override
    public Boolean deleteProblemById(String id) {

        return baseMapper.deleteProblemById(id)>0?true:false;
    }

    @Override
    public Boolean updateProblemById(ChProblem chProblem) {

        return baseMapper.updateProblemById(chProblem)>0?true:false;
    }

    @Override
    public Integer selectCountProblem() {
        return baseMapper.selectCountProblem();
    }


}
