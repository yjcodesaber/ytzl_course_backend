package com.mysiteforme.admin.service.impl.dataimpl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.data.StudyDataDao;
import com.mysiteforme.admin.entity.data.Data;
import com.mysiteforme.admin.service.data.StudyDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 *
 * @author Administrator
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class StudyDataServiceImpl extends ServiceImpl<StudyDataDao, Data> implements StudyDataService {

    @Autowired
    private StudyDataDao studyDataDao;


    @Override
    public Page<Data> selectData(Page<Data> page, Map<String,Object> param) {
     return  page.setRecords(baseMapper.selectData(page,param));
    }
}
