package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChClassDao;
import com.mysiteforme.admin.dao.user.ChUserDao;
import com.mysiteforme.admin.entity.user.ChClass;
import com.mysiteforme.admin.entity.user.ChUser;
import com.mysiteforme.admin.service.user.ChClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChClassServiceImpl extends ServiceImpl<ChClassDao, ChClass> implements ChClassService {

    @Autowired
    private ChClassDao chClassDao;

    @Autowired
    private ChUserDao chUserDao;

    @Override
    public List<ChClass> selectClassName(Map<String, Object> param) {
        return chClassDao.selectClassName(param);
    }

    @Override
    public List<ChUser> selectStudent(String classId) {
        return chUserDao.selectStudent(classId);
    }


    @Override
    public Page<ChClass> selectByEntityWrapper(Page<ChClass> objectPage, EntityWrapper<ChClass> wrapper) {
         objectPage.setRecords(baseMapper.selectByEntityWrapper(objectPage,wrapper));
         return objectPage;
    }

    @Override
    public boolean addInfo(ChClass chClass) {
        return chClassDao.addInfo(chClass);
    }

    @Override
    public boolean editById(ChClass chClass) {
        return chClassDao.editById(chClass);
    }

    @Override
    public boolean cutById(String id) {
        return chClassDao.cutById(id);
    }

    @Override
    public ChClass findById(String id) {
        return chClassDao.findById(id);
    }

    @Override
    public  List<ChClass> selectClass(EntityWrapper<ChClass> wrapper) {
        return chClassDao.selectClass(wrapper);
    }
}
