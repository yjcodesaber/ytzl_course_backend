package com.mysiteforme.admin.service.impl.courseimpl;

import com.mysiteforme.admin.dao.course.ChSectionDao;
import com.mysiteforme.admin.entity.course.ChSection;
import com.mysiteforme.admin.service.course.ChSectionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChSectionServiceImpl extends ServiceImpl<ChSectionDao, ChSection> implements ChSectionService {

    @Resource
    private ChSectionDao chSectionDao;

    @Override
    public boolean insertSection(ChSection chSection) {
        return chSectionDao.insertSection(chSection)>0?true:false;
    }

    @Override
    public boolean updateSectionById(ChSection chSection) {
        return chSectionDao.updateSectionById(chSection)>0?true:false;
    }

    @Override
    public boolean deleteSectionById(String id) {
        return chSectionDao.deleteSectionById(id)>0?true:false;
    }
}
