package com.mysiteforme.admin.service.impl.userimpl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mysiteforme.admin.dao.user.ChPaymentDao;
import com.mysiteforme.admin.entity.user.ChPayment;
import com.mysiteforme.admin.service.user.ChPaymentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ChPaymentServiceImpl extends ServiceImpl<ChPaymentDao, ChPayment> implements ChPaymentService {

    @Resource
    private ChPaymentDao chPaymentDao;

    @Override
    public boolean insertPayment(ChPayment chPayment) {
        return chPaymentDao.insertPayment(chPayment)>0?true:false;
    }

    @Override
    public Boolean updatePaymentById(ChPayment chPayment) {
        return chPaymentDao.updatePaymentById(chPayment)>0?true:false;
    }

    @Override
    public Boolean deletePaymentById(String id) {
        return chPaymentDao.deletePaymentById(id)>0?true:false;
    }

    @Override
    public boolean examine(String id) {
        return chPaymentDao.examine(id)>0?true:false;
    }

    @Override
    public Page<ChPayment> selectByEntityWrapper(Page<ChPayment> objectPage, EntityWrapper<ChPayment> wrapper) {
        objectPage.setRecords(chPaymentDao.selectByEntityWrapper(objectPage,wrapper));
        return objectPage;
    }

    @Override
    public Integer selectCountChPayment() {
        return chPaymentDao.selectCountChPayment();
    }
}
