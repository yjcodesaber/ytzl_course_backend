package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.user.ChStudyVideoRecord;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public interface ChStudyVideoRecordService extends IService<ChStudyVideoRecord> {


    /**
     * @param param
     * @return
     */
    List<ChStudyVideoRecord> selectStudeyVideo(Map<String,Object> param);
}
