package com.mysiteforme.admin.service.user;

import com.mysiteforme.admin.entity.user.UserLinkTask;

/**
 * @author Administrator
 * 用户关联任务表
 */

public interface ChUserLinkTaskService  {


    /**
     * 审核通过更新任务转态，老师评语
     * @param userLinkTaskId
     * @param taskComments
     */
    void approved(String userLinkTaskId, String taskComments);

    /**
     * 通过用户关联任务表id 查询状态
     * @param userLinkTaskId
     * @return
     */
    UserLinkTask select(String userLinkTaskId);
}
