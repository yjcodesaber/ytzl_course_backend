package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChSchool;
import com.baomidou.mybatisplus.service.IService;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChSchoolService extends IService<ChSchool> {

    Page<ChSchool> selectByEntityWrapper(Page<ChSchool> objectPage, EntityWrapper<ChSchool> entityWrapper);

    boolean addInfo(ChSchool chSchool);

    boolean editById(ChSchool chSchool);

    boolean cutById(String id);
}
