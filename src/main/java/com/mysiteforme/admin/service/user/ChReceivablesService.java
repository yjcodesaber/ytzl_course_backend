package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.user.ChReceivables;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-05-07
 */
public interface ChReceivablesService extends IService<ChReceivables> {

    /**
     * @param objectPage
     * @param wrapper
     * @return
     */
    Page<ChReceivables> selectPageByWrapper(Page<ChReceivables> objectPage, EntityWrapper<ChReceivables> wrapper);

    /**
     * @param param
     * @return
     */
    boolean updateReceivablesById(Map<String, Object> param);

    List<ChReceivables> getReceivablesInfoById(String payeeId);

    List<ChReceivables> getReceivablesByMode(Integer payMode);
    boolean insertReceivables(Map<String, Object> param);

    /**
     * @param id
     * @return
     */
    boolean updateDisable(String id);
}
