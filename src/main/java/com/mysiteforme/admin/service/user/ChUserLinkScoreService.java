package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.user.ChUserLinkScore;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChUserLinkScoreService extends IService<ChUserLinkScore> {

    Page<ChUserLinkScore> selectByEntityWrapper(Page<ChUserLinkScore> objectPage, EntityWrapper<ChUserLinkScore> wrapper);

    boolean deleteScoreById(String id);

    boolean insertScore(ChUserLinkScore chUserLinkScore);


    boolean updateScore(ChUserLinkScore chUserLinkScore);
//根据id批量查询数据（excel导出时使用）
    List<ChUserLinkScore> selectScoreByIds(String ids);
}
