package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.user.ChDepartment;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
public interface ChDepartmentService extends IService<ChDepartment> {

    Page<ChDepartment> selectByEntityWrapper(Page<ChDepartment> objectPage, EntityWrapper<ChDepartment> entityWrapper);


    boolean addInfo(ChDepartment chDepartment);


    ChDepartment findById(String id);

    boolean editById(ChDepartment chDepartment);

    boolean cutById(String id);
}
