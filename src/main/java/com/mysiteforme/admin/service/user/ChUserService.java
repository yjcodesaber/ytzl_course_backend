package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.user.ChUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChUserService extends IService<ChUser> {


    Page<ChUser> selectByEntityWrapper(Page<ChUser> objectPage, EntityWrapper<ChUser> entityWrapper);

    boolean active(Map<String,Object> map);

    Boolean wake(String id, int i);

    List<ChUser> findByLinkId(EntityWrapper<ChUser> entityWrapper);

    ChUser findByUserNumber(String userNumber);

    Integer editor(Map<String, Object> param);

    Integer distributionClass(Map<String, Object> param);

    ChUser selectByStudentInfo(String id);

    Integer selectUserNumberCount(String userNumber);

    Integer selectUserAccountCount(String userAccount);

    Integer selectIdCardCount(String idCard);
}
