package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.user.ChPayment;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChPaymentService extends IService<ChPayment> {

    boolean insertPayment(ChPayment chPayment);

    Boolean updatePaymentById(ChPayment chPayment);

    Boolean deletePaymentById(String id);

    boolean examine(String id);

    Page<ChPayment> selectByEntityWrapper(Page<ChPayment> objectPage, EntityWrapper<ChPayment> wrapper);

    Integer selectCountChPayment();

}
