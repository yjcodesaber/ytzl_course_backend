package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.UserLinkCourse;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChUserLinkCourseService extends IService<UserLinkCourse> {
    boolean insertList(String insertInfo);

    List<UserLinkCourse> findByUserId(String userId);

    boolean deleteInfo(String linkId);

    Page<UserLinkCourse> selectByEntityWrapper(Page<UserLinkCourse> objectPage, EntityWrapper<UserLinkCourse> wrapper);
}
