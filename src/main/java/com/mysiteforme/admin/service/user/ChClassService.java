package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.user.ChClass;
import com.mysiteforme.admin.entity.user.ChUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChClassService extends IService<ChClass> {


    /**
     * 动态加载学生姓名和id
     * @return
     */
    List<ChClass> selectClassName(Map<String, Object> param);

    /**
     * 根据班级查询学生
     * @param classId
     * @return
     */
    List<ChUser> selectStudent(String classId);
    Page<ChClass> selectByEntityWrapper(Page<ChClass> objectPage, EntityWrapper<ChClass> wrapper);

    boolean addInfo(ChClass chClass);

    boolean editById(ChClass chClass);

    boolean cutById(String id);

    ChClass findById(String id);

    /**
     * 根据当前登录的id，动态获取班级信息，学习任务
     * @param entityWrapper
     */
    List<ChClass> selectClass(EntityWrapper<ChClass> entityWrapper);

}
