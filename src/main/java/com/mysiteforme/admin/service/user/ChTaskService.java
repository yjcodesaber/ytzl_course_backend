package com.mysiteforme.admin.service.user;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.user.ChTask;
import com.mysiteforme.admin.entity.user.ChClass;

import java.util.List;

/**
 * 学习任务
 * @author Administrator
 */
public interface ChTaskService {


    /**
     * 查询学习任务列表
     * @param page
     * @return
     */
    Page<ChTask> selectTask(Page<ChTask> page, EntityWrapper<ChTask> entityWrapper);

    /**
     * 根据当前系统用户id查询班级信息
     * @param teacherId
     * @return
     */
    List<ChClass> selectClassInfo(Page<Object> page, String teacherId);


    /**
     * 添加任务 和任务用户关联
     * @param chTask
     */
    void addTask(ChTask chTask,String ids);


    /**
     * 统计
     * @return
     */
    Integer selectCountChTask();
}
