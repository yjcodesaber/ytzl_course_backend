package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.course.ChLorePoint;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChLorePointService extends IService<ChLorePoint> {

    boolean insertLorePoint(ChLorePoint chLorePoint);

    boolean updateLorePointById(ChLorePoint chLorePoint);

    boolean deleteLorePointById(String id);

    String getVideoUrl(String id);
}
