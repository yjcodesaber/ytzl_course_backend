package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.course.ChMajor;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChMajorService extends IService<ChMajor> {

    boolean add(ChMajor chMajor);



    boolean editById(ChMajor chMajor);

    boolean cutById(String id);

    List<ChMajor> getMajorList();
}
