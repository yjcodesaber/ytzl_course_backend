package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.course.ChProblem;
import com.baomidou.mybatisplus.service.IService;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
public interface ChProblemService extends IService<ChProblem> {

    Page<ChProblem> selectByWrapperToProblem(Page<ChProblem> objectPage, EntityWrapper<ChProblem> wrapper);

    Boolean deleteProblemById(String id);

    Boolean updateProblemById(ChProblem chProblem);

    Integer selectCountProblem();

}
