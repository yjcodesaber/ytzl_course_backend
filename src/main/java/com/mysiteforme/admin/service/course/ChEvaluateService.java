package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mysiteforme.admin.entity.course.ChEvaluate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-23
 */
public interface ChEvaluateService extends IService<ChEvaluate> {

    Page<ChEvaluate> selectByWrapper(Page<ChEvaluate> objectPage, EntityWrapper<ChEvaluate> wrapper);


    Boolean updateEvaluateById(ChEvaluate chEvaluate);

    Boolean updateErrorById(String id);

    Boolean deleteEvaluateById(String id);

    Integer selectStartById(String chEvaluateId);

    Integer selectCountEvalueate();

}
