package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.course.ChCourse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChCourseService extends IService<ChCourse> {

     Boolean insertCourse(ChCourse chCourse);

    Boolean updateCourseById(ChCourse chCourse);

    Boolean deleteCourseById(String id);

    String getVideoUrl(String id);
}
