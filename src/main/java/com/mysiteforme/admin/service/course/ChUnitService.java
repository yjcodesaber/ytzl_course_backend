package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.course.ChUnit;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChUnitService extends IService<ChUnit> {

    boolean insertUnit(ChUnit chUnit);

    boolean updateUnitById(ChUnit chUnit);

    boolean deleteUnitById(String id);
}
