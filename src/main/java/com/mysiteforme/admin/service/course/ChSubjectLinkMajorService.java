package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.course.ChSubjectLinkMajor;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChSubjectLinkMajorService extends IService<ChSubjectLinkMajor> {

    boolean addList(String infoList);


    boolean deleteByMajorId(String majorId);



}
