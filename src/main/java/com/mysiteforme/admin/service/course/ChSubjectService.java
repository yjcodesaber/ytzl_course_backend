package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.course.ChSubject;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-29
 */
public interface ChSubjectService extends IService<ChSubject> {

    boolean add(ChSubject chSubject);

    boolean editById(ChSubject chSubject);

    boolean cutById(String id);

    List<ChSubject> findAll();

    List<ChSubject> findByMajorId(String majorId);
}
