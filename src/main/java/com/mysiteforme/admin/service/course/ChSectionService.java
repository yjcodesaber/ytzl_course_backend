package com.mysiteforme.admin.service.course;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.course.ChSection;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-15
 */
public interface ChSectionService extends IService<ChSection> {

    boolean insertSection(ChSection chSection);

    boolean updateSectionById(ChSection chSection);

    boolean deleteSectionById(String id);
}
