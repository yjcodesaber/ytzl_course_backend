package com.mysiteforme.admin.service.system;

import java.util.Map;

/**
 * @author sam
 */
public interface UploadService {

    /**
     * 获取秘钥
     *
     * @return
     * @param bucket
     * @param region
     */
    Map<String, Object> serverSts(String bucket, String region);
}
