package com.mysiteforme.admin.service.data;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.data.Data;

import java.util.Map;

/**
 * 学习数据
 * @author Administrator
 */
public interface StudyDataService extends IService<Data> {

    /**统计学生时长数据
     * @param page
     * @return
     */
    Page<Data> selectData(Page<Data> page, Map<String,Object> param);
}
