package com.mysiteforme.admin.service.question;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.question.ChChoice;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
public interface ChChoiceService extends IService<ChChoice> {

    boolean deleteChoiceById(String id);

    boolean correctById(String id);

    boolean wrongById(String id);

    boolean insertChoice(ChChoice chChoice);
}
