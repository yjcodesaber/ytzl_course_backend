package com.mysiteforme.admin.service.question;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.VO.UserExamDto;
import com.mysiteforme.admin.entity.question.ChUserExam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
public interface ChUserExamService extends IService<ChUserExam> {

    /**
     * 通过学生编号查询学生考试题，和
     * @param param
     * @return
     */
        List<UserExamDto> selectStuExam(Map<String,Object> param);

}
