package com.mysiteforme.admin.service.question;

import com.baomidou.mybatisplus.service.IService;
import com.mysiteforme.admin.entity.question.ChQuestion;
import com.mysiteforme.admin.entity.question.ChQuestionVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yj
 * @since 2019-04-24
 */
public interface ChQuestionService extends IService<ChQuestion> {

    boolean deleteQuestionById(String id);

    boolean updateQuestoinById(ChQuestion chQuestion);

    boolean insertQuestion(ChQuestion chQuestion);

    boolean deleteQuestionByIds(String ids);

    String getType(String questionId);

    boolean insertExcel(List<ChQuestionVO> chQuestions);

    boolean creatQandA(ChQuestionVO chQuestionVO);
}
