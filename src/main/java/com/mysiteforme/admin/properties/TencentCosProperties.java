package com.mysiteforme.admin.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author jayden
 */
@Component
@ConfigurationProperties(prefix = "tencent.cos")
public class TencentCosProperties {


    /**
     * 数据桶
     */
    private String bucketName;
    /**
     * 地域
     */
    private String region;

    /**
     * 秘钥Id
     */
    private String secretId;
    /**
     * 秘钥key
     */
    private String secretKey;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
}
